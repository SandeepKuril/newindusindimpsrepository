﻿using EntityLayer;
using IndusIndIMPSAdminPortal.HelperClass;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Data;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace DataAccess
{
    public class LoginDA
    {
       static NLogHelper logs = new NLogHelper(LogManager.GetCurrentClassLogger());
        public LoginDA()
        { }
       static MySqlConnection con;

        public static MySqlConnection getConnection()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            var config = builder.Build();
            //con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("DevConnection").Value);
            con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("IndusIndAdminConStr").Value);
            return con;
        }
        public static int Login(LoginEL ObjLoginEL)
        {
            /*int i = 0;
            MySqlParameter[] param = null;
            try
            {
                MySqlCommand command = new MySqlCommand();
                param = new MySqlParameter[2];
                param[0] = new MySqlParameter("I_Login_Username", ObjLoginEL.UserName);
                param[1] = new MySqlParameter("I_Session_ID", ObjLoginEL.SessionId);
                command.Parameters.AddRange(param);
                DataManager dm = new DataManager();
                i = dm.ExecuteScaler("sp_UserLogin", command);
            }*/
            MySqlConnection connect = getConnection();
            int i = 0;
            try
            {
                using (connect)
                {
                    MySqlCommand cmd = new MySqlCommand("sp_UserLogin", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_Login_Username", ObjLoginEL.UserName);
                    cmd.Parameters.AddWithValue("I_Session_ID", ObjLoginEL.SessionId);
                    con.Open();
                    i = Convert.ToInt32(cmd.ExecuteScalar());
                }                
            }
            catch (Exception ex)
            {
                string methodname = MethodBase.GetCurrentMethod().Name;
                Task.Factory.StartNew(delegate
                {
                    logs.WriteDBError(ObjLoginEL.UserName,ex, "sp_UserLogin", methodname, LogType.Error,LogFiles.INDIMPS_DB_Error);
                });
                i = -1;
            }
            finally
            {
                //param = null;
            }
            return i;
        }
    }
}
