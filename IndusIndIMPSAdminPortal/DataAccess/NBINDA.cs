﻿using EntityLayer;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace DataAccess
{
   public class NBINDA
    {
        MySqlConnection con;

        public MySqlConnection getConnection()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            var config = builder.Build();
            //con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("DevConnection").Value);
            con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("IndusIndAdminConStr").Value);
            return con;
        }

        public DataTable GetNBIN(int? NBIN_Id)
        {
            DataTable DT = new DataTable();
            try
            {
                MySqlCommand command = new MySqlCommand();
                command.Parameters.AddWithValue("I_NBIN_Id", NBIN_Id);
                DataManager dm = new DataManager();

                DT = dm.GetDataTable("sp_GetNBIN", command);
            }
            catch (Exception ex)
            {
                //Generate Log here
                DT = null;
            }
            return DT;
        }

        public Response CreatNBIN(NBINEL data)
        {
            Response respo = new Response();
            MySqlConnection connect = getConnection();
            int response = 0;
            int InsertCode = 0;
            try
            {
                using (connect)
                {
                    MySqlCommand cmd = new MySqlCommand("sp_InsertUpdateNBIN", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_Bank_Id",InsertCode);
                    cmd.Parameters.AddWithValue("I_Bank_Name", data.BANKNAME.ToUpper());
                    cmd.Parameters.AddWithValue("I_IFSC_CODE", data.IFSCCODE.ToUpper());
                    cmd.Parameters.AddWithValue("I_NBIN", data.NBIN.ToUpper());
                    cmd.Parameters.AddWithValue("I_SHORTCODE", data.SHORTCODE.ToUpper());
                    cmd.Parameters.AddWithValue("I_Creater", 1009);
                    con.Open();
                    response = Convert.ToInt32(cmd.ExecuteScalar());
                    if (response > -1)
                    {
                        respo = MesgForUpdate(response);
                        return respo;
                    }
                }
            }
            catch (Exception ex)
            {
                respo.ResponseMessage = "Task can't be performed due to some error";
                respo.ResponseStatus = false;

            }
            return respo;
        }
        public NBINViewModel getAllNBIN()
        {
            MySqlConnection connect = getConnection();
            NBINViewModel model = new NBINViewModel();
            model.ListNBIN = new List<NBINEL>();
            

            MySqlCommand cmd = new MySqlCommand("sp_GetNBIN", connect);
            cmd.CommandType = CommandType.StoredProcedure;
            connect.Open();
            using (MySqlDataReader dataReader = cmd.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    NBINEL obj = new NBINEL();
                    obj.BANKID = Convert.ToInt32(dataReader["BANKID"]);
                    obj.BANKNAME = Convert.ToString(dataReader["BANKNAME"]);
                    obj.SHORTCODE = Convert.ToString(dataReader["SHORTCODE"]);
                    obj.NBIN = Convert.ToString(dataReader["NBIN"]);
                    obj.IFSCCODE = Convert.ToString(dataReader["IFSCCODE"]);
                    obj.STATUS = Convert.ToChar(dataReader["STATUS"]);
                    obj.CREATEDBY = Convert.ToInt32(dataReader["CREATEDBY"]);
                    obj.ENABLESTATUS= Convert.ToInt32(dataReader["ENABLESTATUS"]);
                    model.ListNBIN.Add(obj);
                }

            }
            return model;
        }
        //Get EditData
        public Response EditNBIN(int? BankID)
        {
            NBINEL obj = new NBINEL();
            DataTable dt = new DataTable();
            Response response = new Response();
            try
            {
                using (MySqlConnection connect = getConnection())
                {
                    connect.Open();
                    MySqlCommand cmd = new MySqlCommand("sp_EditNBIN", connect);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_BANKID", BankID);
                    DataManager dm = new DataManager();
                    dt = dm.GetDataTable("sp_EditNBIN", cmd);
                    if (dt.Rows.Count > 0 || dt != null)
                    {
                        obj.BANKID = Convert.ToInt32(dt.Rows[0]["BANKID"]);
                        obj.BANKNAME = Convert.ToString(dt.Rows[0]["BANKNAME"]);
                        obj.SHORTCODE = Convert.ToString(dt.Rows[0]["SHORTCODE"]);
                        obj.NBIN = Convert.ToString(dt.Rows[0]["NBIN"]);
                        obj.IFSCCODE = Convert.ToString(dt.Rows[0]["IFSCCODE"]);
                        //
                        response.ResponseData = obj;
                        response.ResponseMessage = "Success";
                        response.ResponseStatus = true;
                    }
                    using (MySqlDataReader dataReader = cmd.ExecuteReader())
                    {

                        while (dataReader.Read())
                        {
                            obj.BANKID = Convert.ToInt32(dataReader["BANKID"]);
                            obj.BANKNAME = Convert.ToString(dataReader["BANKNAME"]);
                            obj.SHORTCODE = Convert.ToString(dataReader["SHORTCODE"]);
                            obj.NBIN = Convert.ToString(dataReader["NBIN"]);
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                response.ResponseStatus = true;
                response.ResponseMessage = "Task can't be performed due to some error";
            }
            return response;

        }
        public Response UpdateNBIN(NBINEL data)
        {
            Response res = new Response();
            try
            {
                MySqlConnection connect = getConnection();
                using (connect)
                {
                    MySqlCommand cmd = new MySqlCommand("sp_InsertUpdateNBIN", connect);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_Bank_Id", 21);//default id use for Updation
                    cmd.Parameters.AddWithValue("I_Bank_Name", data.BANKNAME.ToUpper());
                    cmd.Parameters.AddWithValue("I_IFSC_CODE", data.IFSCCODE.ToUpper());
                    cmd.Parameters.AddWithValue("I_NBIN", data.NBIN.ToUpper());
                    cmd.Parameters.AddWithValue("I_SHORTCODE", data.SHORTCODE.ToUpper());
                    cmd.Parameters.AddWithValue("I_Creater", 76555);
                    con.Open();
                    int i=Convert.ToInt32( cmd.ExecuteScalar());
                    if (i >-1)
                    {

                        //This is for Update
                        res = MesgForUpdate(i);
                        /*res.ResponseStatus = true;
                        res.ResponseMessage = "NBIN Updated successfully";*/
                        return res;
                    }
                    else
                    {
                        res.ResponseStatus = false;
                        res.ResponseMessage = "NBIN not Updated";
                    }

                }
            }
            catch (Exception ex)
            {
                res.ResponseStatus = false;
                res.ResponseMessage = "Task can't be performed due to some error";
            }

            return res;

        }

        //For Approval
        public Response ApproveNBIN(int? idNBIN)
        {
            Response res = new Response();
            try
            {
                
            }
            catch (Exception ex)
            {
                res.ResponseStatus = false;
                res.ResponseMessage = "Task can't be performed due to some error";
            }

            return res;

        }


        //For response messages
        private Response MesgForUpdate(int? i)
        {
            Response res = new Response();

            switch (i)
            {
                case 4:
                    res.ResponseMessage = "Data is already Exist";
                    res.ResponseStatus = true;
                    break;
                case 2:
                    res.ResponseMessage = "Data is already Disabled";
                    res.ResponseStatus = true;
                    break;
                case 0:
                    res.ResponseMessage = "NBIN is Created/Updated Successfully";
                    res.ResponseStatus = true;
                    break;
                case 3:
                    res.ResponseMessage = "Already waiting for Approval";
                    res.ResponseStatus = true;
                    break;
                case -1:
                    res.ResponseMessage = "Server not responding";
                    break;
            }
            return res;
        }

        //For Checker for NBIN
        public Response CheckNBIN(string nbin, int checker, string feedback)
        {
            Response res = new Response();

            try {
                MySqlConnection connect = getConnection();
                using (connect)
                {
                    MySqlCommand cmd = new MySqlCommand("sp_CheckerNBIN", connect);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_Checker", checker);
                    cmd.Parameters.AddWithValue("I_NBIN", nbin);
                    cmd.Parameters.AddWithValue("I_RejectReason", feedback);
                    connect.Open();
                    int i = Convert.ToInt32(cmd.ExecuteScalar());
                    
                    if (i > 0)
                    {
                        //This is for Approval
                        res = MesgForChecker(i);
                        return res;
                    }
                    else
                    {
                        res.ResponseStatus = false;
                        res.ResponseMessage = "Some went wrong!!!";
                    }

                }

            }
            catch (Exception ex)
            {
                res.ResponseMessage = "Task can't be performed due to some error";
                res.ResponseStatus = false;
            }
            return res;
        }
        //For NBIN checker message
        private Response MesgForChecker(int? i)
        {
            Response res = new Response();

            switch (i)
            {
                case 1:
                    res.ResponseMessage = "NBIN approved Successfully";
                    res.ResponseStatus = true;
                    break;

                case 5:
                    res.ResponseMessage = "Maker can't Check";
                    res.ResponseStatus = true;
                    break;

                case 4:
                    res.ResponseMessage = "This NBIN is alredy Exist";
                    res.ResponseStatus = true;
                    break;

                case 2:
                    res.ResponseMessage = "This NBIN is Rejected";
                    res.ResponseStatus = true;
                    break;

                case 3:
                    res.ResponseMessage = "This NBIN is waiting for Approval";
                    res.ResponseStatus = true;
                    break;

                default:
                    res.ResponseMessage = "Server is not Responding";
                    break;
            }
            return res;
        }
    }
}
