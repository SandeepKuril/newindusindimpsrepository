﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.IO;


namespace DataAccess
{
    internal sealed class DataManager
    {
        static MySqlConnection connection;
        public DataManager()
        {
            try
            {
                connection = GetConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MySqlConnection GetConnection()
        {
            MySqlConnection con;
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            var config = builder.Build();
            con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("IndusIndAdminConStr").Value);
            return con;

        }

        #region Common Procedure Methods
        public int ExecuteSP(string procedureName, MySqlCommand command)
        {
            int i = 0;
            try
            {
                command.CommandText = procedureName;
                command.Connection = connection;

                //Mark As Stored Procedure
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();
                i = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
                command.Dispose();
                connection.Dispose();
            }
            return i;
        }

        public int ExecuteSQL(string strQuery, MySqlCommand command)
        {
            int i = 0;
            try
            {

                command.CommandText = strQuery;
                command.Connection = connection;

                //Mark As Stored Procedure
                command.CommandType = CommandType.Text;

                connection.Open();
                i = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                command.Dispose();
                connection.Dispose();
            }
            return i;
        }

        public int ExecuteSQL(string strQuery)
        {
            MySqlCommand command = new MySqlCommand();
            int i = 0;

            try
            {

                command.CommandText = strQuery;
                command.Connection = connection;

                //Mark As Stored Procedure
                command.CommandType = CommandType.Text;

                connection.Open();
                i = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                command.Dispose();
                connection.Dispose();
            }
            return i;
        }

        //get count returned by SP
        public int ExecuteScaler(string procedureName, MySqlCommand command)
        {
            try
            {
                command.CommandText = procedureName;
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //get count returned by SP
        public string ExecuteScalarString(string procedureName, MySqlCommand command)
        {
            try
            {
                command.CommandText = procedureName;
                command.Connection = connection;

                //Mark As Stored Procedure
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();
                return Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                command.Dispose();
                connection.Dispose();
            }
        }

        //get count returned by SP
        public Int32 ExecuteScalar(string procedureName, MySqlCommand command)
        {
            Int32 intReturnValue = 0;
            try
            {
                command.CommandText = procedureName;
                command.Connection = connection;

                //Mark As Stored Procedure
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();
                intReturnValue = Convert.ToInt32(command.ExecuteScalar());
                connection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                command.Dispose();
                connection.Dispose();
            }

            return intReturnValue;
        }

        //get count returned by SP
        public string ExecuteScalarString(string sql)
        {
            MySqlCommand command = new MySqlCommand();
            string retValue = "";

            try
            {
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                command.Connection = connection;


                connection.Open();
                retValue = Convert.ToString(command.ExecuteScalar());
                connection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                command.Dispose();
                connection.Dispose();
            }
            return retValue;
        }

        //get count returned by SP
        public Int32 ExecuteScalar(string sql)
        {
            MySqlCommand command = new MySqlCommand();
            Int32 retValue = 0;

            try
            {
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                command.Connection = connection;

                connection.Open();
                retValue = Convert.ToInt32(command.ExecuteScalar());
                connection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                command.Dispose();
                connection.Dispose();
            }
            return retValue;
        }

        //Execute Stored Procedure And Returns Dataview as a resultset
        public DataTable GetDataTable(string procedureName, MySqlCommand command)
        {
            DataSet ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter();
            DataTable table = null;
            try
            {
                //command.CommandTimeout = 1000;
                command.CommandTimeout = 0;
                command.CommandText = procedureName;
                command.Connection = connection;
                connection.Open();
                //Mark As Stored Procedure
                command.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = command;
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Connection.Close();
                command.Connection.Dispose();
                command.Dispose();
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null)
            {
                table = ds.Tables[0];
            }
            return table;
        }

        //Execute Stored Procedure And Returns Dataview as a resultset
        public DataTable GetDataTable(string procedureName, MySqlCommand command, string tableName)
        {
            MySqlDataAdapter da = new MySqlDataAdapter();
            DataTable dt = new DataTable();


            try
            {
                command.CommandText = procedureName;
                connection.Open();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;

                da.SelectCommand = command;


                da.Fill(dt);
                dt.TableName = tableName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Connection.Close();
                command.Connection.Dispose();
                command.Dispose();
                if (connection.State == ConnectionState.Open) connection.Close();
            }
            return dt;
        }

        public DataTable GetDataTable(string sqlQuery, string TableName)
        {
            DataTable datatable;
            try
            {
                connection.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(sqlQuery, connection);
                datatable = new DataTable();
                da.Fill(datatable);
                datatable.TableName = TableName.Trim();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
            }

            return datatable;

        }

        public MySqlDataAdapter GetDataAdapter(string procedureName, MySqlCommand command)
        {
            MySqlDataAdapter da = new MySqlDataAdapter();

            try
            {
                command.CommandText = procedureName;
                command.Connection = connection;
                //Mark As Stored Procedure
                command.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = command;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Connection.Close();
                command.Connection.Dispose();
                command.Dispose();
                if (connection.State == ConnectionState.Open) connection.Close();
            }
            return da;
        }

        public DataSet GetDataSet(string procedureName, MySqlCommand command)
        {
            DataSet ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter();

            try
            {
                command.CommandText = procedureName;
                command.Connection = connection;
                connection.Open();

                //Mark As Stored Procedure
                command.CommandType = CommandType.StoredProcedure;

                da.SelectCommand = command;
                da.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
            }
            return ds;
        }

        internal void Commit()
        {
            throw new NotImplementedException();
        }

        #endregion


    }
}