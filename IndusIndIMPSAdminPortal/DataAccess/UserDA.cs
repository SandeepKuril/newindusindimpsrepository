﻿using EntityLayer;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace DataAccess
{
    public class UserDA
    {
        //MySqlConnection con;

        //public MySqlConnection getConnection()
        //{
        //    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
        //    var config = builder.Build();
        //    //con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("DevConnection").Value);
        //    con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("IndusIndAdminConStr").Value);
        //    return con;
        //}

        public List<UserEL> Users
        {
            get
            {
                //UserViewModel userInfo = new UserViewModel();
                //userInfo.UserList = new List<UserEL>();
                List<UserEL> userInfo = new List<UserEL>();
                try
                {
                    DataManager dm = new DataManager();
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Parameters.AddWithValue("I_User_Id", DBNull.Value);
                    //cmd.Parameters["@I_I_User_Id"].IsNullable = true;
                    cmd.Parameters.AddWithValue("I_Action", "Select");
                    DataTable dt = dm.GetDataTable("sp_GetUser", cmd);


                    foreach (DataRow dr in dt.Rows)
                    {
                        UserEL user = new UserEL();
                        user.User_Id = Convert.ToInt32(dr["User_Id"]);
                        user.Login_Username = dr["Login_Username"].ToString();
                        user.Role_Name = dr["Role_Name"].ToString();
                        userInfo.Add(user);
                    }
                }
                catch (Exception expt)
                {
                    Console.WriteLine("Exception" + expt);
                }

                return userInfo;
            }
        }

        public DataTable GetUserById(UserEL data)
        {
            //UserViewModel userInfo = new UserViewModel();
            //userInfo.UserData = new UserEL();
            List<UserEL> userInfo = new List<UserEL>();
            DataTable DT = new DataTable();
            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_User_Id", Convert.ToInt32(data.User_Id));
                cmd.Parameters.AddWithValue("I_Action", "SelectById");

                DT = dm.GetDataTable("sp_GetUser", cmd);
                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    UserEL User = new UserEL();
                //    User.User_Id = Convert.ToInt32(dt.Rows[0]["User_Id"]);
                //    User.Login_Username = dt.Rows[0]["Login_Username"].ToString();
                //    User.Role_Name = dt.Rows[0]["Role_Name"].ToString();
                //    userInfo.Add(User);
                //}
            }
            catch (Exception expt)
            {
                Console.WriteLine("Exception" + expt);
                DT = null;
            }
            return DT;
        }


        public int createUser(UserEL data)
        {
            int res;
            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_User_Id", DBNull.Value);
                cmd.Parameters.AddWithValue("I_Login_Username", data.Login_Username);
                cmd.Parameters.AddWithValue("I_Role_Name", data.Role_Name);
                cmd.Parameters.AddWithValue("I_Client_IP", data.Client_IP);
                cmd.Parameters.AddWithValue("I_Action", "Insert");
                res = dm.ExecuteScaler("sp_InsertUpdateUser", cmd);
            }
            catch (Exception expt)
            {
                res = -1;
                Console.WriteLine("Exception" + expt);
            }
            return res;
        }

        public int updateUser(UserEL data)
        {
            //MySqlConnection connect = getConnection();
            int res;

            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_User_Id", data.User_Id);
                cmd.Parameters.AddWithValue("I_Login_Username", data.Login_Username);
                cmd.Parameters.AddWithValue("I_Role_Name", data.Role_Name);
                cmd.Parameters.AddWithValue("I_Action", "Update");
                res = dm.ExecuteScaler("sp_InsertUpdateUser", cmd);
            }
            catch (Exception expt)
            {
                res = -1;
                Console.WriteLine("Exception" + expt);
            }

            return res;
        }


        public int deleteUser(UserEL data)
        {
            //MySqlConnection connect = getConnection();
            int res;

            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_User_Id", data.User_Id);
                res = dm.ExecuteScaler("sp_DeleteUser", cmd);
            }
            catch (Exception expt)
            {
                res = -1;
                Console.WriteLine("Exception" + expt);
            }
            return res;
        }

    }
}
