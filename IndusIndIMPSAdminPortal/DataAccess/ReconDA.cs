﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using EntityLayer;
using MySql.Data.MySqlClient;
using CommonLayer;
using System.Linq;
using FastMember;
using System.IO;

namespace DataAccess
{
    public class ReconDA
    {

        #region DATE FORMAT
        private string getDateFormat(string date)
        {
            string[] str = null;
            str = date.Split('/');
            string dd = str[0];
            string mm = str[1];
            string yyyy = str[2];
            if (mm.Length != 2)
            {
                mm = "0" + mm;
            }
            if (dd.Length != 2)
            {
                dd = "0" + dd;
            }
            //YYYY-MM-DD Format
            return yyyy + "-" + mm + "-" + dd;
        }
        #endregion

        #region 1-insertNTSLFileInReconFileMaster 
        //public bool insertNTSLFileInReconFileMaster(ReconFilemaster fileData, out string errorMessage, out long reconFileId)
        public bool insertNTSLFileInReconFileMaster(ReconFilemaster fileData, out string errorMessage, out long reconFileId)
        {
            DateTime startTime = DateTime.Now;
            reconFileId = -1;
            Int32 retStatus = -1;
            errorMessage = string.Empty;
            bool isSuccess = false;
            try
            {
                //fileData = (ReconFilemaster)AntiXSS.cleanXSS(fileData);
                //Remove XSS malicius file 
                //fileData = (ReconFilemaster)AntiXSS.cleanXSSUsingHtmlEncode(fileData);

                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();

                cmd.Parameters.AddWithValue("I_File_Name", fileData.NTSLFileName);
                cmd.Parameters.AddWithValue("I_File_Path", fileData.NTSLFilePath);
                cmd.Parameters.AddWithValue("I_Uploaded_By", 1001);
                cmd.Parameters.AddWithValue("I_Cycle_Time", fileData.ReconCycle);
                cmd.Parameters.AddWithValue("I_File_Date", getDateFormat(fileData.ReconDate));
                cmd.Parameters.Add("O_Status", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("O_Recon_File_Id", MySqlDbType.Int32).Direction = ParameterDirection.Output;

                dm.ExecuteScaler("sp_InsertFileInReconFileMaster", cmd);

                retStatus = Convert.ToInt32(cmd.Parameters["O_Status"].Value);

                if (retStatus == 0)
                {
                    reconFileId = Convert.ToInt32(cmd.Parameters["O_Recon_File_Id"].Value);
                    if (reconFileId > 0)
                    {
                        isSuccess = true;
                    }
                    else
                    {
                        errorMessage = ConstantResponseMsg.Fail;
                    }
                }
                else if (retStatus == -1)
                {
                    errorMessage = ConstantResponseMsg.FailureDuringDBOperation;
                }
                else if (retStatus == 1)
                {
                    errorMessage = ConstantResponseMsg.FailedWhileInserting;
                }
                else if (retStatus == 2)
                {
                    errorMessage = ConstantResponseMsg.FileNameAlreadyExistsInDB;
                }
                //else if (retStatus == 3)
                //{
                //    errorMessage = ConstantResponseMsg.AlreadyUploadedAll;
                //}
                else if (retStatus == 4)
                {
                    errorMessage = ConstantResponseMsg.AlreadyUploaded2c;
                }
                else if (retStatus == 5)
                {
                    errorMessage = ConstantResponseMsg.Select2CFirst;
                }
                else if (retStatus == 6)
                {
                    errorMessage = ConstantResponseMsg.Select3CFirst;
                }
                else if (retStatus == 7)
                {
                    errorMessage = ConstantResponseMsg.Select4CFirst;
                }
                else if (retStatus == 8)
                {
                    errorMessage = ConstantResponseMsg.Select1CFirst;
                }
                else if (retStatus == 9)
                {
                    errorMessage = ConstantResponseMsg.SelectPrevDate3C;
                }
                else if (retStatus == 10)
                {
                    errorMessage = ConstantResponseMsg.SelectPrevDate4C;
                }
                else if (retStatus == 11)
                {
                    errorMessage = ConstantResponseMsg.SelectPrevDate1C;
                }
                else if (retStatus == 12)
                {
                    errorMessage = ConstantResponseMsg.SelectPrevDateFile;
                }
                else if (retStatus == 13)
                {
                    errorMessage = ConstantResponseMsg.ReconNotDonePrevFile;
                }

                #region LOG
                //string logMessage = "ReconDAL - " + "insertNTSLFileInReconFileMaster" + ", Status: " + errorMessage
                //                    + ", ReconId:" + Convert.ToString(reconFileId) + "Recon Date: " + fileData.ReconDate
                //                    + ", User Id:" + ((fileData == null) ? "" : Convert.ToString(fileData.UploadedBy));
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion
            }
            catch (Exception ex)
            {
                //errorMessage = Messages.InternalServerError;
                //#region EXCEPTION LOG
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError((((fileData != null) ? Convert.ToString(fileData.UploadedBy) : ""))
                //  , ex.Message + " - " + ex.ToString()
                //  , "SPINSRT_FILE_IN_RECONFILEMSTR:", "ReconDAL:", "insertNTSLFileInReconFileMaster");
                //});
                //#endregion
            }
            finally
            {
                //if (oConn.State == ConnectionState.Open)
                //{
                //    oConn.Close();
                //}
                //#region FINAL LOG
                //string reconId = Convert.ToString(reconFileId);
                //string msg = errorMessage;
                //Task.Factory.StartNew(delegate
                //{

                //    LogHelper.WriteReconLog((fileData == null) ? "" : Convert.ToString(fileData.UploadedBy),
                //            "Status : " + retStatus.ToString() + ", Message: " + msg, " ReconDAL:", "insertNTSLFileInReconFileMaster ",
                //            Convert.ToString(reconId), startTime, DateTime.Now);
                //});
                //#endregion
                //oCmd = null; oConn = null;
            }
            return isSuccess;
        }
        #endregion

        #region 2-insertNTSLData
        public bool insertNTSLData(List<NTSLData> bulkData)
        {
            DateTime startTime = DateTime.Now;
            string Result = string.Empty;
            bool retVal = false;

            DataManager dm = new DataManager();
            MySqlCommand cmd = new MySqlCommand();

            long CreatedBY = 0;
            long Recon_Id = 0;
            string ErrorMessage = string.Empty;
            try
            {
                CreatedBY = bulkData[0].CreatedBy;
                Recon_Id = bulkData[0].ReconMstrId;

                #region 

                MySqlConnection con = dm.GetConnection();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_InsertNTSLData";
                con.Open();

                for (int i = 0; i < bulkData.Count; i++)
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("I_Created_By", bulkData[i].CreatedBy);
                    cmd.Parameters.AddWithValue("I_Recon_Id", bulkData[i].ReconMstrId);
                    cmd.Parameters.AddWithValue("I_Description", bulkData[i].Description);
                    cmd.Parameters.AddWithValue("I_No_Of_Txns", bulkData[i].NoOfTransactions);
                    cmd.Parameters.AddWithValue("I_Debit", bulkData[i].Debit);
                    cmd.Parameters.AddWithValue("I_Credit", bulkData[i].Credit);
                    cmd.Parameters.Add("O_Status", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("O_Id", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    //dm.ExecuteScaler("sp_InsertNTSLData", cmd);
                    cmd.ExecuteScalar();

                    Result += "[ NTSL_FILE_DATA_ID:" + Convert.ToString(cmd.Parameters["O_Id"].Value) + " - Status:" + ((Convert.ToInt32(cmd.Parameters["O_Status"].Value) == 0) ? "Success" : "Failed") + " ] ,";
                }

                Result = Result.TrimEnd(',');
                con.Close();
                retVal = true;

                //Oracle.ManagedDataAccess.Types.OracleDecimal[] objarrayResult = (Oracle.ManagedDataAccess.Types.OracleDecimal[])OCmd.Parameters["O_STATUS"].Value;
                //Oracle.ManagedDataAccess.Types.OracleDecimal[] objarrayDataId = (Oracle.ManagedDataAccess.Types.OracleDecimal[])OCmd.Parameters["O_ID"].Value;
                ////

                #region LOG
                //string logMessage = "ReconDAL - " + "insertNTSLData" + ",  , Response: " + ""
                //                    + ", ReconId:" + Convert.ToString(Recon_Id)
                //                    + ", Inserted By:" + ((CreatedBY > 0) ? Convert.ToString(CreatedBY) : "");
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion
                #endregion

            }
            catch (Exception ex)
            {
                #region EXCEPTION LOG
                //ErrorMessage = "Error : RDAL2 : " + Messages.InternalServerError;
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError((((CreatedBY > 0) ? Convert.ToString(CreatedBY) : ""))
                //  , ex.Message + " - " + ex.ToString()
                //  , "SPIMPS_INSERT_NTSL_DATA:", "ReconDAL:", "insertNTSLData");
                //});
                #endregion
            }
            finally
            {

                #region FINAL LOG
                //DateTime endTime = DateTime.Now;
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog((CreatedBY > 0) ? "" : Convert.ToString(CreatedBY),
                //            "Status : " + retVal.ToString() + ", Message: " + ErrorMessage, " ReconDAL:", "insertNTSLData ",
                //            (Recon_Id > 0) ? Convert.ToString(Recon_Id) : "", startTime, endTime);
                //});
                #endregion
                //OConn = null; OCmd = null; bulkData = null;
            }
            return retVal;
        }
        #endregion

        #region 3-updateRAWFileInReconFileMaster
        public bool updateRAWFileInReconFileMaster(ReconFilemaster fileData, out string errorMessage, long reconId)
        {
            DateTime startTime = DateTime.Now;
            Int32 retStatus = -1;
            errorMessage = string.Empty;
            bool isSuccess = false;
            try
            {
                //Avoid xss
                //fileData = (ReconFilemaster)AntiXSS.cleanXSS(fileData);
                //Remove XSS malicius file 
                //fileData = (ReconFilemaster)AntiXSS.cleanXSSUsingHtmlEncode(fileData);

                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();

                cmd.Parameters.AddWithValue("I_Recon_Id", reconId);
                cmd.Parameters.AddWithValue("I_File_Name", fileData.RAWFileName);
                cmd.Parameters.AddWithValue("I_File_Path", fileData.RAWFilePath);
                cmd.Parameters.AddWithValue("I_File_Date", getDateFormat(fileData.ReconDate));
                cmd.Parameters.AddWithValue("I_Recon_Cycle", fileData.ReconCycle);
                cmd.Parameters.AddWithValue("I_Uploaded_By", 1001);
                cmd.Parameters.Add("O_Status", MySqlDbType.Int32).Direction = ParameterDirection.Output;


                dm.ExecuteScaler("sp_InsertRowFileInRconFleMstr", cmd);

                retStatus = Convert.ToInt32(cmd.Parameters["O_Status"].Value.ToString());
                //
                if (retStatus == 0)
                {
                    isSuccess = true;
                    errorMessage = "Success";
                }
                if (retStatus == 1)
                {
                    errorMessage = "Unable to insert raw file details due to Id not exist";
                }
                if (retStatus == -1)
                {
                    errorMessage = ConstantResponseMsg.FailureDuringDBOperation;
                }

                #region LOG
                string logMessage = "ReconDAL - " + "updateRAWFileInReconFileMaster" + ", Status: " + errorMessage
                                    + ", ReconId:" + Convert.ToString(reconId)
                                    + ", UserId:" + ((fileData != null) ? Convert.ToString(fileData.UploadedBy) : "");
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion

            }
            catch (Exception ex)
            {
                errorMessage = ConstantResponseMsg.InternalServerError;
                #region EXCEPTION LOG
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError((((fileData != null) ? Convert.ToString(fileData.UploadedBy) : ""))
                //  , ex.Message + " - " + ex.ToString()
                //  , "SPINSRTROW_FILE_IN_RCONFLEMSTR:", "ReconDAL:", "updateRAWFileInReconFileMaster");
                //});
                #endregion
            }
            finally
            {
                //if (oConn.State == ConnectionState.Open)
                //{
                //    oConn.Close();
                //}
                #region FINAL LOG
                string msg = errorMessage;
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog((fileData == null) ? "" : Convert.ToString(fileData.UploadedBy),
                //            "Status : " + retStatus.ToString() + ", Message: " + msg, " ReconDAL:", "updateRAWFileInReconFileMaster ",
                //            (reconId > 0) ? Convert.ToString(reconId) : "", startTime, DateTime.Now);
                //});
                #endregion
                //oCmd = null;
            }
            return isSuccess;

        }
        #endregion

        #region 4-insertRawFileData 
        public bool insertRawFileData(List<RawDataFile> bulkData, long UserId, Int64 reconId, out string ErrorMsg)
        {
            DateTime startTime = DateTime.Now;
            bool retVal = false;
            ErrorMsg = string.Empty;
            string message = string.Empty;
            try
            {
                //Remove xss
                //bulkData = (List<RawData>)AntiXSS.cleanXSS(bulkData);

                int rowCount = bulkData.Count;
                int successCount = 0;

                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                MySqlConnection con = dm.GetConnection();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_InsertRAWData";
                con.Open();

                MySqlTransaction tran = con.BeginTransaction();

                for (int i = 0; i < bulkData.Count; i++)
                {
                    //cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("I_Recon_Id", reconId);
                    cmd.Parameters.AddWithValue("I_ParticipantId", bulkData[i].ParticipantID);
                    cmd.Parameters.AddWithValue("I_TransactionType", bulkData[i].TransactionType);
                    cmd.Parameters.AddWithValue("I_FromAccountType", bulkData[i].FromAccountType);
                    cmd.Parameters.AddWithValue("I_ToAccountType", bulkData[i].ToAccountType);
                    cmd.Parameters.AddWithValue("I_RRN", bulkData[i].TransactionSerialNumber);
                    cmd.Parameters.AddWithValue("I_Status", bulkData[i].ResponseCode);
                    cmd.Parameters.AddWithValue("I_Pannumber", bulkData[i].PANNumber);
                    cmd.Parameters.AddWithValue("I_MemberNumber", bulkData[i].MemberNumber);
                    cmd.Parameters.AddWithValue("I_ApprovalNumber", bulkData[i].ApprovalNumber);
                    cmd.Parameters.AddWithValue("I_SystemTraceAuditNumber", bulkData[i].SystemTraceAuditNumber);
                    cmd.Parameters.AddWithValue("I_Txn_Date", bulkData[i].TransactionDate);
                    cmd.Parameters.AddWithValue("I_Txn_Time", bulkData[i].TransactionTime);
                    cmd.Parameters.AddWithValue("I_MerchantCategoryCode", bulkData[i].MerchantCategoryCode);
                    cmd.Parameters.AddWithValue("I_CardAcceptorSettlDate", bulkData[i].CardAcceptorSettlDate);
                    cmd.Parameters.AddWithValue("I_CardAcceptorId", bulkData[i].CardAcceptorID);
                    cmd.Parameters.AddWithValue("I_CardAcceptorterminalId", bulkData[i].CardAcceptorTerminalID);
                    cmd.Parameters.AddWithValue("I_CardAcceptorTermLocation", bulkData[i].CardAcceptorTermLocation);
                    cmd.Parameters.AddWithValue("I_AquirerId", bulkData[i].AquirerID);
                    cmd.Parameters.AddWithValue("I_AcquirerSettlementDate", bulkData[i].AcquirerSettlementDate);
                    cmd.Parameters.AddWithValue("I_TransactionCurrencyCode", bulkData[i].TransactionCurrencyCode);
                    cmd.Parameters.AddWithValue("I_TransactionAmount", bulkData[i].TransactionAmount);
                    cmd.Parameters.AddWithValue("I_ActualTransactionAmount", bulkData[i].ActualTransactionAmount);
                    cmd.Parameters.AddWithValue("I_TransactivityFee", bulkData[i].TransActivityFee);
                    cmd.Parameters.AddWithValue("I_AcquirerStlCurrCode", bulkData[i].AcquirerStlCurrCode);
                    cmd.Parameters.AddWithValue("I_AcquirerSettlementAmount", bulkData[i].AcquirerSettlementAmount);
                    cmd.Parameters.AddWithValue("I_AcquirerStlFee", bulkData[i].AcquirerStlFee);
                    cmd.Parameters.AddWithValue("I_AcquirerStlProcFee", bulkData[i].AcquirerStlProcFee);
                    cmd.Parameters.AddWithValue("I_AcquirerStlConvRate", bulkData[i].AcquirerStlConvRate);
                    cmd.Parameters.AddWithValue("I_PaymentReference", bulkData[i].PaymentReference);
                    cmd.Parameters.AddWithValue("I_Custinfo_Ifsc_Acno", bulkData[i].Unknown);

                    int Val = 0;
                    Val = cmd.ExecuteNonQuery();
                    if (Val > 0)
                    {
                        successCount++;
                    }
                    cmd.Parameters.Clear();
                }

                //con.Close();
                if (rowCount == successCount)
                {
                    tran.Commit();
                    con.Close();
                    //retVal = true;                   
                    ErrorMsg = "Success";
                    retVal = insertNtslRawTxnSumCount(reconId, out message);
                    if (!retVal)
                        ErrorMsg = message;
                }
                else
                {
                    ErrorMsg = "Raw data could not be inserted because Raw data count mismatch while inserting in table";
                    tran.Rollback();
                }

                #region LOG
                string logMessage = "ReconDAL - " + "insertRawFileData , [INSERT RAW DATA]" + ",  , Status: " + ErrorMsg
                                    + ", ReconId:" + Convert.ToString(reconId)
                                    + ", Inserted By:" + ((UserId > 0) ? Convert.ToString(UserId) : "");
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion
            }
            catch (Exception ex)
            {
                //tran.Rollback();
                ErrorMsg = "Error : RDAL3 :  Raw data could not be inserted due to exception occured";

                #region EXCEPTION LOG
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError((((UserId > 0) ? Convert.ToString(UserId) : ""))
                //  , ex.Message + " - " + ex.ToString()
                //  , "INLINE QUERY USED:", "ReconDAL:", "insertRawFileData");
                //});
                #endregion
            }
            finally
            {
                //string msg = ErrorMsg;
                //if (OConn.State == ConnectionState.Open)
                //{
                //    OConn.Close();
                //}

                //#region FINAL LOG
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog((UserId > 0) ? "" : Convert.ToString(UserId),
                //            "Status : " + retVal.ToString() + ", Message: " + msg, " ReconDAL:", "insertRawFileData ",
                //            (reconId > 0) ? Convert.ToString(reconId) : "", startTime, DateTime.Now);
                //});
                //#endregion

                //OConn = null; OCmd = null;
                //bulkData = null;
            }
            return retVal;
        }
        #endregion

        #region 5-insertNtslRawTxnSumCount
        public bool insertNtslRawTxnSumCount(Int64 ReconId, out string errorMsg)
        {
            DateTime startTime = DateTime.Now;
            Int32 retValue = -1;
            bool isSuccess = false;
            errorMsg = string.Empty;
            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();

                cmd.Parameters.AddWithValue("I_Recon_File_Id", ReconId);
                cmd.Parameters.Add("O_Status", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                dm.ExecuteScaler("sp_UpdateTotalTransactionAmount", cmd);
                retValue = Convert.ToInt32(cmd.Parameters["O_Status"].Value.ToString());
                //if (retValue == 0)
                //{
                //    isSuccess = true;
                //}
                if (retValue == 0)
                {
                    isSuccess = true;
                    errorMsg = "Success";
                }
                if (retValue == 1)
                {
                    errorMsg = "Error occured while updating NTSL and Raw sum and count";
                }
                if (retValue == 2)
                {
                    errorMsg = "Failed: NTSL and Raw not matched";
                }
                if (retValue == 3)
                {
                    errorMsg = "Recon Id is not found in Raw File data table while checking count ands sum";
                }
                if (retValue == -1)
                {
                    errorMsg = ConstantResponseMsg.FailureDuringDBOperation;
                }


                #region LOG
                string logMessage = "ReconDAL - " + "insertNtslRawTxnSumCount" + ", Status: " + ((isSuccess == true) ? "Success" : "Failed"
                                    + ", ReconId:" + Convert.ToString(ReconId)
                                    + ", UserId By: NA");
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion
            }
            catch (Exception ex)
            {
                #region EXCEPTION LOG
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError("NA", ex.Message + " - " + ex.ToString()
                //  , "SP_GET_TOTALAMOUNTTRANSACTION:", "ReconDAL:", "insertNtslRawTxnSumCount");
                //});
                #endregion
            }
            finally
            {
                #region FINAL LOG
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog("NA", "Status :" + Convert.ToString(retValue),
                //        "ReconDAL:", "insertNtslRawTxnSumCount", Convert.ToString(ReconId), startTime, DateTime.Now);
                //});
                #endregion
            }
            return isSuccess;
        }
        #endregion

        #region 6-deleteReconMaster
        public bool deleteReconMaster(long RECON_ID, long ModifiedBy, string Reason)
        {
            DateTime startTime = DateTime.Now;
            bool ret = false;
            string ErrorMessage = string.Empty;
            int retVal = -1;
            try
            {
                #region ....

                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();

                cmd.Parameters.AddWithValue("I_Recon_Id", RECON_ID);
                cmd.Parameters.AddWithValue("I_Modified_By", ModifiedBy);
                cmd.Parameters.AddWithValue("I_Reason", Reason);
                cmd.Parameters.Add("O_Status", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                dm.ExecuteScaler("sp_DeleteReconFileMaster", cmd);
                retVal = Convert.ToInt32(cmd.Parameters["O_Status"].Value.ToString());

                switch (retVal)
                {
                    #region ERROR CODES
                    case -1:
                        ErrorMessage = ConstantResponseMsg.FailureDuringDBOperation;
                        break;
                    case 0:
                        ret = true;
                        ErrorMessage = ConstantResponseMsg.Success;
                        break;
                    default:
                        ErrorMessage = ConstantResponseMsg.Fail;
                        break;
                        #endregion
                }

                #region LOG
                string logMessage = "ReconDAL - " + "deleteReconMaster" + ",  , ResponseMessage: " + ErrorMessage
                                    + ", ReconId:" + Convert.ToString(RECON_ID)
                                    + ", Deleted By:" + ((ModifiedBy > 0) ? Convert.ToString(ModifiedBy) : "");
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion


                #endregion
            }
            catch (Exception ex)
            {
                #region EXCEPTION LOG
                ErrorMessage = "Error : RDAL1 : " + ConstantResponseMsg.InternalServerError;
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError((((ModifiedBy > 0) ? Convert.ToString(ModifiedBy) : ""))
                //  , ex.Message + " - " + ex.ToString()
                //  , "SPIMPS_DELETE_RECON_MASTER:", "ReconDAL:", "deleteReconMaster");
                //});
                #endregion
            }
            finally
            {
                //if (OConn.State == ConnectionState.Open)
                //{
                //    OConn.Close();
                //}
                DateTime endTime = DateTime.Now;
                string strmesage = ErrorMessage;

                #region FINAL LOG
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog((ModifiedBy > 0) ? "" : Convert.ToString(ModifiedBy),
                //            "Status : " + retVal.ToString() + ", Message: " + strmesage, " ReconDAL:", "deleteReconMaster ",
                //            (RECON_ID > 0) ? Convert.ToString(RECON_ID) : "", startTime, endTime);
                //});
                #endregion

                //OConn = null; OCmd = null;
            }
            return ret;
        }
        #endregion

        #region 7-checkStatusUploadedFiles
        public int checkStatusUploadedFiles(out List<ReconFileCheker> objList)
        {
            DateTime start = DateTime.Now;
            int retVal = -2;
            objList = null;
            try
            {
                #region 
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();

                cmd.Parameters.Add("O_Status", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                //cmd.Parameters.Add("O_Data", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                DataTable Dt = dm.GetDataTable("sp_CheckRcnFileUpldStatus", cmd);
                objList = new List<ReconFileCheker>();
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    foreach (DataRow row in Dt.Rows)
                    {
                        ReconFileCheker objDetails = new ReconFileCheker();
                        objDetails.ReconFileId = Convert.ToString(row["Recon_File_Id"]);
                        objDetails.Status = Convert.ToString(row["Status"]);
                        objDetails.ReconDate = Convert.ToString(row["File_Date"]);
                        objDetails.NTSLFileName = Convert.ToString(row["Ntsl_File_Name"]);
                        objDetails.RawFileName = Convert.ToString(row["Raw_File_Name"]);
                        objDetails.NTSLTotalAmount = Convert.ToString(row["Ntsl_Total_00_Amount"]);
                        objDetails.RawTotalAmount = Convert.ToString(row["Raw_Total_00_Amount"]);
                        objDetails.NTSLTotalSuccesCountTxn = Convert.ToString(row["Ntsl_Total_00_Count_Txn"]);
                        objDetails.RawTotalSuccesCountTxn = Convert.ToString(row["Raw_Total_00_Count_Txn"]);
                        objDetails.NTSLTotalAmount08 = Convert.ToString(row["Ntsl_Total_08_Amount"]);
                        objDetails.RawTotalAmount08 = Convert.ToString(row["Raw_Total_08_Amount"]);
                        objDetails.NTSLTotalSuccesCountTxn08 = Convert.ToString(row["Ntsl_Total_08_Count_Txn"]);
                        objDetails.RawTotalSuccesCountTxn08 = Convert.ToString(row["Raw_Total_08_Count_Txn"]);
                        objDetails.Remark = Convert.ToString(row["StatusRemark"]);
                        objDetails.MakerId = Convert.ToInt64(row["Uploaded_By"]);
                        objList.Add(objDetails);
                    }
                }
                retVal = Convert.ToInt32(cmd.Parameters["O_Status"].Value.ToString());
                //retVal = Convert.ToInt32(OCmd.Parameters["O_STATUS"].Value.ToString());
                Dt = null;
                #endregion
            }
            catch (Exception ex)
            {
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError("", ex.Message + " - " + ex.ToString(), "SPIMPS_CHK_RCN_FLS_UPLD_STS"
                //        , "ReconDAL", "checkStatusUploadedFiles");
                //});
            }
            finally
            {
                //if (OConn.State == ConnectionState.Open)
                //{
                //    OConn.Close();
                //}
                //DateTime end = DateTime.Now;
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteReconLog("NA", " status:" + retVal.ToString() + "Message : SPIMPS_CHK_RCN_FLS_UPLD_STS"
                //        , "ReconDAL", "checkStatusUploadedFiles", "NA", start, end);
                //});
                //OConn = null; OCmd = null;
            }
            return retVal;
        }
        #endregion

        #region 8-checkRecon
        public bool checkRecon(CheckRecon checkRecon, out string outMessage)
        {
            DateTime startTime = DateTime.Now;
            int retVal = -1;
            bool isSuccess = false;
            Response response = new Response();
            outMessage = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(checkRecon.ReconFileId))
                {
                    outMessage = "Invalid Request , Can't checked";
                    return isSuccess;
                }

                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();

                cmd.Parameters.AddWithValue("I_Recon_Id", Convert.ToInt64(checkRecon.ReconFileId));
                cmd.Parameters.AddWithValue("I_Action", "Check");
                //cmd.Parameters.AddWithValue("I_Checked_By", checkRecon.CheckerId);
                cmd.Parameters.AddWithValue("I_Checked_By", 1002);
                cmd.Parameters.AddWithValue("I_Rejected_By", DBNull.Value);
                cmd.Parameters.AddWithValue("I_Rejected_Reason", DBNull.Value);
                cmd.Parameters.Add("O_Status", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                dm.ExecuteScaler("sp_CheckRejectRecon", cmd);
                retVal = Convert.ToInt32(cmd.Parameters["O_Status"].Value.ToString());

                switch (retVal)
                {
                    case 0:
                        isSuccess = true;
                        outMessage = ConstantResponseMsg.FileHasBeenChecked;
                        break;
                    case 1:
                        outMessage = ConstantResponseMsg.RecordNotFound;
                        break;
                    case 2:
                        outMessage = ConstantResponseMsg.MakerCantCheck;
                        break;
                    case -1:
                        outMessage = ConstantResponseMsg.InternalServerError;
                        break;
                    default:
                        outMessage = ConstantResponseMsg.InternalServerError;
                        break;
                }
                #region LOG
                //string logMessage = "ReconDAL - " + "checkRecon" + ",  , Status: " + outMessage
                //                    + ", ReconId:" + Convert.ToString(checkRecon.reconId)
                //                    + ", UserId:" + ((checkRecon.CheckerId > 0) ? Convert.ToString(checkRecon.CheckerId) : "");
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion
            }
            catch (Exception ex)
            {
                outMessage = "Error occured while do checking";
                #region EXCEPTION LOG
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError((((checkRecon.CheckerId > 0) ? Convert.ToString(checkRecon.CheckerId) : ""))
                //  , ex.Message + " - " + ex.ToString()
                //  , "SPIMPS_CHECK_RECON:", "ReconDAL:", "checkRecon");
                //});
                #endregion
            }
            finally
            {
                //string msg = outMessage;
                //#region FINAL LOG
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog((checkRecon.CheckerId > 0) ? Convert.ToString(checkRecon.CheckerId) : "",
                //            "Status : " + retVal.ToString() + ", Message: " + msg, " ReconDAL:", "checkRecon ",
                //            (string.IsNullOrEmpty(checkRecon.reconId)) ? Convert.ToString(checkRecon.reconId) : "", startTime, DateTime.Now);
                //});
                //#endregion
                //oCmd = null; oCon = null;
            }
            return isSuccess;
        }
        #endregion

        #region 9-rejectRecon
        public bool rejectRecon(RejectRecon rejectReason, out string Msg)
        {
            DateTime start = DateTime.Now;
            int retVal = -2;
            Msg = string.Empty;
            bool isSuccess = false;

            try
            {

                if (string.IsNullOrEmpty(rejectReason.ReconFileId))
                {
                    Msg = "Invalid Request , Can't checked";
                    return isSuccess;
                }
                if (string.IsNullOrEmpty(rejectReason.RejectedReason))
                {
                    Msg = "Please provide rejected reason";
                    return isSuccess;
                }

                #region 

                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();

                cmd.Parameters.AddWithValue("I_Recon_Id", Convert.ToInt64(rejectReason.ReconFileId));
                cmd.Parameters.AddWithValue("I_Action", "Reject");
                cmd.Parameters.AddWithValue("I_Checked_By", DBNull.Value);
                cmd.Parameters.AddWithValue("I_Rejected_By", 1002);
                cmd.Parameters.AddWithValue("I_Rejected_Reason", rejectReason.RejectedReason);
                cmd.Parameters.Add("O_Status", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                //dm.ExecuteScaler("sp_CheckRejectRecon", cmd);
                //retVal = Convert.ToInt32(cmd.Parameters["O_Status"].Value.ToString());

                DataTable Dt = dm.GetDataTable("sp_CheckRejectRecon", cmd);
                retVal = Convert.ToInt32(cmd.Parameters["O_Status"].Value.ToString());

                //
                switch (retVal)
                {
                    case 0:
                        isSuccess = true;
                        Msg = ConstantResponseMsg.FileHasBeenRejected;
                        if (Dt != null && Dt.Rows.Count > 0)
                        {
                            foreach (DataRow row in Dt.Rows)
                            {
                                //Remove File from directory
                                string ntsl_path = Convert.ToString(row["Ntsl_File_Path"]);
                                string raw_path = Convert.ToString(row["Raw_File_Path"]);

                                if (File.Exists(ntsl_path))
                                {
                                    File.Delete(ntsl_path);
                                }
                                if (File.Exists(raw_path))
                                {
                                    File.Delete(raw_path);
                                }
                            }
                        }
                        break;
                    case 2:
                        Msg = ConstantResponseMsg.MakerCantReject;
                        break;
                    case 3:
                        Msg = ConstantResponseMsg.FileAlreadyRejectd;
                        break;
                    case -1:
                        Msg = ConstantResponseMsg.InternalServerError;
                        break;
                    default:
                        Msg = ConstantResponseMsg.InternalServerError;
                        break;
                }

                #region LOG
                //string logMessage = "ReconDAL - " + "rejectRecon" + ",  , Status: " + Msg
                //                    + ", ReconId:" + Convert.ToString(rejectReason.reconId)
                //                    + ", UserId:" + ((rejectReason.Rejected_By > 0) ? Convert.ToString(rejectReason.Rejected_By) : "");
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion

                Dt = null;
                #endregion
            }
            catch (Exception ex)
            {
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError("", ex.Message + " - " + ex.ToString(), "SPIMPS_REJECT_RECON"
                //        , "ReconDAL", "rejectRecon");
                //});
            }
            finally
            {
                //DateTime end = DateTime.Now;
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteReconLog("NA", " status:" + retVal.ToString() + "Message : SPIMPS_REJECT_RECON"
                //        , "ReconDAL", "checkStatusUploadedFiles", "NA", start, end);
                //});
                //OConn = null; OCmd = null;
            }
            return isSuccess;
        }

        #endregion

        #region 10-getDoReconList
        //public bool getDoReconList(string sessionKey, out List<PrimaryRecoReprtCheker> objPRRC, long makerId, out string Msg)
        public bool getDoReconList(out List<ReconFileCheker> objPRRC, out string Msg)
        {
            DateTime startTime = DateTime.Now;
            bool res = false;
            Msg = string.Empty;
            objPRRC = null;
            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                DataTable Dt = dm.GetDataTable("sp_GetReconList", cmd);

                objPRRC = new List<ReconFileCheker>();
                if (Dt != null && Dt.Rows.Count > 0)
                //if (Dt != null && Dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in Dt.Rows)
                    {
                        ReconFileCheker PRRC = new ReconFileCheker();
                        PRRC.ReconFileId = Convert.ToString(dr["Recon_File_Id"]);
                        PRRC.MakerId = Convert.ToInt64(dr["Uploaded_By"]);
                        PRRC.NTSLFileName = dr["Ntsl_File_Name"].ToString();
                        PRRC.RawFileName = dr["Raw_File_Name"].ToString();
                        PRRC.ReconDate = dr["File_Date"].ToString();
                        PRRC.Status = dr["Status"].ToString();
                        PRRC.NTSLTotalAmount = dr["Ntsl_Total_00_Amount"].ToString();
                        PRRC.NTSLTotalSuccesCountTxn = dr["Ntsl_Total_00_Count_Txn"].ToString();
                        PRRC.RawTotalAmount = dr["Raw_Total_00_Amount"].ToString();
                        PRRC.RawTotalSuccesCountTxn = dr["Raw_Total_00_Count_Txn"].ToString();
                        PRRC.NTSLTotalAmount08 = dr["Ntsl_Total_08_Amount"].ToString();
                        PRRC.NTSLTotalSuccesCountTxn08 = dr["Ntsl_Total_08_Count_Txn"].ToString();
                        PRRC.RawTotalAmount08 = dr["Raw_Total_08_Amount"].ToString();
                        PRRC.RawTotalSuccesCountTxn08 = dr["Raw_Total_08_Count_Txn"].ToString();

                        //PRRC.Remark = dr["STATUSREMARK"].ToString();
                        PRRC.NTSLSettlment = dr["Ntsl_Gefu_Setlmnt"].ToString();

                        //PRRC.ReconFileId = "'" + AESCrypto.Encrypt(Convert.ToString(dr["RECON_FILE_ID"]), sessionKey) + "'";

                        //Avoid Xss
                        //PRRC = (PrimaryRecoReprtCheker)AntiXSS.cleanXSS(PRRC);
                        objPRRC.Add(PRRC);
                    }
                    res = true;
                }
                else
                {
                    Msg = ConstantResponseMsg.RecordNotFound;
                }
                #region LOG
                //string logMessage = "ReconDAL - " + "getDoReconList" + ",  , Status: DataTableRows- " + dt.Rows.Count
                //                    + ", ReconId: NA"
                //                    + ", UserId:" + ((makerId > 0) ? Convert.ToString(makerId) : "");
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion
            }
            catch (Exception ex)
            {
                //Msg = "Error occured while fetching data for recon";
                //#region EXCEPTION LOG
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteDBError((((makerId > 0) ? Convert.ToString(makerId) : ""))
                //  , ex.Message + " - " + ex.ToString()
                //  , "SP_GET_PRMR_RECO_RPRT_MAKER:", "ReconDAL:", "getDoReconList");
                //});
                //#endregion
            }
            finally
            {
                //string msgs = string.Empty;
                //#region FINAL LOG
                //if (dt.Rows.Count > 0)
                //    msgs = "Data found, count :" + dt.Rows.Count;
                //else
                //    msgs = "Data not found, count :" + dt.Rows.Count;
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog((makerId > 0) ? Convert.ToString(makerId) : "",
                //            "Status : " + msgs, " ReconDAL:", "getDoReconList ",
                //            "NA", startTime, DateTime.Now);
                //});
                //#endregion
                //oConn = null; oCmd = null; dt = null;
            }
            return res;
        }
        #endregion

        #region 11-doReconProcess
        public bool doReconProcess(Int64 reconId, Int64 ReconBy, out string outMessage)
        {
            DateTime startTime = DateTime.Now;
            outMessage = string.Empty;
            int retStatus = -1;
            bool isSuccess = false;
            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();

                cmd.Parameters.AddWithValue("I_RECON_ID", Convert.ToInt64(reconId));
                cmd.Parameters.AddWithValue("I_RECON_BY", Convert.ToInt64(ReconBy));
                cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                dm.ExecuteScaler("sp_DoReconProcess", cmd);
                retStatus = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());

                switch (retStatus)
                {
                    case 0:
                        outMessage = ConstantResponseMsg.FileHasBeenProcessed;
                        isSuccess = true;
                        break;
                    case -1:
                        outMessage = ConstantResponseMsg.FailureDuringDBOperation;
                        break;
                    case 2:
                        outMessage = ConstantResponseMsg.ReconIdNotFoundInNTSlMaster;
                        break;
                    case 3:
                        outMessage = ConstantResponseMsg.InsertedDeletedNotMatched;
                        break;
                    case 4:
                        outMessage = ConstantResponseMsg.NoDataFoundInRange;
                        break;
                    case 5:
                        outMessage = ConstantResponseMsg.PrevReconChecking;
                        break;
                    default:
                        outMessage = ConstantResponseMsg.InternalServerError;
                        break;
                }

                #region LOG
                //string logMessage = "ReconDAL - " + "doReconProcess" + ",  , Status: " + outMessage
                //                    + ", ReconId:" + Convert.ToString(reconId)
                //                    + ", UserId:" + ((ReconBy > 0) ? Convert.ToString(ReconBy) : "");
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                #endregion

            }
            catch (Exception ex)
            {
                outMessage = "Error occured while start recon";
                #region EXCEPTION LOG
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteDBError((((ReconBy > 0) ? Convert.ToString(ReconBy) : ""))
                //  , ex.Message + " - " + ex.ToString()
                //  , "SP_DO_RECON_PROCESS:", "ReconDAL:", "doReconProcess");
                //});
                #endregion
            }
            finally
            {
                //string statusMsg = outMessage;
                //#region FINAL LOG
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog((ReconBy > 0) ? Convert.ToString(ReconBy) : "",
                //            "Status : " + retStatus.ToString() + ", Message: " + statusMsg, " ReconDAL:", "doReconProcess ",
                //            (reconId > 0) ? Convert.ToString(reconId) : "", startTime, DateTime.Now);
                //});
                //#endregion
                //oCmd = null; oConn = null;
            }
            return isSuccess;
        }
        #endregion

    }
}
