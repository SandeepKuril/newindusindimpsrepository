﻿using EntityLayer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace DataAccess
{
    public class RoleDA
    {
        static MySqlConnection con;

        public static MySqlConnection getConnection()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            var config = builder.Build();
            //con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("DevConnection").Value);
            con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("IndusIndAdminConStr").Value);
            return con;
        }
        public static DataTable GetRole(int RoleId)
        {
            DataTable DT = new DataTable();
            try
            {
                MySqlCommand command = new MySqlCommand();
                command.Parameters.AddWithValue("I_Role_Id", RoleId);
                DataManager dm = new DataManager();

                DT = dm.GetDataTable("sp_GetRole", command);
            }
            catch (Exception ex)
            {
                //Generate Log here
                DT = null;
            }
            return DT;
        }
        public static int SaveRole(Role ObjRole)
        {
            int i = 0;
            int val = -1;
            MySqlConnection connect = getConnection();
            //MySqlCommand ObjCommand = null;
            try
            {
                using (connect)
                {
                    MySqlCommand ObjCommand = new MySqlCommand("SPIMPS_CREATE_ROLE", con);
                    ObjCommand.CommandType = CommandType.StoredProcedure;
                    ObjRole.UserId = "Sandeep";
                   // ObjCommand = new MySqlCommand();
                    //ObjCommand.Parameters.AddWithValue("I_Role_Id", ObjRole.RoleId);
                    ObjCommand.Parameters.AddWithValue("I_Role_Name", ObjRole.RoleName);
                    ObjCommand.Parameters.AddWithValue("I_User_Id", ObjRole.UserId);
                    ObjCommand.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    /*ObjCommand.CommandText = "SPIMPS_CREATE_ROLE";
                    ObjCommand.CommandType = CommandType.StoredProcedure;
                    ObjCommand.ExecuteReader();*/
                    con.Open();
                    i = Convert.ToInt32(ObjCommand.ExecuteScalar());
                   /* DataManager dm = new DataManager();
                    i = dm.ExecuteScaler("SPIMPS_CREATE_ROLE", ObjCommand);*/
                    val = Convert.ToInt32(ObjCommand.Parameters["O_STATUS"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                i = -1;
            }
            finally
            {
                //ObjCommand = null;
            }
            return val;
        }
        public static int DeleteRole(int RoleId,string UserId)
        {
            int i = 1;
            MySqlCommand ObjCommand = null;
            try
            {
                ObjCommand = new MySqlCommand();
                ObjCommand.Parameters.AddWithValue("I_Role_Id", RoleId);
                ObjCommand.Parameters.AddWithValue("I_User_Id", UserId);
                DataManager dm = new DataManager();
                i = dm.ExecuteScaler("sp_DeleteRole", ObjCommand);
            }
            catch (Exception ex)
            {
                i = -1;
            }
            finally
            {
                ObjCommand = null;
            }
            return i;
        }

        public static int UpdateRole(Role role)
        {
            int i = 1;
            int val = 0;
            MySqlCommand ObjCommand = null;
            try
            {
                //string userId = HttpContext.Session.GetString("LoginUser");
                ObjCommand = new MySqlCommand();
                ObjCommand.Parameters.AddWithValue("I_Role_Name", role.RoleName);
                ObjCommand.Parameters.AddWithValue("I_Role_Id", role.RoleId);
                ObjCommand.Parameters.AddWithValue("I_User_Id", role.UserId);
                ObjCommand.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                DataManager dm = new DataManager();
                i = dm.ExecuteScaler("SPIMPS_UpdateRole", ObjCommand);
                val = Convert.ToInt32(ObjCommand.Parameters["O_STATUS"].Value.ToString());
            }
            catch (Exception ex)
            {
                val = -1;
            }
            finally
            {
                ObjCommand = null;
            }
            return val;
        }
    }
}
