﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using EntityLayer;
using MySql.Data.MySqlClient;

namespace DataAccess
{
    public class PartnerDA
    {
        //public PartnerViewModel Partners
        public List<PartnerEL> Partners
        {
            get
            {
                //PartnerViewModel partnerInfo = new PartnerViewModel();
                //partnerInfo.PartnerList = new List<PartnerEL>();
                List<PartnerEL> partnerInfo = new List<PartnerEL>();
                
                try
                {
                    DataManager dm = new DataManager();
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Parameters.AddWithValue("I_Partner_Id", DBNull.Value);
                    //cmd.Parameters["@I_I_User_Id"].IsNullable = true;
                    cmd.Parameters.AddWithValue("I_Action", "Select");
                    DataTable dt = dm.GetDataTable("sp_GetPartner", cmd);


                    foreach (DataRow dr in dt.Rows)
                    {
                        PartnerEL partner = new PartnerEL();
                        //partner.User_Id = Convert.ToInt32(dr["User_Id"]);
                        partner.Partner_Id = Convert.ToInt32(dr["Partner_Id"]);
                        partner.Partner_Name = dr["Partner_Name"].ToString();
                        partner.Contact_Person_Name = dr["Contact_Person_Name"].ToString();
                        partner.Account_Number = dr["Account_Number"].ToString();
                        partner.Status_Msg = dr["Status_Msg"].ToString();
                        //partnerInfo.PartnerList.Add(partner);
                        partnerInfo.Add(partner);
                    }
                }
                catch (Exception expt)
                {
                    Console.WriteLine("Exception" + expt);
                }

                return partnerInfo;
            }
        }



        public List<PartnerEL> GetPartnerById(int partnerId)
        {
            //UserViewModel userInfo = new UserViewModel();
            //userInfo.UserData = new UserEL();
            List<PartnerEL> partnerInfo = new List<PartnerEL>();

            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_Partner_Id", partnerId);
                cmd.Parameters.AddWithValue("I_Action", "SelectById");

                DataTable dt = dm.GetDataTable("sp_GetPartner", cmd);

                if (dt != null && dt.Rows.Count > 0)
                {
                    PartnerEL partner = new PartnerEL();
                    partner.Partner_Id = Convert.ToInt32(dt.Rows[0]["Partner_Id"]);
                    partner.Partner_Name = dt.Rows[0]["Partner_Name"].ToString();
                    partner.Mobile_Number = dt.Rows[0]["Mobile_Number"].ToString();
                    partner.Landline = dt.Rows[0]["Landline"].ToString();
                    partner.Email = dt.Rows[0]["Email"].ToString();
                    partner.Office_Address = dt.Rows[0]["Office_Address"].ToString();
                    partner.Pincode = Convert.ToInt32(dt.Rows[0]["Pincode"]);
                    partner.State = dt.Rows[0]["State"].ToString();
                    partner.City = dt.Rows[0]["City"].ToString();
                    partner.Pan_Number = dt.Rows[0]["Pan_Number"].ToString();
                    partner.Contact_Person_Name = dt.Rows[0]["Contact_Person_Name"].ToString();
                    partner.Contact_Person_Number = dt.Rows[0]["Contact_Person_Number"].ToString();
                    partner.Account_Number = dt.Rows[0]["Account_Number"].ToString();
                    partner.Ifsc_Code = dt.Rows[0]["Ifsc_Code"].ToString();
                    partner.Centeral_Gst_Number = dt.Rows[0]["Centeral_Gst_Number"].ToString();
                    partner.State_Gst_Number = dt.Rows[0]["State_Gst_Number"].ToString();
                    partner.Gst_Address = dt.Rows[0]["Gst_Address"].ToString();
                    partner.Website_Url = dt.Rows[0]["Website_Url"].ToString();

                    partnerInfo.Add(partner);
                }
            }
            catch (Exception expt)
            {
                Console.WriteLine("Exception" + expt);
            }

            return partnerInfo;
        }


        public int insertPartner(PartnerEL partnerData)
        {
            //MySqlConnection connect = getConnection();
            int res;

            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_Partner_Id", DBNull.Value);
                cmd.Parameters.AddWithValue("I_Partner_Name", partnerData.Partner_Name);
                cmd.Parameters.AddWithValue("I_Mobile_Number", partnerData.Mobile_Number);
                cmd.Parameters.AddWithValue("I_Landline", partnerData.Landline);
                cmd.Parameters.AddWithValue("I_Email", partnerData.Email);
                cmd.Parameters.AddWithValue("I_Office_Address", partnerData.Office_Address);
                cmd.Parameters.AddWithValue("I_Pincode", partnerData.Pincode);
                cmd.Parameters.AddWithValue("I_State", partnerData.State);
                cmd.Parameters.AddWithValue("I_City", partnerData.City);
                cmd.Parameters.AddWithValue("I_Pan_Number", partnerData.Pan_Number);
                cmd.Parameters.AddWithValue("I_Contact_Person_Name", partnerData.Contact_Person_Name);
                cmd.Parameters.AddWithValue("I_Contact_Person_Number", partnerData.Contact_Person_Number);
                cmd.Parameters.AddWithValue("I_Account_Number", partnerData.Account_Number);
                cmd.Parameters.AddWithValue("I_Ifsc_Code", partnerData.Ifsc_Code);
                cmd.Parameters.AddWithValue("I_Centeral_Gst_Number", partnerData.Centeral_Gst_Number);
                cmd.Parameters.AddWithValue("I_State_Gst_Number", partnerData.State_Gst_Number);
                cmd.Parameters.AddWithValue("I_Gst_Address", partnerData.Gst_Address);
                cmd.Parameters.AddWithValue("I_Website_Url", partnerData.Website_Url);
                cmd.Parameters.AddWithValue("I_Action", "Insert");
                res = dm.ExecuteScaler("sp_InsertUpdatePartner", cmd);
            }
            catch (Exception expt)
            {
                res = -1;
                Console.WriteLine("Exception" + expt);
            }

            return res;
        }


        public int updatePartner(PartnerEL partnerData)
        {
            //MySqlConnection connect = getConnection();
            int res;

            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_Partner_Id", partnerData.Partner_Id);
                cmd.Parameters.AddWithValue("I_Partner_Name", partnerData.Partner_Name);
                cmd.Parameters.AddWithValue("I_Mobile_Number", partnerData.Mobile_Number);
                cmd.Parameters.AddWithValue("I_Landline", partnerData.Landline);
                cmd.Parameters.AddWithValue("I_Email", partnerData.Email);
                cmd.Parameters.AddWithValue("I_Office_Address", partnerData.Office_Address);
                cmd.Parameters.AddWithValue("I_Pincode", partnerData.Pincode);
                cmd.Parameters.AddWithValue("I_State", partnerData.State);
                cmd.Parameters.AddWithValue("I_City", partnerData.City);
                cmd.Parameters.AddWithValue("I_Pan_Number", partnerData.Pan_Number);
                cmd.Parameters.AddWithValue("I_Contact_Person_Name", partnerData.Contact_Person_Name);
                cmd.Parameters.AddWithValue("I_Contact_Person_Number", partnerData.Contact_Person_Number);
                cmd.Parameters.AddWithValue("I_Account_Number", partnerData.Account_Number);
                cmd.Parameters.AddWithValue("I_Ifsc_Code", partnerData.Ifsc_Code);
                cmd.Parameters.AddWithValue("I_Centeral_Gst_Number", partnerData.Centeral_Gst_Number);
                cmd.Parameters.AddWithValue("I_State_Gst_Number", partnerData.State_Gst_Number);
                cmd.Parameters.AddWithValue("I_Gst_Address", partnerData.Gst_Address);
                cmd.Parameters.AddWithValue("I_Website_Url", partnerData.Website_Url);
                cmd.Parameters.AddWithValue("I_Action", "Update");
                res = dm.ExecuteScaler("sp_InsertUpdatePartner", cmd);
            }
            catch (Exception expt)
            {
                res = -1;
                Console.WriteLine("Exception" + expt);
            }

            return res;
        }


        public int deletePartner(PartnerEL partnerData)
        { 
            int res;
            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_Partner_Id", partnerData.Partner_Id);
                res = dm.ExecuteScaler("sp_DeletePartner", cmd);
            }
            catch (Exception expt)
            {
                res = -1;
                Console.WriteLine("Exception" + expt);
            }

            return res;
        }

        public int checkPartner(PartnerEL partnerData)
        {
            int res;
            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_Partner_Id", partnerData.Partner_Id);
                cmd.Parameters.AddWithValue("I_Action", "Check");
                cmd.Parameters.AddWithValue("I_Checked_By", 10001);
                cmd.Parameters.AddWithValue("I_Rejected_By", DBNull.Value);
                cmd.Parameters.AddWithValue("I_Rejected_Reason", DBNull.Value);
                res = dm.ExecuteScaler("sp_CheckRejectPartner", cmd);
            }
            catch (Exception expt)
            {
                res = -1;
                Console.WriteLine("Exception" + expt);
            }

            return res;
        }


        public int rejectPartner(PartnerEL partnerData)
        {
            int res;
            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_Partner_Id", partnerData.Partner_Id);
                cmd.Parameters.AddWithValue("I_Action", "Reject");
                cmd.Parameters.AddWithValue("I_Checked_By", DBNull.Value);
                cmd.Parameters.AddWithValue("I_Rejected_By", 10001);
                cmd.Parameters.AddWithValue("I_Rejected_Reason", partnerData.Rejected_Reason);
                res = dm.ExecuteScaler("sp_CheckRejectPartner", cmd);
            }
            catch (Exception expt)
            {
                res = -1;
                Console.WriteLine("Exception" + expt);
            }

            return res;
        }


    }
}
