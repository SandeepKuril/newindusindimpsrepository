﻿using CommonLayer;
using EntityLayer;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class AdjustmentDA
    {
        #region ConnectionString
        MySqlConnection con;

        public object AntiXSS { get; private set; }

        public MySqlConnection getConnection()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            var config = builder.Build();
            //con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("DevConnection").Value);
            con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("IndusIndAdminConStr").Value);
            return con;
        }
        #endregion

        #region DATE FORMAT
        private string getDateFormat(string date)
        {
            string[] str = null;
            str = date.Split('/');
            string dd = str[0];
            string mm = str[1];
            string yyyy = str[2];
            if (mm.Length != 2)
            {
                mm = "0" + mm;
            }
            if (dd.Length != 2)
            {
                dd = "0" + dd;
            }
            //YYYY-MM-DD Format
            return yyyy + "-" + mm + "-" + dd;
        }
        //
        private string breakDateFromFile(string date)
        {
            string strDate = date;
            string currentYear = DateTime.Now.ToString("yyyy");
            currentYear = currentYear.Substring(0, 2);
            string dd = strDate.Substring(0, 2);
            string mm = strDate.Substring(2, 2);
            string yyyy = currentYear + strDate.Substring(4, 2);

            return yyyy + "-" + mm + "-" + dd;
        }

        #endregion

        #region 14-deleteTCCRETMaster 
        public bool deleteTCCRETMaster(long TCCRET_ID, long ModifiedBy)
        {
            bool ret = false;
            string ErrorMessage = string.Empty;
            MySqlConnection connect = getConnection();
            try
            {
                #region ....

                MySqlCommand cmd = null;
                using (connect)
                {
                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_TCC_RET_ID", TCCRET_ID);
                    cmd.Parameters.AddWithValue("I_MODIFIED_BY", ModifiedBy);
                    cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add("O_TCC_RET_ID", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    dm.ExecuteScaler("SPIMPS_DELETE_TCCRET_MSTR", cmd);

                    //Output
                    int retVal = -1;
                    retVal = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());
                    con.Close();
                    switch (retVal)
                    {
                        #region ERROR CODES
                        case -1:
                            ErrorMessage = Messages.FailureDuringDBOperation;
                            break;
                        case 0:
                            ret = true;
                            ErrorMessage = Messages.Success;
                            break;
                        default:
                            ErrorMessage = Messages.Faile;
                            break;
                            #endregion
                    }
                    //LOG 
                    //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", Deleted TCCRET master record , Status: " + ErrorMessage + ", RAW_ID:" + Convert.ToString(TCCRET_ID) + ", Deleted By:" + Convert.ToString(ModifiedBy);
                    //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                    #endregion
                }
            }

            catch (Exception ex)
            {
                ErrorMessage = Messages.ExceptionError;
                string methodname = MethodBase.GetCurrentMethod().Name;
                /*Task.Factory.StartNew(delegate
                {
                    logs.WriteDBError(ObjLoginEL.UserName, ex, "sp_UserLogin", methodname, LogType.Error, LogFiles.INDIMPS_DB_Error);
                });*/
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con = null; //OCmd = null;
            }
            return ret;
        }
        #endregion

        #region 15-updateStatusInTCCRETMaster
        public bool updateStatusInTCCRETMaster(long TCCRET_ID, long ModifiedBy)
        {
            bool ret = false;
            string ErrorMessage = string.Empty;
            MySqlConnection connect = getConnection();
            try
            {
                MySqlCommand cmd = null;
                #region ....
                using (connect)
                {
                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_TCC_RET_ID", TCCRET_ID);
                    cmd.Parameters.AddWithValue("I_MODIFIED_BY", ModifiedBy);
                    cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add("O_TCC_RET_ID", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    dm.ExecuteScaler("SPIMPS_UPDT_STATUS_TR_MSTR", cmd);

                    //Output
                    int retVal = -1;
                    retVal = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());
                    con.Close();
                    switch (retVal)
                    {
                        #region ERROR CODES
                        case -1:
                            ErrorMessage = Messages.FailureDuringDBOperation;
                            break;
                        case 0:
                            ret = true;
                            ErrorMessage = Messages.Success;
                            break;
                        case 1:
                            ErrorMessage = Messages.Faile;
                            break;
                        case 2:
                            ErrorMessage = Messages.ReconIdNotFoundInNTSlMaster;
                            break;
                        default:
                            ErrorMessage = Messages.Faile;
                            break;
                            #endregion
                    }
                    //LOG 
                    //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", Update Status TCCRET master record , Status: " + ErrorMessage + ", RAW_ID:" + Convert.ToString(TCCRET_ID) + ", Deleted By:" + Convert.ToString(ModifiedBy);
                    //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                    #endregion
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = Messages.ExceptionError;
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con = null; //cmd = null;
            }
            return ret;
        }
        #endregion

        #region 16-insertTccRetDesc
        public bool insertTccRetDesc(TCCRET_File tccRet_file, Int64 UserId, out string ErrorMessage, out Int64 TCC_RET_ID)
        {
            bool ret = false;
            TCC_RET_ID = 0;
            ErrorMessage = string.Empty;
            MySqlConnection connect = getConnection();
            //OracleConnection OConn = new OracleConnection(ConnectionString);
            //OracleCommand OCmd = new OracleCommand();
            try
            {
                //Remove XSS malicius file 
                // tccRet_file = (TCCRET_File)AntiXSS.cleanXSSUsingHtmlEncode(tccRet_file);
                #region ....

                //
                MySqlCommand cmd = null;
                using (connect)
                {
                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_FILE_NAME", tccRet_file.FileName);
                    cmd.Parameters.AddWithValue("I_FILE_PATH", tccRet_file.FilePath);
                    cmd.Parameters.AddWithValue("I_TCC_RET_DATE", tccRet_file.Tcc_Ret_Date);
                    cmd.Parameters.AddWithValue("I_UPLOADED_BY", UserId);
                    cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("O_TCC_RET_ID", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    dm.ExecuteScaler("SPIMPS_INSERT_ADJUSTMENT_FILE_MASTER", cmd);

                    //Output
                    int retStatus = -1;
                    retStatus = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());
                    if (retStatus == 0)
                    {
                        TCC_RET_ID = Convert.ToInt64(cmd.Parameters["O_TCC_RET_ID"].Value.ToString());
                    }
                    con.Close();
                    /*response = Convert.ToInt32(cmd.ExecuteScalar());
                    if (response > -1)
                    {
                        respo = MesgForUpdate(response);
                        return respo;
                    }*/

                    //

                    switch (retStatus)
                    {
                        #region ERROR CODES
                        case -1:
                            ErrorMessage = Messages.FailureDuringDBOperation;
                            break;
                        case 0:
                            ret = true;
                            ErrorMessage = Messages.Success;
                            break;
                        case 1:
                            ErrorMessage = Messages.Faile;
                            break;
                        case 2:
                            ErrorMessage = Messages.FileNameAlreadyExistsInDB;
                            break;
                        default:
                            ErrorMessage = Messages.Faile;
                            break;
                            #endregion
                    }
                    //LOG 
                    // string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", TCC/RET File uploded in directory and inserted data in Database, Status: " + ErrorMessage + ", TCC_RET_ID:" + Convert.ToString(TCC_RET_ID);
                    //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogApplication); });
                }
                #endregion
            }
            catch (Exception ex)
            {
                ErrorMessage = Messages.ExceptionError;
                string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                // Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con = null; tccRet_file = null;
            }
            return ret;
        }
        #endregion

        #region 17-insertTccRetFileData
        public bool insertTccRetFileData(List<TCCRET> listTccRet, long UserId)
        {
            bool retVal = false;
            //OracleConnection oConn = new OracleConnection(ConnectionString);
            //OracleCommand oCmd = new OracleCommand();
            MySqlConnection connect = getConnection();
            //oCmd.CommandType = CommandType.Text;
            //oCmd.Connection = oConn;
            
            //OracleTransaction oTxn = oConn.BeginTransaction();
            //
            try
            {
                
                MySqlCommand cmd = null;
                #region ....
                using (connect)
                {
                    con.Open();
                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.Text;
                    //listTccRet = (List<TCCRET>)AntiXSS.cleanXSS(listTccRet);
                    //listTccRet.RemoveAt(0);
                    int countRows = listTccRet.Count;
                    int successCount = 0;
                    //connect.Open();
                    #region bulkData Reading
                    foreach (var r in listTccRet)
                    {
                        //con.Open();
                        string query = "SET SQL_SAFE_UPDATES=0;INSERT INTO adjustment_file_data(TCC_RET_MSTR_ID,TXN_UID,U_ID,ADJDATE,ADJTYPE,REMITTER,BENEFICIERY,RESPONSE,TXNDATE,TXNTIME,RRN_NO,TERMINALID,BEN_MOBILE_NO,REM_MOBILE_N0,CHBDATE,CHBREF,TXNAMOUNT,ADJAMOUNT,REM_FEE,BEN_FEE,BEN_FEESW,ADJ_FEE,NPCIFEE,REMFEETAX,BEN_FEE_TAX,NPCITAX,ADJREF,BANKADJREF,ADJPROOF,SHDT70,SHDT71,SHDT72,SHDT73,SHDT74,SHDT75,SHDT76,SHDT77)"
                             + "VALUES(@I_TCC_RET_MSTR_ID, @I_TXN_UID, @I_U_ID, @I_ADJDATE, @I_ADJTYPE, @I_REMITTER, @I_BENEFICIERY, @I_RESPONSE, @I_TXNDATE, @I_TXNTIME, @I_RRN_NO, @I_TERMINALID, @I_BEN_MOBILE_NO, @I_REM_MOBILE_N0, @I_CHBDATE, @I_CHBREF, @I_TXNAMOUNT, @I_ADJAMOUNT, @I_REM_FEE, @I_BEN_FEE, @I_BEN_FEESW, @I_ADJ_FEE, @I_NPCIFEE, @I_REMFEETAX, @I_BEN_FEE_TAX, @I_NPCITAX, @I_ADJREF, @I_BANKADJREF, @I_ADJPROOF, @I_SHDT70, @I_SHDT71, @I_SHDT72, @I_SHDT73, @I_SHDT74, @I_SHDT75, @I_SHDT76, @I_SHDT77)";

                        cmd.CommandText = query;
                        cmd.Parameters.AddWithValue("@I_TCC_RET_MSTR_ID", r.TCC_RET_MSTR_ID);
                        cmd.Parameters.AddWithValue("@I_RRN", r.RRN);
                        cmd.Parameters.AddWithValue("@I_TXN_UID", r.TXN_UID);
                        cmd.Parameters.AddWithValue("@I_U_ID", r.U_ID);
                        cmd.Parameters.AddWithValue("@I_ADJDATE", r.ADJDATE);
                        cmd.Parameters.AddWithValue("@I_ADJTYPE", r.ADJTYPE);
                        cmd.Parameters.AddWithValue("@I_REMITTER", r.REMITTER);
                        cmd.Parameters.AddWithValue("@I_BENEFICIERY", r.BENEFICIERY);
                        cmd.Parameters.AddWithValue("@I_RESPONSE", r.RESPONSE);
                        cmd.Parameters.AddWithValue("@I_TXNDATE", r.TXNDATE);
                        cmd.Parameters.AddWithValue("@I_TXNTIME", r.TXNTIME);
                        cmd.Parameters.AddWithValue("@I_RRN_NO", r.RRN);
                        cmd.Parameters.AddWithValue("@I_TERMINALID", r.TERMINALID);
                        cmd.Parameters.AddWithValue("@I_BEN_MOBILE_NO", r.BEN_MOBILE_NO);
                        cmd.Parameters.AddWithValue("@I_REM_MOBILE_N0", r.REM_MOBILE_N0);
                        cmd.Parameters.AddWithValue("@I_CHBDATE", r.CHBDATE);
                        cmd.Parameters.AddWithValue("@I_CHBREF", r.CHBREF);
                        cmd.Parameters.AddWithValue("@I_TXNAMOUNT", r.TXNAMOUNT);
                        cmd.Parameters.AddWithValue("@I_ADJAMOUNT", r.ADJAMOUNT);
                        cmd.Parameters.AddWithValue("@I_REM_FEE", r.REM_FEE);
                        cmd.Parameters.AddWithValue("@I_BEN_FEE", r.BEN_FEE);
                        cmd.Parameters.AddWithValue("@I_BEN_FEESW", r.BEN_FEESW);
                        cmd.Parameters.AddWithValue("@I_ADJ_FEE", r.ADJ_FEE);
                        cmd.Parameters.AddWithValue("@I_NPCIFEE", r.NPCIFEE);
                        cmd.Parameters.AddWithValue("@I_REMFEETAX", r.REMFEETAX);
                        cmd.Parameters.AddWithValue("@I_BEN_FEE_TAX", r.BEN_FEE_TAX);
                        cmd.Parameters.AddWithValue("@I_NPCITAX", r.NPCITAX);
                        cmd.Parameters.AddWithValue("@I_ADJREF", r.ADJREF);
                        cmd.Parameters.AddWithValue("@I_BANKADJREF", r.BANKADJREF);
                        cmd.Parameters.AddWithValue("@I_ADJPROOF", r.ADJPROOF);
                        cmd.Parameters.AddWithValue("@I_SHDT70", r.SHDT70);
                        cmd.Parameters.AddWithValue("@I_SHDT71", r.SHDT71);
                        cmd.Parameters.AddWithValue("@I_SHDT72", r.SHDT72);
                        cmd.Parameters.AddWithValue("@I_SHDT73", r.SHDT73);
                        cmd.Parameters.AddWithValue("@I_SHDT74", r.SHDT74);
                        cmd.Parameters.AddWithValue("@I_SHDT75", r.SHDT75);
                        cmd.Parameters.AddWithValue("@I_SHDT76", r.SHDT76);
                        cmd.Parameters.AddWithValue("@I_SHDT77", r.SHDT77);
                        //dm.ExecuteScaler("SPIMPS_DELETE_TCCRET_MSTR", cmd);
                        //cmd.ExecuteNonQuery();
                        //cmd.Parameters.AddRange(oParam);
                        //con.Close();
                        //con.Open();
                        int val = cmd.ExecuteNonQuery();
                        if (val > 0)
                        {
                            successCount = successCount + 1;
                        }
                        cmd.Parameters.Clear();
                    }
                    #endregion
                    connect.Close();
                    if (countRows == successCount)
                    {
                        dm.Commit();
                        retVal = true;
                    }
                    /*else
                    {

                    }*/
                }
                #endregion
            }
            catch (Exception ex)
            {
                //dm.Rollback();
                string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con = null; //cmd = null;
                listTccRet = null;
            }
            return retVal;
        }

        #endregion

        #region 18-getTCCRETReport 
        public bool getTCCRETReport(string sessionKey, out string errorMsg, out List<TccRetREPORT> objTccRetList)
        {
            bool resp = false;
            errorMsg = string.Empty;
            MySqlConnection connect = getConnection();
            //OracleConnection oConn = new OracleConnection(ConnectionString);
            //OracleCommand oCmd = new OracleCommand();
            objTccRetList = null;
            try
            {
                MySqlCommand cmd = null;
                using (connect)
                {
                    DataTable Dt = new DataTable();
                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("O_DATA", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    Dt = dm.GetDataTable("SP_TCC_RET_REPORT", cmd);

                    if (Dt.Rows.Count > 0 && Dt != null)
                    {
                        objTccRetList = new List<TccRetREPORT>();
                        foreach (DataRow dr in Dt.Rows)
                        {
                            TccRetREPORT objTR = new TccRetREPORT();
                            //objTR.TccRetId = dr["TCC_RET_ID"].ToString();
                            objTR.Status = dr["STATUS"].ToString();
                            objTR.StatusRemark = dr["STATUSREMARK"].ToString();
                            objTR.FileName = dr["FILE_NAME"].ToString();
                            objTR.Date = dr["TCC_RET_DATE"].ToString();
                            objTR.UploadedBy = dr["UPLOADED_BY"].ToString();

                            //objTR.TccRetId = "'" + AESCrypto.Encrypt(Convert.ToString(dr["TCC_RET_ID"]), sessionKey) + "'";
                            objTR.TccRetId = "'" + Convert.ToString(dr["TCC_RET_ID"])+ sessionKey + "'";

                            //Avoid xss
                            //objTR =(TccRetREPORT)AntiXSS.cleanXSS(objTR);
                            objTccRetList.Add(objTR);
                        }
                    }
                    int o_status = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());
                    switch (o_status)
                    {
                        case -1:
                            errorMsg = Messages.FailureDuringDBOperation;
                            break;
                        case 0:
                            errorMsg = Messages.Success;
                            resp = true;
                            break;
                        case 1:
                            errorMsg = Messages.RecordNotFound;
                            break;
                        default:
                            errorMsg = Messages.Faile;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = Messages.ExceptionError;
                /*string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });*/
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con = null; //oCmd = null;
            }
            return resp;
        }
        #endregion

        #region 19-doTccRetProcess
        public bool doTccRetProcess(Int64 TccRetId, Int64 UpdatedBy, out string outMessage)
        {
            MySqlConnection connect = getConnection();
            //OracleConnection OConn = new OracleConnection(ConnectionString);
            //OracleCommand OCmd = new OracleCommand();
            outMessage = string.Empty;
            Int32 retStatus = -1;
            bool isSuccess = false;
            try
            {

                //
                MySqlCommand cmd = null;
                using (connect)
                {
                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_TCC_RET_MSTR_ID", TccRetId);
                    cmd.Parameters.AddWithValue("I_UPDATED_BY", UpdatedBy);
                    cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add("O_TCC_RET_ID", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    dm.ExecuteScaler("SPIMPS_DO_TCC_RET_FILE_PROCESS", cmd);
                    retStatus = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());
                    switch (retStatus)
                    {
                        case 0:
                            outMessage = Messages.FileHasBeenProcessed;
                            isSuccess = true;
                            break;
                        case -1:
                            outMessage = Messages.FailureDuringDBOperation;
                            break;
                        case 1:
                            outMessage = Messages.RecordNotFound;
                            break;
                        case 2:
                            outMessage = Messages.TccRetUpdated;
                            break;
                        default:
                            outMessage = Messages.InternalServerError;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                outMessage = Messages.ExceptionError;
                string classMethod = ex.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                //cmd = null;
            }
            return isSuccess;
        }
        #endregion

        #region 20-getNotCheckedTccRetList
        public bool getNotCheckedTccRetList(string sessionKey, out string errorMsg, out List<TccRetREPORT> objTccRetList)
        {
            bool resp = false;
            errorMsg = string.Empty;
            MySqlConnection connect = getConnection();
            //OracleConnection OConn = new OracleConnection(ConnectionString);
            //OracleCommand OCmd = new OracleCommand();
            objTccRetList = null;
            try
            {
                MySqlCommand cmd = null;
                using (connect)
                {
                    DataTable dt = new DataTable();
                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("O_DATA", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    dt = dm.GetDataTable("SP_GET_UNCHEKED_TCCRET_LIST", cmd);

                    if (dt.Rows.Count > 0 && dt != null)
                    {
                        objTccRetList = new List<TccRetREPORT>();
                        foreach (DataRow dr in dt.Rows)
                        {
                            TccRetREPORT objTR = new TccRetREPORT();
                            //objTR.TccRetId = dr["TCC_RET_ID"].ToString();
                            objTR.Status = dr["STATUS"].ToString();
                            objTR.StatusRemark = dr["STATUSREMARK"].ToString();
                            objTR.FileName = dr["FILE_NAME"].ToString();
                            objTR.Date = dr["TCC_RET_DATE"].ToString();
                            objTR.UploadedBy = dr["UPLOADED_BY"].ToString();

                            //objTR.TccRetId = "'" + AESCrypto.Encrypt(Convert.ToString(dr["TCC_RET_ID"]), sessionKey) + "'";
                            objTR.TccRetId = "'" + Convert.ToString(dr["TCC_RET_ID"] + sessionKey) + "'";

                            //Avoid xss
                            //objTR = (TccRetREPORT)AntiXSS.cleanXSS(objTR);
                            objTccRetList.Add(objTR);
                        }
                    }
                    int o_status = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());
                    switch (o_status)
                    {
                        case -1:
                            errorMsg = Messages.FailureDuringDBOperation;
                            break;
                        case 0:
                            errorMsg = Messages.Success;
                            resp = true;
                            break;
                        case 1:
                            errorMsg = Messages.RecordNotFound;
                            break;
                        default:
                            errorMsg = Messages.Faile;
                            break;
                    }
                }
            }

            catch (Exception ex)
            {
                errorMsg = Messages.ExceptionError;
                string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con = null; //oCmd = null;
            }
            return resp;
        }
        #endregion

        #region 21-checkTccRet
        public bool checkTccRet(CheckTCCRET checkTccRet, out string outMessage)
        {
            int retVal = -1;
            bool isSuccess = false;
            Response response = new Response();
            MySqlConnection connect = getConnection();
            //OracleConnection OConn = new OracleConnection(ConnectionString);
            //OracleCommand OCmd = new OracleCommand();
            try
            {
                if (string.IsNullOrEmpty(checkTccRet.tccRetId))
                {
                    outMessage = "Invalid Request , Can't checked";
                    return isSuccess;
                }
                MySqlCommand cmd = null;
                using (connect)
                {
                    DataTable dt = new DataTable();
                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_TCCERT_ID", checkTccRet.tccRetId);
                    cmd.Parameters.AddWithValue("I_CHECKED_BY", checkTccRet.CheckerId);
                    cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add("O_DATA", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    dt = dm.GetDataTable("SPIMPS_CHECK_TECRET", cmd);

                    retVal = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());
                    con.Close();
                    switch (retVal)
                    {
                        case 0:
                            isSuccess = true;
                            outMessage = Messages.FileHasBeenChecked;
                            break;
                        case 1:
                            outMessage = Messages.RecordNotFound;
                            break;
                        case 2:
                            outMessage = Messages.MakerCantCheck;
                            break;
                        case -1:
                            outMessage = Messages.InternalServerError;
                            break;
                        default:
                            outMessage = Messages.InternalServerError;
                            break;
                    }
                }
            }

            catch (Exception ex)
            {
                outMessage = Messages.ExceptionError;
                string classMethod = ex.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                //cmd = null;
            }
            return isSuccess;
        }
        #endregion
        
        #region 22-rejectTccRet
        public bool rejectTccRet(RejectTCCRET rejectTccRet, out string Msg)
        {
            bool isSuccess = false;
            int retVal = -1;
            Response response = new Response();
            MySqlConnection connect = getConnection();
            Msg = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(rejectTccRet.tccRetId))
                {
                    Msg = "Invalid Request , Can't checked";
                    return isSuccess;
                }
                if (string.IsNullOrEmpty(rejectTccRet.Rejected_Reason))
                {
                    Msg = "Please provide rejected reason";
                    return isSuccess;
                }

                //
                MySqlCommand cmd = null;
                using (connect)
                {
                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("I_REJECTED_BY", rejectTccRet.Rejected_By);
                    cmd.Parameters.AddWithValue("I_TCCERT_ID", rejectTccRet.tccRetId);
                    cmd.Parameters.AddWithValue("I_REJECTED_REASON", rejectTccRet.Rejected_Reason);
                    cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add("O_TCC_RET_ID", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    dm.ExecuteScaler("SPIMPS_REJECT_TCCRET", cmd);

                    retVal = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());
                    con.Close();
                    switch (retVal)
                    {
                        case 0:
                            isSuccess = true;
                            Msg = Messages.FileHasBeenRejected;
                            break;
                        case 1:
                            Msg = Messages.FileAlreadyRejectd;
                            break;
                        case 2:
                            Msg = Messages.MakerCantReject;
                            break;
                        case -1:
                            Msg = Messages.InternalServerError;
                            break;
                        default:
                            Msg = Messages.InternalServerError;
                            break;
                    }
                }
            }

            catch (Exception ex)
            {
                Msg = Messages.ExceptionError;
                string classMethod = ex.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                //oCmd = null;
            }
            return isSuccess;
        }
        #endregion

        #region 13-checkStatusUploadedFilesTCCRET
        public int checkStatusUploadedFilesTCCRET(out List<TccRetREPORT> objList)
        {
            DateTime start = DateTime.Now;
            int retVal = -2;
            //ErrorMessage = string.Empty;
            MySqlConnection connect = getConnection();
            MySqlCommand cmd = null;
            objList = null;
            try
            {
                #region 
               
                using (connect)
                {
                    DataTable Dt = new DataTable();

                    DataManager dm = new DataManager();
                    cmd = new MySqlCommand();
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                    Dt = dm.GetDataTable("SPIMPS_CHK_TCCRET_FLS_UPLD_STS", cmd);


                    /*OCmd.CommandType = CommandType.StoredProcedure;
                OCmd.CommandText = "SPIMPS_CHK_TCCRET_FLS_UPLD_STS";
                OCmd.Connection = OConn;
                OracleParameter OPO_Status = new OracleParameter("O_STATUS", OracleDbType.Int32, ParameterDirection.Output);
                OracleParameter OPO_Data = new OracleParameter("O_DATA", OracleDbType.RefCursor, ParameterDirection.Output);
                OCmd.Parameters.Add(OPO_Status);
                OCmd.Parameters.Add(OPO_Data);
                //
                OracleDataAdapter ODA = new OracleDataAdapter(OCmd);*/

                    objList = new List<TccRetREPORT>();
                    if (Dt != null && Dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in Dt.Rows)
                        {
                            TccRetREPORT objDetails = new TccRetREPORT();
                            objDetails.FileName = Convert.ToString(row["FILE_NAME"]);
                            objDetails.Date = Convert.ToString(row["TCC_RET_DATE"]);
                            objDetails.Status = Convert.ToString(row["STATUS"]);
                            objDetails.StatusRemark = Convert.ToString(row["STATUSREMARK"]);
                            objDetails.UploadedBy = Convert.ToString(row["UPLOADED_BY"]);
                            objList.Add(objDetails);
                        }
                    }
                    retVal = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());
                    Dt = null;
                }
                #endregion
            }
            catch (Exception ex)
            {
               /* Task.Factory.StartNew(delegate {
                    LogHelper.WriteDBError("", ex.Message + " - " + ex.ToString(), "SPIMPS_CHK_TCCRET_FLS_UPLD_STS"
                        , "ReconDAL", "checkStatusUploadedFilesTCCRET");
                });*/
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                DateTime end = DateTime.Now;
               /* Task.Factory.StartNew(delegate {
                    LogHelper.WriteReconLog("NA", " status:" + retVal.ToString() + "Message : SPIMPS_CHK_TCCRET_FLS_UPLD_STS"
                        , "ReconDAL", "checkStatusUploadedFilesTCCRET", "NA", start, end);
                });*/
                con = null; cmd = null;
            }
            return retVal;
        }
        #endregion
    }
}
