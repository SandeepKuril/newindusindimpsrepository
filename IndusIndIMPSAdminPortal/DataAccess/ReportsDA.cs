﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityLayer;
using System.Data;
using MySql.Data.MySqlClient;

namespace DataAccess
{
    public class ReportsDA
    {

        #region getDateFormat
        //working
        private string getDateFormat(string date)
        {
            string[] str = null;
            str = date.Split('-');
            string dd = str[1];
            string mm = str[0];
            string yyyy = str[2];
            if (mm.Length != 2)
            {
                mm = "0" + mm;
            }
            if (dd.Length != 2)
            {
                dd = "0" + dd;
            }
            //YYYY-MM-DD Format
            return yyyy + "-" + mm + "-" + dd;
        }
        #endregion


        #region getReconReport
        public int getReconReport(ReportsRequest request, out DataTable Dt)
        //public int getReconReport(ReportsRequest request, out List<ReconReportsResponse> objListReconReport)
        {
            int retStatus = -1;
            //objListReconReport = null;
            Dt = new DataTable();
            try
            {
                DataManager dm = new DataManager();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Parameters.AddWithValue("I_From_Date", getDateFormat(request.fromDate));
                cmd.Parameters.AddWithValue("I_To_Date", getDateFormat(request.toDate));
                cmd.Parameters.AddWithValue("I_Recon_Status", request.status);
                cmd.Parameters.AddWithValue("I_RRN", request.rrn);
                cmd.Parameters.Add("O_STATUS", MySqlDbType.Int32).Direction = ParameterDirection.Output;
                //DataTable Dt = dm.GetDataTable("sp_GetReconReportByDate", cmd);
                Dt = dm.GetDataTable("sp_GetReconReportByDate", cmd);

                retStatus = Convert.ToInt32(cmd.Parameters["O_STATUS"].Value.ToString());

                //objListReconReport = new List<ReconReportsResponse>();

                //foreach (DataRow dr in Dt.Rows)
                //{
                //    ReconReportsResponse report = new ReconReportsResponse();
                //    report.RRN = dr["RRNNo"].ToString();
                //    report.Amount = dr["AMOUNT"].ToString();
                //    report.ResponseCode = dr["SWITCHRESPONSECODE"].ToString();
                //    report.NpciResponseCode = dr["NPCIRESPONSECODE"].ToString();
                //    report.ReconStatus = dr["FINALSTATUS"].ToString();
                //    objListReconReport.Add(report);
                //}

            }
            catch (Exception ex)
            {
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
            }
            finally
            {

            }
            return retStatus;
        }
        //
        #endregion


    }
}
