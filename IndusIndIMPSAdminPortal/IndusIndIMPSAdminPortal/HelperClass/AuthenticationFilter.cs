﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndusIndIMPSAdminPortal.HelperClass
{
    public class AuthenticationFilter : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                string tempSession = Convert.ToString(context.HttpContext.Session.GetString("AuthenticationToken"));
                string tempAuthCookie = Convert.ToString(context.HttpContext.Request.Cookies["AuthenticationToken"]);
                //Checking value of session and cookie
                if (tempSession != null && tempAuthCookie != null)
                {
                    if (!tempSession.Equals(tempAuthCookie))
                    {
                        ViewResult result = new ViewResult { ViewName = "PortalLogin" };
                        //result.ViewName = "PortalLogin";
                        context.Result = result;
                    }
                }
                else
                {
                    ViewResult result = new ViewResult { ViewName = "DashBoard" };
                    context.Result = result;
                }

            }
            catch (Exception ex)
            {

            }
        }
    }
}
