﻿using CommonLayer;
using DataAccess;
using EntityLayer;
using IndusIndIMPSAdminPortal.HelperClass;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IndusIndIMPSAdminPortal
{
    public static class LoginModel
    {
        static NLogHelper logs = new NLogHelper(LogManager.GetCurrentClassLogger());
        public static Response Login(LoginEL ObjLoginEL)
        {
            Response Objresponse = new Response();
            int i = 0;
            try
            {
                i = LoginDA.Login(ObjLoginEL);
                switch (i)
                {
                    case 1:
                        Objresponse.ResponseStatus = true;
                        break;
                    case 2:
                        Objresponse.ResponseMessage = ConstantResponseMsg.LoginUserNotExist;
                        break;
                    case 3:
                        Objresponse.ResponseMessage = ConstantResponseMsg.LoginUserBlocked;
                        break;
                    case -1:
                        Objresponse.ResponseMessage = ConstantResponseMsg.DBError;
                        break;
                }
            }
            catch(Exception ex)
            {
                Objresponse.ResponseMessage = ConstantResponseMsg.ModelError;
                Task.Factory.StartNew(delegate {
                    logs.Write_Nlog(MethodBase.GetCurrentMethod().Name, ex, LogType.Error, LogFiles.INDIMPS_LogsError);
                });
            }
            return Objresponse;
        }

    }
}
