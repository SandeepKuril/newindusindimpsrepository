﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityLayer;
using DataAccess;
using CommonLayer;

namespace IndusIndIMPSAdminPortal.Models
{
    public class PartnerModel
    {
        //public PartnerViewModel GetPartnerList()
        public List<PartnerEL> GetPartnerList()
        {
            PartnerDA objPartnerList = new PartnerDA();
            //return objPartnerList.Partners;
            return objPartnerList.Partners.ToList();
        }

        public List<PartnerEL> GetPartnerById(int partnerId) 
        {
            PartnerDA objPartnerList = new PartnerDA();
            List<PartnerEL> partners = objPartnerList.GetPartnerById(partnerId);
            return partners;
        }

        public Response insertPartner(PartnerEL partnerData)
        {
            PartnerDA objPartner = new PartnerDA();
            int res = objPartner.insertPartner(partnerData);
            //string Response;

            Response ObjResponse = new Response();

            switch (res)
            {
                case -1:
                    ObjResponse.ResponseMessage = "Error";
                    break;
                case 0:
                    ObjResponse.ResponseStatus = true;
                    ObjResponse.ResponseMessage = ConstantResponseMsg.InsertPartner;
                    break;
                case 1:
                    ObjResponse.ResponseMessage = ConstantResponseMsg.PartnerExist;
                    break;
                default:
                    ObjResponse.ResponseMessage = "Err";
                    break;
            }

            return ObjResponse;
        }

        public Response updatePartner(PartnerEL partnerData)
        {
            PartnerDA objPartner = new PartnerDA();
            int res = objPartner.updatePartner(partnerData);
            //string Response;
            Response ObjResponse = new Response();

            switch (res)
            {
                case -1:
                    ObjResponse.ResponseMessage = "Error";
                    break;
                case 0:
                    ObjResponse.ResponseStatus = true;
                    ObjResponse.ResponseMessage = ConstantResponseMsg.UpdatedPartner;
                    break;
                case 1:
                    ObjResponse.ResponseMessage = ConstantResponseMsg.PartnerExist;
                    break;
                default:
                    ObjResponse.ResponseMessage = "Err";
                    break;
            }
            return ObjResponse;
        }


        public string deletePartner(PartnerEL partnerData)
        {
            PartnerDA objPartner = new PartnerDA();
            int res = objPartner.deletePartner(partnerData);
            string Response;

            switch (res)
            {
                case -1:
                    Response = "Error";
                    break;
                case 0:
                    Response = ConstantResponseMsg.DeletedPartner;
                    break;
                case 1:
                    Response = ConstantResponseMsg.PartnerNotExist;
                    break;
                default:
                    Response = "Err";
                    break;
            }
            return Response;
        }

        public string checkPartner(PartnerEL partnerData)
        {
            PartnerDA objPartner = new PartnerDA();
            int res = objPartner.checkPartner(partnerData);
            string Response;

            switch (res)
            {
                case -1:
                    Response = "Error";
                    break;
                case 0:
                    Response = ConstantResponseMsg.CheckedPartner;
                    break;
                case 1:
                    Response = ConstantResponseMsg.PartnerNotExist;
                    break;
                case 2:
                    Response = ConstantResponseMsg.CheckNotAllowed;
                    break;
                default:
                    Response = "Err";
                    break;
            }
            return Response;
        }


        public string rejectPartner(PartnerEL partnerData)
        {
            PartnerDA objPartner = new PartnerDA();
            int res = objPartner.rejectPartner(partnerData);
            string Response;

            switch (res)
            {
                case -1:
                    Response = "Error";
                    break;
                case 0:
                    Response = ConstantResponseMsg.RejectedPartner;
                    break;
                case 1:
                    Response = ConstantResponseMsg.PartnerNotExist;
                    break;
                case 2:
                    Response = ConstantResponseMsg.RejectNotAllowed;
                    break;
                default:
                    Response = "Err";
                    break;
            }
            return Response;
        }

    }

    
}
