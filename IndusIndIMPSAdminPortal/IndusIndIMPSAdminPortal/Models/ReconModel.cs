﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EntityLayer;
using Microsoft.Extensions.Configuration;
using DataAccess;
using System.Data;
using CommonLayer;
//using DateTime.ParseExact;

namespace IndusIndIMPSAdminPortal.Models
{
    public class ReconModel
    {

        //private bool UploadFileValidation(string NtslFile, string Rawfile, string ReconDate, string ReconCycle, out string Msg)
        private bool UploadFileValidation(string NtslFile, string Rawfile, out string Msg)
        {
            Msg = string.Empty;
            //string getdate = getDateFormat(ReconDate);


            if (Path.GetExtension(NtslFile) != ".xlsx")
            {
                Msg = "Please upload NTSL file with .xlsx or extention";
                return false;
            }
            if (NtslFile.Length > 300)
            {
                Msg = "NTSL File length exceeds, should be less than 300 char";
                return false;
            }
            if (Rawfile.Length > 300)
            {
                Msg = "Raw File length exceeds, should be less than 300 char";
                return false;
            }
            //if (string.IsNullOrEmpty(ReconDate))
            //{
            //    Msg = "Please select recon date";
            //    return false;
            //}
            //if (string.IsNullOrEmpty(ReconCycle))
            //{
            //    Msg = "Please select recon cycle";
            //    return false;
            //}
            if (!NtslFile.Contains("IMPSNTSL"))
            {
                Msg = "NTSL file name changed";
                return false;
            }
            if (!Rawfile.Contains("ACQ"))
            {
                Msg = "Raw file name changed";
                return false;
            }
            //if (!(NtslFile.Contains(getdate)) || (!Rawfile.Contains(getdate)))
            //{
            //    Msg = "Recon date should be same as  NTSL and Raw file date";
            //    return false;
            //}
            if (NtslFile == Rawfile)
            {
                Msg = "NTSL and Raw file name should not be same";
                return false;
            }
            //if (!(NtslFile.ToLower().Contains(ReconCycle.ToLower()) || Rawfile.ToLower().Contains(ReconCycle.ToLower())))
            //{
            //    if (ReconCycle.ToLower() == "all")
            //    {
            //        if (!NtslFile.ToLower().Contains("1C") && Rawfile.ToLower().Contains("1C"))
            //        {
            //            Msg = "In correct file uploaded against selected recon cycle";
            //            return false;
            //        }
            //        else
            //        {
            //            return true;
            //        }
            //    }
            //    else
            //    {
            //        Msg = "In correct file uploaded against selected recon cycle";
            //        return false;
            //    }
            //}

            if (!((NtslFile.Contains("1C") && Rawfile.Contains("1C")) || (NtslFile.Contains("2C") && Rawfile.Contains("2C"))
                || (NtslFile.Contains("3C") && Rawfile.Contains("3C")) || (NtslFile.Contains("4C") && Rawfile.Contains("4C"))
                ))
            {
                if (!((NtslFile.ToLower().Contains("1c") && Rawfile.ToLower().Contains("1c"))
                || (NtslFile.ToLower().Contains("2c") && Rawfile.ToLower().Contains("2c"))
                || (NtslFile.ToLower().Contains("3c") && Rawfile.ToLower().Contains("3c"))
                || (NtslFile.ToLower().Contains("4c") && Rawfile.ToLower().Contains("4c"))
                ))
                {
                    Msg = "Recon cycle should be same for both uploaded files";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }


        public Response uploadNtslRowFile(NTSLEL fileData)
        {
            Response res = new Response();
            ReconFilemaster RFM = null;
            ReconDA reconDa = null;
            long Recon_Id = 0;
            string NTSLFileName = string.Empty;
            string RAWFileName = string.Empty;
            string NTSLFileDirectoryPath = string.Empty;
            string RAWFileDirectoryPath = string.Empty;

            try
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
                string IMPSReconFiles = builder.GetSection("appSettings").GetSection("IMPSReconFiles").Value;

                string Msg = string.Empty;
                #region Validation
                if (!UploadFileValidation(fileData.NtslFile.FileName, fileData.RawFile.FileName, out Msg))
                {
                    res.ResponseMessage = Msg;
                    res.ResponseStatus = false;
                    return res;
                }
                #endregion

                #region Checking for Internet Explorer
                //if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                //{
                //    #region
                //    string[] NPCItestfiles = NTSLFile.FileName.Split(new char[] { '\\' });
                //    NTSLFileName = NPCItestfiles[NPCItestfiles.Length - 1];
                //    //
                //    string[] CBStestfiles = RowFile.FileName.Split(new char[] { '\\' });
                //    RAWFileName = CBStestfiles[CBStestfiles.Length - 1];
                //    #endregion
                //}
                //else
                //{
                NTSLFileName = fileData.NtslFile.FileName;
                RAWFileName = fileData.RawFile.FileName;
                //}
                #endregion

                string ReconDate = NTSLFileName.Substring(11, 2) + "/" + NTSLFileName.Substring(13, 2) + "/20" + NTSLFileName.Substring(15, 2);
                string ReconCycle = NTSLFileName.Substring(18, 2);

                //if(NTSLFileName.Contains("1C") || NTSLFileName.ToLower().Contains("1c"))
                //{
                //    ReconCycle = "1C";
                //}
                //else if(NTSLFileName.Contains("2C") || NTSLFileName.ToLower().Contains("2c")) 
                //{
                //    ReconCycle = "2C";
                //}
                //else if(NTSLFileName.Contains("3C") || NTSLFileName.ToLower().Contains("3c"))
                //{
                //    ReconCycle = "3C";
                //}
                //else if(NTSLFileName.Contains("4C") || NTSLFileName.ToLower().Contains("4c"))
                //{
                //    ReconCycle = "4C";
                //}


                //If Directory (Folder) does not exists. Create it.
                if (!Directory.Exists(IMPSReconFiles))
                {
                    Directory.CreateDirectory(IMPSReconFiles);
                }
                NTSLFileDirectoryPath = Path.Combine(IMPSReconFiles, NTSLFileName);
                RAWFileDirectoryPath = Path.Combine(IMPSReconFiles, RAWFileName);

                if (File.Exists(NTSLFileDirectoryPath))
                {
                    res.ResponseMessage = "NTSL file is already exist in directory.";
                    return res;
                }
                else if (File.Exists(RAWFileDirectoryPath))
                {
                    res.ResponseMessage = "Raw file is already exist in directory.";
                    return res;
                }

                //Upload in Directory for future refrenece
                using (FileStream fsNTSL = new FileStream(NTSLFileDirectoryPath, FileMode.Create))
                {
                    fileData.NtslFile.CopyTo(fsNTSL);
                }

                using (FileStream fsRAW = new FileStream(RAWFileDirectoryPath, FileMode.Create))
                {
                    fileData.RawFile.CopyTo(fsRAW);
                }

                //fileData.NtslFile.CopyTo(new FileStream(NTSLFileDirectoryPath, FileMode.Create));
                //fileData.RawFile.CopyTo(new FileStream(RAWFileDirectoryPath, FileMode.Create));


                //Initialise Object 
                RFM = new ReconFilemaster();
                RFM.UploadedBy = Convert.ToInt64(1001);
                RFM.NTSLFileName = NTSLFileName;
                RFM.NTSLFilePath = NTSLFileDirectoryPath;
                RFM.RAWFileName = RAWFileName;
                RFM.RAWFilePath = RAWFileDirectoryPath;
                RFM.ReconDate = ReconDate;
                RFM.ReconCycle = ReconCycle;

                #region INSER DATA IN RECON FILE MASTER
                reconDa = new ReconDA();
                string Message = string.Empty;

                #region READING FILE USING OPEN XML
                //string Message = string.Empty;
                DataTable NTSLDT = new DataTable();

                if (!FileReading.NTSLxlsFileToDataTable(RFM.NTSLFilePath, out Message, out NTSLDT))
                {
                    res.ResponseData = string.Empty;
                    res.ResponseStatus = false;
                    res.ResponseMessage = Message;

                    //Remove File's from directory
                    if (File.Exists(NTSLFileDirectoryPath))
                        File.Delete(NTSLFileDirectoryPath);
                    if (File.Exists(RAWFileDirectoryPath))
                        File.Delete(RAWFileDirectoryPath);
                    return res;
                }
                #endregion


                if (!reconDa.insertNTSLFileInReconFileMaster(RFM, out Message, out Recon_Id))
                {
                    res.ResponseMessage = Message;
                    res.ResponseStatus = false;
                    //Remove File's from directory
                    if (File.Exists(NTSLFileDirectoryPath))
                        File.Delete(NTSLFileDirectoryPath);
                    if (File.Exists(RAWFileDirectoryPath))
                        File.Delete(RAWFileDirectoryPath);
                    //Remove from table
                    if (Recon_Id > 0)
                        reconDa.deleteReconMaster(Recon_Id, RFM.UploadedBy, Message);

                    RFM = null; reconDa = null;
                    return res;
                }
                #endregion


                #region NTSL DATATABLE TO OBJECT
                List<NTSLData> ntsldata = null;
                if (!FileReading.NTSLDataTableToDBObject(NTSLDT, Recon_Id, out ntsldata, out Message))
                {
                    res.ResponseData = string.Empty;
                    res.ResponseStatus = false;
                    res.ResponseMessage = Message;

                    //Remove File's from directory
                    if (File.Exists(NTSLFileDirectoryPath))
                        File.Delete(NTSLFileDirectoryPath);
                    if (File.Exists(RAWFileDirectoryPath))
                        File.Delete(RAWFileDirectoryPath);
                    //Remove from table
                    if (Recon_Id > 0)
                        reconDa.deleteReconMaster(Recon_Id, RFM.UploadedBy, Message);
                    return res;
                }
                #endregion


                #region CONVERT RAW TO OBJECT
                List<RawDataFile> listRFD = null;
                FileReading objFR = new FileReading();
                string msg = string.Empty;
                if (!objFR.RAWFileToObject(RFM.RAWFilePath, out listRFD, out msg))
                {
                    res.ResponseData = string.Empty;
                    res.ResponseStatus = false;
                    res.ResponseMessage = msg;

                    //Remove File's from directory
                    if (File.Exists(NTSLFileDirectoryPath))
                        File.Delete(NTSLFileDirectoryPath);
                    if (File.Exists(RAWFileDirectoryPath))
                        File.Delete(RAWFileDirectoryPath);
                    //Remove from table
                    if (Recon_Id > 0)
                        reconDa.deleteReconMaster(Recon_Id, RFM.UploadedBy, res.ResponseMessage);

                    //reconDal = null; RFM = null;
                    objFR = null; return res;
                }
                #endregion


                #region INSERT NTSL and Raw DATA
                if (!reconDa.insertNTSLData(ntsldata))
                {
                    //send response to client
                    res.ResponseStatus = false;
                    res.ResponseMessage = "Failed while inserting NTSL data";

                    //Remove File from directory
                    if (File.Exists(NTSLFileDirectoryPath))
                        File.Delete(NTSLFileDirectoryPath);
                    if (File.Exists(RAWFileDirectoryPath))
                        File.Delete(RAWFileDirectoryPath);
                    //Delete from table
                    if (Recon_Id > 0)
                    {
                        reconDa.deleteReconMaster(Recon_Id, RFM.UploadedBy, res.ResponseMessage);
                    }

                    reconDa = null; RFM = null;
                    return res;
                }
                else
                {
                    #region UPDATE RawFilename,RawPathname
                    string errorMsg = string.Empty;
                    if (!reconDa.updateRAWFileInReconFileMaster(RFM, out errorMsg, Recon_Id))
                    {
                        //Remove File's from directory
                        if (File.Exists(NTSLFileDirectoryPath))
                            File.Delete(NTSLFileDirectoryPath);
                        if (File.Exists(RAWFileDirectoryPath))
                            File.Delete(RAWFileDirectoryPath);
                        //Delete from table
                        if (Recon_Id > 0)
                        {
                            reconDa.deleteReconMaster(Recon_Id, RFM.UploadedBy, errorMsg);
                        }
                        res.ResponseMessage = errorMsg;
                        reconDa = null; RFM = null;
                        return res;
                    }
                    #endregion

                    #region Insert Raw Data
                    //long uploadedBy = Convert.ToInt64(UserId);
                    long uploadedBy = Convert.ToInt64(1001);
                    Task.Factory.StartNew(delegate { insertRawFileData(listRFD, uploadedBy, Recon_Id, NTSLFileDirectoryPath, RAWFileDirectoryPath); });
                    res.ResponseMessage = "File uploading is under process. Please wait some times and check status.";
                    res.ResponseStatus = true;
                    //bulkData = null;
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {

            }
            finally
            {
                //reconDal = null;RFM = null; 
            }

            return res;
        }

        private void insertRawFileData(List<RawDataFile> bulkData, long UserId, long Recon_Id, string NTSLFileDirectoryPath, string RAWFileDirectoryPath)
        {
            string message = string.Empty;
            string startTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            ReconDA objRDAL = new ReconDA();
            string count = Convert.ToString(bulkData.Count);

            if (!objRDAL.insertRawFileData(bulkData, UserId, Recon_Id, out message))
            {
                //Remove File's from directory
                if (File.Exists(NTSLFileDirectoryPath))
                    File.Delete(NTSLFileDirectoryPath);
                if (File.Exists(RAWFileDirectoryPath))
                    File.Delete(RAWFileDirectoryPath);
                //Delete from table
                if (Recon_Id > 0)
                    objRDAL.deleteReconMaster(Recon_Id, UserId, message);
            }
            //else
            //{
            //    message = "Insert RAW Data insertation Success";
            //    bool isMatched = objRDAL.matchNTSL_RawData(Recon_Id, out message);
            //    //Write Count code here
            //    if (!isMatched)
            //    {
            //        //Remove File from directory
            //        if (File.Exists(NTSLFileDirectoryPath))
            //            File.Delete(NTSLFileDirectoryPath);
            //        if (File.Exists(RAWFileDirectoryPath))
            //            File.Delete(RAWFileDirectoryPath);
            //        //Delete record from database
            //        if (Recon_Id > 0)
            //            objRDAL.deleteReconMaster(Recon_Id, UserId, message);
            //    }

            //}
            //
            // Task.Factory.StartNew(delegate { LogHelper.Writelog("ReconModel - insertRawFileData : rawFileData inserted into DB TIMESTAMP , RAW_MASTER_ID:" + Convert.ToString(Recon_Id) + " DB status:" + message + ", RowCount -" + count + ", StartTime: " + startTime + ", End Time: " + DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff"), LogType.Information, LogFiles.LogApplication); });
            objRDAL = null; bulkData = null;
        }


        public Response checkStatusUploadedFiles()
        {
            DateTime start = DateTime.Now;
            Response response = new Response();
            ReconDA objReconDA = null;
            List<ReconFileCheker> objReportList = null;
            try
            {
                int retVal = -2; //  -2 Status is Internal Server Error
                objReportList = new List<ReconFileCheker>();
                objReconDA = new ReconDA();
                retVal = objReconDA.checkStatusUploadedFiles(out objReportList);
                switch (retVal)
                {
                    #region
                    case -1:
                        response.ResponseStatus = false;
                        response.ResponseMessage = "Something went wrong while checking status with DB, ErrorCode - R010";
                        break;
                    case -2:
                        response.ResponseStatus = false;
                        response.ResponseMessage = "Something went wrong while checking status with DB, ErrorCode - R011";
                        break;
                    case 0:
                        response.ResponseStatus = true;
                        response.ResponseMessage = ConstantResponseMsg.Success;
                        response.ResponseData = objReportList;
                        break;
                    case 1:
                        response.ResponseStatus = false;
                        response.ResponseMessage = ConstantResponseMsg.notFound;
                        break;
                    default:
                        response.ResponseStatus = false;
                        response.ResponseMessage = "Something went wrong while checking status with DB, ErrorCode - R012";
                        break;
                        #endregion
                }
            }
            catch (Exception ex)
            {
                response.ResponseStatus = false;
                response.ResponseMessage = "Something went wrong,checkStatusUploadedFiles - ErrorCode - R009";
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.Writelog("ReconModel - checkStatusUploadedFiles", ex, LogType.Error, LogFiles.LogsError);
                //});
            }
            finally
            {
                objReconDA = null;
                objReportList = null;
                DateTime end = DateTime.Now;
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog(UserId.ToString(), response.Status + " - " + Convert.ToString(response.Message)
                //    , "ReconModel", "checkStatusUploadedFiles", "NA", start, end);
                //});
            }
            return response;
        }


        public Response getUncheckedRecon()
        {
            Response response = new Response();
            ReconDA objReconDA = null;
            List<ReconFileCheker> objReportList = null;
            try
            {
                int retVal = -2; //  -2 Status is Internal Server Error
                objReportList = new List<ReconFileCheker>();
                objReconDA = new ReconDA();
                retVal = objReconDA.checkStatusUploadedFiles(out objReportList);

                var res = objReportList.FirstOrDefault(i => i.Status == "1");

                switch (retVal)
                {
                    #region
                    case -1:
                        response.ResponseStatus = false;
                        response.ResponseMessage = "Something went wrong while checking status with DB, ErrorCode - R010";
                        break;
                    case -2:
                        response.ResponseStatus = false;
                        response.ResponseMessage = "Something went wrong while checking status with DB, ErrorCode - R011";
                        break;
                    case 0:
                        response.ResponseStatus = true;
                        response.ResponseMessage = ConstantResponseMsg.Success;
                        response.ResponseData = res;
                        break;
                    case 1:
                        response.ResponseStatus = false;
                        response.ResponseMessage = ConstantResponseMsg.notFound;
                        break;
                    default:
                        response.ResponseStatus = false;
                        response.ResponseMessage = "Something went wrong while checking status with DB, ErrorCode - R012";
                        break;
                        #endregion
                }
            }
            catch (Exception ex)
            {
                response.ResponseStatus = false;
                response.ResponseMessage = "Something went wrong,checkStatusUploadedFiles - ErrorCode - R009";
            }
            finally
            {
                objReconDA = null;
                objReportList = null;
            }
            return response;
        }


        public Response checkRecon(CheckRecon checkRecon)
        {
            ReconDA objDA = null;
            string outMessage = string.Empty;
            Response response = new Response();
            try
            {
                objDA = new ReconDA();
                if (!objDA.checkRecon(checkRecon, out outMessage))
                {
                    response.ResponseMessage = outMessage;
                    return response;
                }
                response.ResponseStatus = true;
                response.ResponseData = "";
                response.ResponseMessage = outMessage;
            }
            catch (Exception ex)
            {
                //#region RESPONSE
                ////Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + Convert.ToString(checkRecon.CheckerId);
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response
                //response.Message = Messages.InternalServerError;
                //#endregion
            }
            finally
            {
                //objDAL = null; checkRecon = null;
            }
            return response;
        }


        public Response rejectRecon(RejectRecon rejectRecon)
        {
            ReconDA objDA = null;
            string outMessage = string.Empty;
            Response response = new Response();
            try
            {
                objDA = new ReconDA();
                if (!objDA.rejectRecon(rejectRecon, out outMessage))
                {
                    response.ResponseMessage = outMessage;
                    return response;
                }
                response.ResponseStatus = true;
                response.ResponseData = "";
                response.ResponseMessage = outMessage;
            }

            catch (Exception ex)
            {
                //#region RESPONSE
                ////Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + Convert.ToString(rejectRecon.Rejected_By);
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response
                //response.Message = Messages.InternalServerError;
                //#endregion
            }
            finally
            {
                //objDAL = null; rejectRecon = null;
            }
            return response;
        }

        //public Response getDoReconList(string sessionKey, long MakerId)
        public Response getDoReconList()
        {
            Response response = new Response();
            ReconDA recoDa = new ReconDA();
            List<ReconFileCheker> listPRRC = null;
            string Msg = string.Empty;
            try
            {
                listPRRC = new List<ReconFileCheker>();
                //bool status = recoDal.getDoReconList(sessionKey, out listPRRC, MakerId, out Msg);
                bool status = recoDa.getDoReconList(out listPRRC, out Msg);
                if (status)
                {
                    response.ResponseData = listPRRC;
                    response.ResponseMessage = ConstantResponseMsg.Success;
                    response.ResponseStatus = status;
                }
                else
                {
                    response.ResponseData = "";
                    response.ResponseMessage = Msg;
                    response.ResponseStatus = status;
                }
            }
            catch (Exception ex)
            {
                //#region RESPONSE
                ////Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + Convert.ToString(MakerId);
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response
                //response.Status = false;
                //response.Message = Messages.InternalServerError;
                //#endregion
            }
            finally
            {
                recoDa = null;
            }
            return response;
        }


        public Response doReconProcess(Int64 reconId, Int64 UserId)
        {
            ReconDA objDA = null;
            string outMessage = string.Empty;
            Response response = new Response();
            try
            {
                objDA = new ReconDA();

                if (!objDA.doReconProcess(reconId, UserId, out outMessage))
                {
                    response.ResponseMessage = outMessage;
                    return response;
                }
                response.ResponseStatus = true;
                response.ResponseData = "";
                response.ResponseMessage = outMessage;
            }
            catch (Exception ex)
            {
                #region RESPONSE
                ////Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + Convert.ToString(UserId);
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response
                //response.Message = Messages.InternalServerError;
                #endregion
            }
            finally
            {
                objDA = null;
            }
            return response;
        }

    }
}
