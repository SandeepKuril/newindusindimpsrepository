﻿using CommonLayer;
using DataAccess;
using EntityLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace IndusIndIMPSAdminPortal
{
    public static class RoleModel
    {
        public static Response SaveRole(Role Role)
        {
            Response Objresponse = new Response();

            int i = 0;
            try
            {
                string validData = RoleValidation(Role.RoleName);
                if (validData != null)
                {
                    Role.RoleName = validData;
                    i = RoleDA.SaveRole(Role);
                    switch (i)
                    {
                        case 0:
                            Objresponse.ResponseStatus = true;
                            Objresponse.ResponseMessage = ConstantResponseMsg.SaveRole;
                            break;
                        case 1:
                            Objresponse.ResponseMessage = ConstantResponseMsg.AlreadyExist;
                            break;
                        case 2:
                            Objresponse.ResponseMessage = ConstantResponseMsg.AlreadyExistDeactive;
                            break;
                        case -1:
                            Objresponse.ResponseMessage = ConstantResponseMsg.DBError;
                            break;
                    }
                }
                else{
                    Objresponse.ResponseMessage = "Data is not valid";
                    Objresponse.ResponseStatus = true;
                }
                
            }
            catch (Exception ex)
            {
                Objresponse.ResponseMessage = ConstantResponseMsg.ModelError;
            }
            return Objresponse;
        }
        public static List<Role> GetAllRole()
        {
            Response ObjResponse = new Response();
            DataTable DT = new DataTable();
            Role ObjRoleEL = new Role();
            List<Role> ObjRoleList = new List<Role>();
            try
            {
                DT = RoleDA.GetRole(0);
                if (DT != null || DT.Rows.Count > 0)
                {
                    foreach(DataRow DR in DT.Rows)
                    {
                        Role ObjRole = new Role();

                        ObjRole.RoleId = Convert.ToInt32(DR["Role_Id"]);
                        ObjRole.RoleName = Convert.ToString(DR["Role_Name"]);
                        ObjRole.CreatedOn = Convert.ToString(DR["Created_On"]);
                        ObjRole.UserId = Convert.ToString(DR["Created_By"]);

                        ObjRoleList.Add(ObjRole);
                    }
                }

            }
            catch (Exception ex)
            {
                //ObjRoleEL.ResponseStatus = false; // list;
                //ObjRoleEL.ResponseMessage = ConstantResponseMsg.ModelError; ;
            }
            return ObjRoleList;
        }
        public static Response GetSingleRole(int RoleId)
        {
            Response response = new Response();
            DataTable DT = new DataTable();
            Role ObjRoleEL = new Role();
            try
            {
                if (RoleId != 0)
                {
                    DT = RoleDA.GetRole(RoleId);
                    if (DT != null || DT.Rows.Count > 0)
                    {
                        ObjRoleEL.RoleName = DT.Rows[0]["Role_Name"].ToString();
                        ObjRoleEL.RoleId = Convert.ToInt32(DT.Rows[0]["Role_Id"]);

                        response.ResponseData = ObjRoleEL;
                        response.ResponseStatus = true;
                    }
                    else
                    {
                        response.ResponseData = "Data not found";
                    }
                }
                else
                {
                    response.ResponseMessage = "Selected data are not valid";
                    response.ResponseStatus = true;
                }
                
            }
            catch (Exception ex)
            {
                response.ResponseMessage = ConstantResponseMsg.ModelError; ;
            }
            return response;
        }
        public static Response DeleteRole(int RoleId,string UserId)
        {
            Response response = new Response();
            DataTable DT = new DataTable();
            Role ObjRoleEL = new Role();
            try
            {
                if (RoleId != 0 && UserId != "")
                {
                    int i = RoleDA.DeleteRole(RoleId, UserId);
                    switch (i)
                    {
                        case 0:
                            response.ResponseStatus = true;
                            response.ResponseMessage = ConstantResponseMsg.DeleteRole;
                            break;
                        case -1:
                            response.ResponseStatus = true;
                            response.ResponseMessage = ConstantResponseMsg.DBError;
                            break;
                        default:
                            response.ResponseStatus = true;
                            response.ResponseMessage = ConstantResponseMsg.ModelError;
                            break;
                    }
                }
                else
                {
                    response.ResponseMessage = "Data are not valid";
                    response.ResponseStatus = true;
                }
                
            }
            catch (Exception ex)
            {
                response.ResponseMessage = ConstantResponseMsg.ModelError; ;
            }
            return response;
        }

        public static Response UpdateRole(Role role)
        {
            Response response = new Response();
            DataTable DT = new DataTable();
            Role ObjRoleEL = new Role();
            try
            {
                if (RoleUpdateValidation(role))
                {
                    int i = RoleDA.UpdateRole(role);
                    switch (i)
                    {
                        case 0:
                            response.ResponseStatus = true;
                            response.ResponseMessage = ConstantResponseMsg.UpdateRole;
                            break;
                        case -1:
                            response.ResponseStatus = true;
                            response.ResponseMessage = ConstantResponseMsg.DBError;
                            break;
                        case 1:
                            response.ResponseStatus = true;
                            response.ResponseMessage = ConstantResponseMsg.AlreadyExist;
                            break;
                        case 2:
                            response.ResponseStatus = true;
                            response.ResponseMessage = ConstantResponseMsg.AlreadyExistDeactive;
                            break;
                        default:
                            response.ResponseStatus = true;
                            response.ResponseMessage = ConstantResponseMsg.ModelError;
                            break;
                    }
                }
                else
                {
                    response.ResponseMessage = "Data is not valid";
                    response.ResponseStatus = true;
                }
            }
            catch (Exception ex)
            {
                response.ResponseMessage = ConstantResponseMsg.ModelError; ;
            }
            return response;
        }

        public static string RoleValidation(string role)
        {
            //Role role1 = new Role();
            string roleName = null;
            try
            {
                if(role != null)
                {
                    roleName = role.ToUpper();
                }
            }
            catch(Exception ex)
            {

            }
            return roleName;
        }

        public static bool RoleUpdateValidation(Role role)
        {
            Role role1 = new Role();
            bool val = false;
            try
            {
                if (role.UserId !="" && role.RoleName!="" && role.RoleId != 0)
                {
                  
                    val = true;
                }
            }
            catch (Exception ex)
            {
                val = false;
            }
            return val;
        }
    }
}
