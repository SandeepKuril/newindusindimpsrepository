﻿using DataAccess;
using EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonLayer;
using System.Data;

namespace IndusIndIMPSAdminPortal.Models
{
    public class UserModel
    {
        public List<UserEL> GetUserList()
        {
            UserDA objUserList = new UserDA();
            return objUserList.Users.ToList();
        }

        public Response GetUserById(UserEL data)
        {
            //Response response = new Response();
            //response.ResponseData = objUserById.GetUserById(data);
            //return response;

            UserDA objUserDA = new UserDA();
            Response response = new Response();
            DataTable DT = new DataTable();
            UserEL ObjUserEL = new UserEL();
            try
            {
                DT = objUserDA.GetUserById(data);
                if (DT != null || DT.Rows.Count > 0)
                {
                    ObjUserEL.Login_Username = DT.Rows[0]["Login_Username"].ToString();
                    ObjUserEL.Role_Name = DT.Rows[0]["Role_Name"].ToString();

                    response.ResponseData = ObjUserEL;
                    response.ResponseStatus = true;
                }
                else
                {
                    response.ResponseData = "Data not found";
                }
            }
            catch (Exception ex)
            {
                response.ResponseMessage = ConstantResponseMsg.ModelError; ;
            }
            return response;


        }

        public Response createUser(UserEL data)
        {
            UserDA userDataInsert = new UserDA();
            int res = userDataInsert.createUser(data);
            //string Response;
            Response ObjResponse = new Response();

            switch (res)
            {
                case -1:
                    ObjResponse.ResponseMessage = "Error";
                    break;
                case 0:
                    ObjResponse.ResponseStatus = true;
                    ObjResponse.ResponseMessage = ConstantResponseMsg.InsertUser;
                    break;
                case 1:
                    ObjResponse.ResponseMessage = ConstantResponseMsg.UserExist;
                    break;
                default:
                    ObjResponse.ResponseMessage = "Err";
                    break;
            }
            return ObjResponse;
        }

        public Response updateUser(UserEL data)
        {
            UserDA objUserUpdate = new UserDA();
            int res = objUserUpdate.updateUser(data);
            //string Response;
            Response ObjResponse = new Response();

            switch (res)
            {
                case -1:
                    ObjResponse.ResponseMessage = "Error";
                    break;
                case 0:
                    ObjResponse.ResponseStatus = true;
                    ObjResponse.ResponseMessage = ConstantResponseMsg.UpdatedUser;
                    break;
                case 1:
                    ObjResponse.ResponseMessage = ConstantResponseMsg.UserExist;
                    break;
                default:
                    ObjResponse.ResponseMessage = "Err";
                    break;
            }
            return ObjResponse;
        }

        public Response deleteUser(UserEL data)
        {
            UserDA userDataDelete = new UserDA();
            int res = userDataDelete.deleteUser(data);
            Response ObjResponse = new Response();

            switch (res)
            {
                case -1:
                    ObjResponse.ResponseMessage = "Error";
                    break;
                case 0:
                    ObjResponse.ResponseStatus = true;
                    ObjResponse.ResponseMessage = ConstantResponseMsg.DeletedUser;
                    break;
                case 1:
                    ObjResponse.ResponseMessage = ConstantResponseMsg.UserNotExist;
                    break;
                default:
                    ObjResponse.ResponseMessage = "Err";
                    break;
            }

            return ObjResponse;
        }
    }
}
