﻿using CommonLayer;
using DataAccess;
using EntityLayer;
using IdentityServer3.Core.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace IndusIndIMPSAdminPortal.Models
{
    public class AdjustmentModel
    {
        //Save File
        public Response SaveTccRetData(HttpRequest Request, long UserId)
        {

            string response = string.Empty;
            Response res = new Response();
            string fileExt = string.Empty;
            string TCCRETFileName = string.Empty;
            string TCCRETDirectoryPath = string.Empty;
            TCCRET_File objTCCRET_File = null;
            AdjustmentDA adjustmentDAL = new AdjustmentDA();
            Int64 TccRetId = 0;
            try
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
                var config = builder.Build();
                //con = new MySqlConnection(config.GetSection("ConnectionStrings").GetSection("DevConnection").Value);
                string fileDirectoryPath = config.GetSection("appSettings").GetSection("IMPSReconFiles").Value;
                //string fileDirectoryPath = ConfigurationManager.AppSettings["IMPSReconFiles"].ToString();
                //get all files from Rquest Object
                //HttpFileCollectionBase files = Request.Form.Files[0];
                IFormFile TCC_RETfile = Request.Form.Files[0];
                // readExcel(TCC_RETfile);
                if (TCC_RETfile == null)
                {
                    res.ResponseStatus = false;
                    res.ResponseMessage = "Please select TCC/RET File";
                }
                if (string.IsNullOrEmpty(Convert.ToString(Request.Form["tccRetDate"])))
                {
                    res.ResponseStatus = false;
                    res.ResponseMessage = "Please select date to upload";
                }
                else
                {
                    fileExt = Path.GetExtension(TCC_RETfile.FileName);
                    if (fileExt.CompareTo(".xlsx") == 0)
                    {
                        //Basic Validation
                        #region BASIC VALIDATION
                        TCCRETFileName = TCC_RETfile.FileName;
                        if (!Directory.Exists(fileDirectoryPath))
                        {
                            Directory.CreateDirectory(fileDirectoryPath);
                        }
                        //
                        TCCRETDirectoryPath = Path.Combine(fileDirectoryPath, TCCRETFileName);
                        //
                        objTCCRET_File = new TCCRET_File();
                        objTCCRET_File.FileName = TCCRETFileName;
                        objTCCRET_File.FilePath = TCCRETDirectoryPath;
                        objTCCRET_File.Tcc_Ret_Date = Convert.ToString(Request.Form["tccRetDate"]);

                        #endregion

                        if (File.Exists(TCCRETDirectoryPath))
                        {
                            res.ResponseStatus = false;
                            res.ResponseMessage = "File is already exist in directory.";
                        }
                        else
                        {
                            using (FileStream stream = new FileStream(TCCRETDirectoryPath, FileMode.Create))
                            {
                                //postedFile.CopyTo(stream);
                                //uploadedFiles.Add(fileName);
                                TCC_RETfile.CopyTo(stream);
                            }
                            //Uplod file in direcory
                            
                            if (adjustmentDAL.insertTccRetDesc(objTCCRET_File, UserId, out response, out TccRetId))
                            {
                                #region CONVERT FILE
                                List<TCCRET> bulkData = new List<TCCRET>();
                                if (TCC_RETfile != null)
                                {
                                    //new dated : 10/12/2018
                                    string msg = string.Empty;
                                    DataTable dt = null;
                                    bool ret = FileReading.TCCRETxlsxFileToDataTable(TCCRETDirectoryPath, out msg, out dt);
                                    if (!ret)
                                    {
                                        res.ResponseStatus = false;
                                        res.ResponseMessage = msg;

                                        #region Remove From Directory and Table
                                        if (TccRetId > 0)
                                        {
                                            adjustmentDAL.deleteTCCRETMaster(TccRetId, UserId);
                                        }
                                        File.Delete(TCCRETDirectoryPath);
                                        #endregion

                                        return res;
                                    }
                                    ret = FileReading.TCCRETDataTableToDBObject(dt, TccRetId, out bulkData, out msg);
                                    if (!ret)
                                    {
                                        res.ResponseStatus = false;
                                        res.ResponseMessage = msg;

                                        #region Remove From Directory and Table
                                        if (TccRetId > 0)
                                        {
                                            adjustmentDAL.deleteTCCRETMaster(TccRetId, UserId);
                                        }
                                        File.Delete(TCCRETDirectoryPath);
                                        #endregion

                                        return res;
                                    }
                                    if (bulkData != null || bulkData.Count > 0)
                                    {
                                        #region INSERT TCC/RET DATA
                                        Task.Factory.StartNew(delegate { insertTccRetData(bulkData, UserId, TCCRETDirectoryPath, TccRetId); });
                                        res.ResponseStatus = true;
                                        res.ResponseMessage = "File uploading is under process. Please wait some times and check status.";
                                        //bulkData = null;
                                        #endregion
                                    }
                                    else
                                    {
                                        if (TccRetId > 0)
                                        {
                                            adjustmentDAL.deleteTCCRETMaster(TccRetId, UserId);
                                        }
                                        File.Delete(TCCRETDirectoryPath);
                                        res.ResponseStatus = false;
                                        res.ResponseMessage = "Failed during converting tcc ret file.Please try again";
                                    }

                                }
                                #endregion
                            }
                            else
                            {
                                res.ResponseStatus = false;
                                res.ResponseMessage = response;
                                res.ResponseData = "";
                                System.IO.File.Delete(TCCRETDirectoryPath);
                            }
                        }
                    }
                    else
                    {
                        res.ResponseStatus = false;
                        res.ResponseMessage = "Please select file with excel file with .xlsx extension.";
                    }
                }
            }
            catch (Exception ex)
            {
                #region RESPONSE
                //Log Writing 
                string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + UserId;
                //Task.Factory.StartNew(delegate { LogHelper.WriteLog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                //Response
                res.ResponseStatus = false;
                res.ResponseMessage = "";// Messages.InternalServerError;
                #endregion

                #region Remove From Directory and Table
                if (TccRetId > 0)
                {
                    adjustmentDAL.deleteTCCRETMaster(TccRetId, UserId);
                }
                File.Delete(TCCRETDirectoryPath);
                #endregion
            }
            return res;
        }
        //
        private void insertTccRetData(List<TCCRET> bulkData, long UserId, string TccRetFileDirectoryPath, Int64 tccRetId)
        {
            string message = string.Empty;
            string startTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            AdjustmentDA objRDAL = new AdjustmentDA();
            string count = string.Empty;
            if (!objRDAL.insertTccRetFileData(bulkData, UserId))
            {
                count = bulkData.Count.ToString();
                //Remove File from directory
                System.IO.File.Delete(TccRetFileDirectoryPath);
                //Delete record from database
                if (tccRetId > 0)
                {
                    objRDAL.deleteTCCRETMaster(tccRetId, UserId);
                }
                //send response to client
                message = "Insert TCC/RET Data insertation failed";
            }
            else
            {
                message = "Insert TCC/RET Data insertation Success";
                objRDAL.updateStatusInTCCRETMaster(tccRetId, UserId);
            }
            //
            //Task.Factory.StartNew(delegate { LogHelper.Writelog("ReconModel - insertTCCRETFileData : tccRetFileData inserted into DB TIMESTAMP , TCCRET_MASTER_ID:" + Convert.ToString(tccRetId) + " DB status:" + message + ", RowCount -" + count + ", StartTime: " + startTime + ", End Time: " + DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff"), LogType.Information, LogFiles.LogApplication); });
            objRDAL = null; bulkData = null;
        }

        #region checkStatusUploadedFilesTccRet
        public Response checkStatusUploadedFilesTccRet(long UserId)
        {
            DateTime start = DateTime.Now;
            Response response = new Response();
            AdjustmentDA adjustmentDAL = null;
            List<TccRetREPORT> objList = null;
            try
            {
                int retVal = -2; //  -2 Status is Internal Server Error
                objList = new List<TccRetREPORT>();
                adjustmentDAL = new AdjustmentDA();
                retVal = adjustmentDAL.checkStatusUploadedFilesTCCRET(out objList);
                switch (retVal)
                {
                    #region
                    case -1:
                        response.ResponseStatus = false;
                        response.ResponseMessage = "Something went wrong while checking status with DB, ErrorCode - R010";
                        break;
                    case -2:
                        response.ResponseStatus = false;
                        response.ResponseMessage = "Something went wrong while checking status with DB, ErrorCode - R011";
                        break;
                    case 0:
                        response.ResponseStatus = true;
                        response.ResponseMessage = CommonLayer.Messages.Success;
                        response.ResponseData = objList;
                        break;
                    case 1:
                        response.ResponseStatus = false;
                        response.ResponseMessage = CommonLayer.Messages.notFound;
                        break;
                    default:
                        response.ResponseStatus = false;
                        response.ResponseMessage = "Something went wrong while checking status with DB, ErrorCode - R012";
                        break;
                        #endregion
                }
            }
            catch (Exception ex)
            {
                response.ResponseStatus = false;
                response.ResponseMessage = "Something went wrong,checkStatusUploadedFiles - ErrorCode - R009";
                /*Task.Factory.StartNew(delegate
                {
                    LogHelper.Writelog("ReconModel - checkStatusUploadedFilesTccRet", ex, LogType.Error, LogFiles.LogsError);
                });*/
            }
            finally
            {
                adjustmentDAL = null;
                objList = null;
                DateTime end = DateTime.Now;
                /*Task.Factory.StartNew(delegate
                {
                    LogHelper.WriteReconLog(UserId.ToString(), response.Status + " - " + Convert.ToString(response.Message)
                    , "ReconModel", "checkStatusUploadedFilesTccRet", "NA", start, end);
                });*/
            }
            return response;
        }
        #endregion
    }
}
