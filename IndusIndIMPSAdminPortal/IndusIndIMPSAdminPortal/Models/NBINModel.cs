﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using EntityLayer;

namespace IndusIndIMPSAdminPortal.Models
{
    public class NBINModel
    {
        NBINDA NB = new NBINDA();

        private Response CheckValidity(NBINEL data)
        {
             //bool ValidState =true;
           
            Response response = new Response();
            //Check Bank Name here..
            if (data.BANKNAME.Trim().Length <= 100)
            {
                response.ResponseStatus = true;
            }
            else 
            {
                response.ResponseStatus = false;
                response.ResponseMessage = "Bank name must less than 100 character";
                return response;
            }
            //Check IFSC code here..
            if (data.IFSCCODE.Trim().Length == 4 || data.IFSCCODE.Trim().Length == 11)
            {
                response.ResponseStatus = true;
               

            }
            else
            {
                response.ResponseStatus = false;
                response.ResponseMessage = "IFSCCODE must be 4 or 11 character";
                return response;
            }
            //Check short code here..
            //Check NBIN here
            if (data.NBIN.Trim().Length == 4 && (Convert.ToInt32(data.NBIN) >= 1000))
            {
                response.ResponseStatus = true;
            }
            else
            {
                response.ResponseMessage = "NBIN must be 4 digit";
                response.ResponseStatus = false;
                return response;
            }
            return response;
        }
        public Response Create(NBINEL data)
        {
            Response response = new Response();
            string msg = string.Empty;
            try
            {
                //validation check
                if (CheckValidity(data).ResponseStatus)
                {
                    //response.ResponseMessage = msg;
                    response = NB.CreatNBIN(data);
                    return response;
                    //response.ResponseStatus = false;
                    /*if (NB.CreatNBIN(data).ResponseStatus) {
                        response.ResponseStatus = true;
                        response.ResponseMessage = "Success";
                    }
                    else
                    {
                        response.ResponseMessage = "Failed";
                        response.ResponseStatus = false;
                    }*/
                    
                }
                else
                {
                    //response.ResponseStatus = false;
                    response = CheckValidity(data);
                    return response;
                }
               
            }
            catch(Exception ex) {
                response.ResponseStatus = false;
                response.ResponseMessage = "Can't reached to the server!!!";
            }
            return response;
        }
        public NBINViewModel GetNBINList()
        {
                NBINDA objUserList = new NBINDA();
                return objUserList.getAllNBIN();
            
        }
        public Response GetNBINbyId(int? BankID)
        {
            Response res = new Response();
            try
            {

                NBINDA objUserList = new NBINDA();
                res = objUserList.EditNBIN(BankID);
                res.ResponseStatus = true;
                res.ResponseMessage = "Success";
                return res;
            }
            catch (Exception ex)
            {
                res.ResponseStatus = false;
                res.ResponseMessage = "Failed";
            }

            return res;
        }

        //Checking validity for EditNBIN
        private Response EidtValidity(NBINEL data)
        {
            //bool ValidState =true;

            Response response = new Response();
            //Check Bank Name here..
            if (data.BANKNAME.Trim().Length <= 100)
            {
                response.ResponseStatus = true;
            }
            else
            {
                response.ResponseStatus = false;
                response.ResponseMessage = "Bank name must less than 100 character";
                return response;
            }
            //Check IFSC code here..
            /*if (data.IFSCCODE.Trim().Length == 4 || data.IFSCCODE.Trim().Length == 11)
            {
                response.ResponseStatus = true;


            }
            else
            {
                response.ResponseStatus = false;
                response.ResponseMessage = "IFSCCODE must be 4 or 11 character";
                return response;
            }*/
            //Check short code here..
            //Check IFSC code here
            if (data.IFSCCODE.Trim().Length == 4 || data.IFSCCODE.Trim().Length == 11)
            {
                response.ResponseStatus = true;


            }
            else
            {
                response.ResponseStatus = false;
                response.ResponseMessage = "IFSCCODE must be 4 or 11 character";
                return response;
            }
            //Check NBIN here
            if (data.NBIN.Trim().Length == 4 && (Convert.ToInt32(data.NBIN) >= 1000))
            {
                response.ResponseStatus = true;
            }
            else
            {
                response.ResponseMessage = "NBIN must be 4 digit";
                response.ResponseStatus = false;
                return response;
            }
            return response;
        }
        public Response UpdateNBIN(NBINEL data)
        {
            Response res = new Response();
            try
            {
                if (EidtValidity(data).ResponseStatus) {
                    res = NB.UpdateNBIN(data);
                    return res;
                }
                else
                {
                    res = EidtValidity(data);
                    return res;
                }
                
            }
            catch (Exception ex)
            {
                res.ResponseStatus = false;
                res.ResponseMessage = "Failed";
            }
            return res;
        }

        public Response CheckNBIN(string nbin, int checker, string feedback)
        {
            Response res = new Response();
            try
            {
                res = NB.CheckNBIN(nbin, checker, feedback);
            }
            catch (Exception ex)
            {
                res.ResponseStatus = false;
                res.ResponseMessage = "Failed";
            }
            return res;
        }
    }
}
