﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityLayer;
using DataAccess;
using System.Data;
using CommonLayer;
using System.IO;
using CsvHelper;
using System.Text;

namespace IndusIndIMPSAdminPortal.Models
{
    public class ReportsModel
    {


        public Response getReconReport(ReportsRequest reportRequest, Int64 UserId)
        {
            ReportsDA objDAL = null;
            Response response = new Response();
            //List<ReconReportsResponse> listReconReport = null;
            try
            {
                objDAL = new ReportsDA();
                DataTable dt = null;
                //listReconReport = new List<ReconReportsResponse>();
                int retStatus = objDAL.getReconReport(reportRequest, out dt);
                //int retStatus = objDAL.getReconReport(reportRequest, out listReconReport);
                switch (retStatus)
                {
                    case -1:
                        response.ResponseMessage = ConstantResponseMsg.FailureDuringDBOperation;
                        break;
                    case 0:
                        response.ResponseStatus = true;
                        response.ResponseMessage = ConstantResponseMsg.Success;
                        //response.ResponseData = listReconReport;
                        response.ResponseData = DtToCSV(dt);
                        break;
                    case 1:
                        response.ResponseMessage = ConstantResponseMsg.RecordNotFound;
                        break;
                    default:
                        response.ResponseMessage = ConstantResponseMsg.Fail;
                        break;
                }
            }

            catch (Exception ex)
            {
                #region RESPONSE
                //Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + Convert.ToString(UserId);
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response
                //response.Message = Messages.InternalServerError;
                #endregion
            }
            finally
            {
                objDAL = null;
            }

            return response;
        }

        private string DtToCSV(DataTable dt)
        {
            //StringWriter csvString = new StringWriter();
            //using (var csv = new CsvWriter(csvString))
            //{
            //    foreach (DataColumn column in dt.Columns)
            //    {
            //        csv.WriteField(column.ColumnName);
            //    }
            //    csv.NextRecord();

            //    foreach (DataRow row in dt.Rows)
            //    {
            //        for (var i = 0; i < dt.Columns.Count; i++)
            //        {
            //            csv.WriteField(row[i]);
            //        }
            //        csv.NextRecord();
            //    }
            //}
            //return csvString.ToString();

            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            return sb.ToString();
        }

    }
}
