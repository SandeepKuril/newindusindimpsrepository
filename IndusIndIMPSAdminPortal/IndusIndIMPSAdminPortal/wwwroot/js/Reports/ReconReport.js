﻿
var ReportData = "";
$(document).ready(function () {
    $("#Reports_ID").addClass("active");
    $("#reconReportTable").hide();

    $("#btnDownload").click(function () {
        var fromDate = $('#txtFromDate').val().trim();
        var toDate = $('#txtToDate').val().trim();
        var status = $('#ddlReconStatus').val().trim();
        var rrn = $('#txtRRN').val().trim();

        downloadReport(fromDate, toDate, status, rrn);

    });

});

function downloadReport(_fromDate, _toDate, _status, _rrn) {
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    var Obj = {
        fromdate: _fromDate,
        toDate: _toDate,
        status: _status,
        rrn: _rrn
    };
    $.ajax({
        type: "POST",
        url: "/Reports/getReconReportList",
        data: JSON.stringify(Obj),
        //data: Obj,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (data) {
            //
            if (!data.responseStatus) {
                $.unblockUI();
                alert(data.responseMessage);
                return;
            }
            $.unblockUI();
            alert(data.responseMessage);
            //call function and set MerchantUserId tempData
            var urlexport = "/Reports/getReconReportList";

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = urlexport;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

        },
        error: function (e) {
            $.unblockUI();
            alert('failure');
        }
    });
    $.unblockUI();
}
