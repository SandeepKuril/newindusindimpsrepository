﻿$(document).ready(function () {
    $("#Reports_ID").addClass("active");
    $("#reconReportTable").hide();

    //Get Recon Report List
    $("#btnView").click(function (e) {
        debugger;
        //var res = validationTR();
        //if (res == false) {
        //    return false;
        //}

        getReconReport1();

        //getTestList();

        //var objReconReport = {
        //    fromDate : $('#txtFromDate').val().trim(),
        //    toDate: $('#txtToDate').val().trim(),
        //    status: $('#ddlReconStatus').val().trim(),
        //    rrn: $('#txtRRN').val().trim()
        //}
        //var controllerAction = "/Reports/getReconReportList";
        //getReconReport(objReconReport, controllerAction);
    }); 

    $("#btnDownload").click(function (e) {
        var res = validationTR();
        if (res == false) {
            return false;
        }
        var fromDate = $('#txtFromDate').val().trim();
        var toDate = $('#txtToDate').val().trim();
        var status = $('#ddlReconStatus').val().trim();
        var rrn = $('#txtRRN').val().trim();

        downloadReport(fromDate, toDate, status, rrn);

        //getTestList();
    }); 



});

function validationTR() {
    var isValid = true;
    if ($('#txtFromDate').val().trim() == '') {
        $('#txtFromDate').css('border-color', 'Red');
        $('#txtFromDateMsg').html('Please select from date');
        isValid = false;
    }
    else {
        $('#txtFromDate').css('border-color', 'black');
        $('#txtFromDateMsg').html("");
    }
    if ($('#txtToDate').val().trim() == '') {
        $('#txtToDate').css('border-color', 'Red');
        $('#txtToDateMsg').html('Please select to date');
        isValid = false;
    }
    else {
        $('#txtToDate').css('border-color', 'black');
        $('#txtToDateMsg').html("");
    }
    if ($('#ddlReconStatus').val() == -1) {
        $('#ddlReconStatus').css('border-color', 'Red');
        $('#ddlReconStatusMsg').html('Please select status');
        isValid = false;
    } else {
        $('#ddlReconStatus').css('border-color', 'black');
        $('#ddlReconStatusMsg').html('');
    }
    if ($('#txtRRN').val() != '') {
        if ($('#txtRRN').val().length != 12) {
            $('#txtRRN').css('border-color', 'Red');
            $('#txtRRNMsg').html('RRN length should be 12 digit');
            isValid = false;
        }
    } else {
        $('#txtRRN').css('border-color', 'black');
        $('#txtRRNMsg').html("");
    }
    return isValid;
}

//Recon Report
function getReconReport(objReconReport, controllerAction) {
    //alert("In getReconReport");

    AjaxCall(controllerAction, objReconReport, function (data) {
        
        alert(data.responseStatus)
            if (data.responseStatus == true) {
                showTxnReportTable(data);
                alert(data.responseMessage);
                $("#reconReportTable").show();
                //Reload Page
                //ReloadPage("/User/UserManagement");
                //window.location.href = '/User/UserManagement';
                //$('#divUploadFileStatus').hide();
                //$('#divCheckFile').hide();
                //$('#divProcessFile').show();
                //getReconList();
                //ClearTable();
            }
            else
            {
                $("#reconReportTable").hide();
            }
    });
}


function getReconReport1() {
    var objReconReport = {
        fromDate: $('#txtFromDate').val().trim(),
        toDate: $('#txtToDate').val().trim(),
        status: $('#ddlReconStatus').val().trim(),
        rrn: $('#txtRRN').val().trim()
    }
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        url: urlToController + "/Reports/getReconReportList",
        //data: JSON.stringify(objReconReport),
        type: "GET",
        data: objReconReport,
        contentType: "application/json;charset=utf-8",
        //dataType: "json",
        success: function (result) {
            $.unblockUI();
            if (result.Status == true) {
                if (rrn != '') {
                    //$("#btnDownload").prop("disabled", false);
                    //ReportData = result.Data;
                    showTxnReportTable(result);
                    alert(result.Message);
                    //$("#btnSearch").prop("disabled", true);
                    $("#reconReportTable").show();
                    //$.unblockUI();
                } else {
                    //$.blockUI({ message: null });
                    //destroyReportTable('reportTable');
                    //$("#btnDownload").prop("disabled", true);
                    var dt = new Date();
                    var fileName = "RECON_REPORT_";
                    fileName = fileName + dt.getDate() + (dt.getMonth() + 1) + dt.getFullYear() + "_" + dt.getHours() + dt.getMinutes() + dt.getSeconds();
                    JSONToCSVConvertor(result.responseData, fileName, 'RECON REPORT', true);
                    alert('Recon report file has been downloaded');
                    //alert("File has been downloaded");
                    $.unblockUI();
                }
            }
            else {
                $.unblockUI();
                alert(result.responseMessage);
                //$("#btnDownload").prop("disabled", true);
                //$("#btnSearch").prop("disabled", false);
                //destroyReportTable("reportTable");
            }
        }
    });
    $.unblockUI();
}


function getTestList() {

    var objReconReport = {
        fromDate: $('#txtFromDate').val().trim(),
        toDate: $('#txtToDate').val().trim(),
        status: $('#ddlReconStatus').val().trim(),
        rrn: $('#txtRRN').val().trim()
    }
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        url: "/Reports/getTestList",
        type: "POST",
        data: objReconReport,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $.unblockUI();
            alert("In Success");
            $("#reconReportTable").show();
            alert(result.responseMessage);
            
        },
        error: function (e) {
            $.unblockUI();
            alert("Error");
        }
    });
}


// Binding Table Data
function showTxnReportTable(data) {
    if (data.responseStatus == false) {
        $('#reconReportTable').DataTable().destroy();
        alert(data.responseMessage);
    }
    $('#reconReportTable').DataTable().destroy();
    var t = $('#reconReportTable').DataTable({
        "lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers"
    });
    var iDisplayIndex = 0;
    t.clear().draw();
    $.each(data.responseData, function (index, value) {
        t.row.add([
            iDisplayIndex += 1,
            value.RRNNo,
            value.SwitchRespCode,
            value.RawFileRespCode,
            value.Amount,
            value.ReconStatus,
            value.Message,
            value.OperationAction,
            value.CycleTime,
            value.ReconDate,
            //'<ul>' +
            //    '<li style ="list-style : none; display : inline-block; padding-right: 10px; cursor : pointer;">' +
            //        '<a id="viewMoreTxnRptBtn" onclick = "viewMoreFn(' + value.RRNNo + ')">' +
            //            '<input type="button" id="viewMoreBtn" class="btn btn-primary btn-xs"  value="VIEW MORE" />' +
            //        '</a>' +
            //	'</li>' +
            //'</ul>'
        ]).draw();
    });
}


function downloadReport(_fromDate, _toDate, _status, _rrn) {
    var Obj = {
        fromDate: _fromDate,
        toDate: _toDate,
        status: _status,
        rrn: _rrn
    };
    //$.blockUI({ message: null });
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        data: JSON.stringify(Obj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        url: "/Reports/setReport",
        success: function (data) {
            if (!data.responseStatus) {
                $.unblockUI();
                alert(data.responseMessage);
                //$.unblockUI();
                return;
            }
            $.unblockUI();
            alert(data.responseMessage);
            //call function and set MerchantUserId tempData
            var urlexport = "/Reports/getReconReportList";

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = urlexport;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            //
            $.unblockUI();
        },
        error: function (e) {
            $.unblockUI();
            alert('failure');
            //$.unblockUI();
            //alert(e.responseText());
        }
    });
    $.blockUI({ message: null });
}

