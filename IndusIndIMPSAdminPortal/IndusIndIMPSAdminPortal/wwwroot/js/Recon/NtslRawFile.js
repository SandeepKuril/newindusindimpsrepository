﻿$(document).ready(function () {
    //debugger;
    $("#IR_ID").addClass("active");
    if (Response) {
        $('#lblAlertNTSL').text(Response);
        $("#alertNTSL").modal("show");
    }

    $('#divCheckFile').hide(); 
    $('#divProcessFile').hide();
    getFilesStatus();

    //Get unchecked recon file data
    $("#btnCheck").click(function (e) {
        UncheckReconList();
    });

    //Check Recon
    $("#btnDoCheck").click(function (e) {
        //debugger;
        var objRecon = {
            ReconFileId: $('#ReconFileId').val(),
        }
        var controllerAction = "/Recon/doCheckRecon";
        CheckRecon(objRecon, controllerAction);

    }); 

    //On Reject button click show bootstrap modal
    $("#btnDoReject").click(function (e) {
        //var reject_ID = $(this).data('id');
        $("#rejectRecon").modal("show");
        //$("#Partner_Id_R").val(reject_ID);
    });

    //Reject Recon
    $("#btnRejectRecon").click(function (e) {
        //debugger;
        if ($('#rejectReason').val().trim() == "") {

            $('#rejectReasonMsg').html("Please enter Reason for Rejection.");
            return false;
        }

        var objRecon = {
            ReconFileId: $('#ReconFileId').val(),
            RejectedReason: $('#rejectReason').val()
        }
        var controllerAction = "/Recon/doRejectRecon";
        RejectRecon(objRecon, controllerAction);
    });

    //Get do Recon List
    $("#btnProcess").click(function (e) {
        getReconList();
    });

});


// Get Uploaded file Status
function getFilesStatus() {
    //$.blockUI({ message: null });
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        type: "GET",
        url: "/Recon/checkStatusUploadedFiles",
        success: function (data) {
            $.unblockUI();
            if (data.Status == false) {
                alert(data.responseMessage);
                //$('#divFileUploadStatus').hide();
                //$('#divFileUpload').show();
                return;
            }
            //debugger;
            fillDataInTable(data);
        },
        error: function (e) {
            //alert(e.responseText());
            $.unblockUI();
            alert("error");
        }
    });
}

//Get Unckecked Recon file List
function UncheckReconList() {
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        url: "/Recon/getUncheckedReconList",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        success: function (result) {
            $('#divUploadFileStatus').hide();
            $('#divProcessFile').hide();
            $('#divCheckFile').show(); 

            $('#ReconFileId').val(result.responseData.reconFileId);
            $('#UploadedBy').text(result.responseData.makerId);
            $('#UploadedBy_R').text(result.responseData.makerId);
            $('#ReconDate').text(result.responseData.reconDate);
            $('#ReconDate_R').text(result.responseData.reconDate);
            $('#FileName').text(result.responseData.ntslFileName);
            $('#FileName_R').text(result.responseData.rawFileName);
            $('#Remark').text(result.responseData.remark);
            $('#Remark_R').text(result.responseData.remark);
            $('#SuccessAmt').text(result.responseData.ntslTotalAmount);
            $('#SuccessAmt_R').text(result.responseData.rawTotalAmount);
            $('#SuccessCnt').text(result.responseData.ntslTotalSuccesCountTxn);
            $('#SuccessCnt_R').text(result.responseData.rawTotalSuccesCountTxn);
            $('#FailAmt').text(result.responseData.ntslTotalAmount08);
            $('#FailAmt_R').text(result.responseData.rawTotalAmount08);
            $('#FailCnt').text(result.responseData.ntslTotalSuccesCountTxn08);
            $('#FailCnt_R').text(result.responseData.rawTotalSuccesCountTxn08);
            $.unblockUI();
        },
        error: function (e) {
            alert("error");
            $.unblockUI();
        }
    });
}

function fillDataInTable(data) {
    //debugger;
    var t = $('#uploadStatus').DataTable({
        "lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers",
        retrieve: true
    });
    //debugger;
    var iDisplayIndex = 0;
    t.clear().draw();
    $.each(data.responseData, function (index, value) {
        t.row.add([
            iDisplayIndex += 1,
            value.status,
            value.reconDate,
            value.ntslFileName,
            value.rawFileName,
            value.remark,
            value.makerId
        ]).draw();
    });
}

// Check Recon File
function CheckRecon(objUser, controllerAction) {
    AjaxCall(controllerAction, objUser, function (data) {
        //debugger;
        alert(data.responseMessage)
        if (data.responseStatus) {
            //Reload Page
            //ReloadPage("/User/UserManagement");
            //window.location.href = '/User/UserManagement';
            $('#divUploadFileStatus').hide();
            $('#divCheckFile').hide();
            $('#divProcessFile').show();
            getReconList();
            ClearTable();
        }
    });
}

//Reject Recon File
function RejectRecon(objUser, controllerAction) {
    AjaxCall(controllerAction, objUser, function (data) {
        //debugger;
        alert(data.responseMessage)
        if (data.responseStatus) {
            //Reload Page
            //ReloadPage("/User/UserManagement");
            //window.location.href = '/User/UserManagement';
            $('#divUploadFileStatus').hide();
            $('#divCheckFile').hide();
            $('#divProcessFile').show();
            $("#rejectRecon").modal("hide");
            ClearTable();
        }
    });
}

function getReconList() {
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        url: "/Recon/getDoReconList",
        type: "POST",
        contentType: "application/json;charset=utf-8",
        success: function (result) {
            $("#divProcessFile").show();
            $('#divUploadFileStatus').hide();
            $('#divCheckFile').hide();
            $.unblockUI();
           // showTxnReportTable(result);
            showProcessFileTable(result);
            
        },
        error: function (e) {
             $.unblockUI();
            //alert(e.responseText());
        }
    });
    $.unblockUI();
}

function showProcessFileTable(data) {
    if (data.responseStatus == false) {
        $('#processFile').DataTable().destroy();
        alert(data.responseMessage);
    }
    $('#processFile').DataTable().destroy();
    var t = $('#processFile').DataTable({
        "lengthMenu": [[5, 10, 50, -1], ["All"]],//"lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers"
    });
    t.clear().draw();
    $.each(data.responseData, function (index, value) {
        if (value.status == '5') {
            t.row.add([
                //value.ReconFileId,
                value.makerId,
                value.reconDate,
                value.ntslFileName,
                value.rawFileName,
                '',//value.Remark,
                value.ntslTotalAmount,
                value.ntslTotalSuccesCountTxn,
                value.rawTotalAmount,
                value.rawTotalSuccesCountTxn,
                value.ntslTotalAmount08,
                value.ntslTotalSuccesCountTxn08,
                value.rawTotalAmount08,
                value.rawTotalSuccesCountTxn08,
                '<ul>' +
                '<li style ="list-style : none; display : inline-block; padding-right: 10px; cursor : pointer;">' +
                '<a id="viewMoreTxnRptBtn" onclick = "ProcessFn(' + value.reconFileId + ')">' +
                '<input type="button" id="processBtn" class="btn btn-primary btn-xs"  value="Do Recon" title ="Click to Process" />' +
                '</a>' +
                '</li>' +
                '</ul>'
            ]).draw();
        }
        else
        {
            t.row.add([
                //value.ReconFileId,
                value.makerId,
                value.reconDate,
                value.ntslFileName,
                value.rawFileName,
                '',//value.Remark,
                value.ntslTotalAmount,
                value.ntslTotalSuccesCountTxn,
                value.rawTotalAmount,
                value.rawTotalSuccesCountTxn,
                value.ntslTotalAmount08,
                value.ntslTotalSuccesCountTxn08,
                value.rawTotalAmount08,
                value.rawTotalSuccesCountTxn08,
                '<ul>' +
                '<li style ="list-style : none; display : inline-block; padding-right: 10px; cursor : pointer;">' +
                '<a id="viewMoreTxnRptBtn">' +
                '<input type="button" id="ReconDoneBtn" class="btn btn-primary btn-xs"  value="Recon Done" />' +
                '</a>' +
                '</li>' +
                '</ul>'
            ]).draw();
        }
    });
}

function ProcessFn(reconFileId) {
    var obj = {
        ReconFileId: reconFileId
    }
    var controllerAction = "/Recon/doReconProcess";
    AjaxCall(controllerAction, obj, function (data) {
        //debugger;
        alert(data.responseMessage)
        if (data.responseStatus) {
            ReloadPage("/Recon/NtslRawFile");
            //window.location.href = '/User/UserManagement';
            //$('#divUploadFileStatus').hide();
            //$('#divCheckFile').hide();
            //$('#divProcessFile').show();
        }
    });
}

function ClearTable()
{
    $('#ReconFileId').val('');
    $('#UploadedBy').text('');
    $('#UploadedBy_R').text('');
    $('#ReconDate').text('');
    $('#ReconDate_R').text('');
    $('#FileName').text('');
    $('#FileName_R').text('');
    $('#Remark').text('');
    $('#Remark_R').text('');
    $('#SuccessAmt').text('');
    $('#SuccessAmt_R').text('');
    $('#SuccessCnt').text('');
    $('#SuccessCnt_R').text('');
    $('#FailAmt').text('');
    $('#FailAmt_R').text('');
    $('#FailCnt').text('');
    $('#FailCnt_R').text('');
}