﻿$(document).ready(function () {
    $("#NBIN_ID").addClass("active");

    $('#txtEditBankname').keypress(function (event) {
        $('#lblEditBanknameMgs').hide();
        $('#txtEditBankname').removeClass('border-danger');
    });
    $('#txtEditIFSC').keypress(function (event) {
        $('#lblEditIFSCMgs').hide();
        $('#txtEditIFSC').removeClass('border-danger');
    });
    /*$('#txtEditBankcode').keypress(function (event) {
        $('#txtEditBankcode').removeClass('border-danger');
        
    });*/
   /* $('#txtEditNBIN').keypress(function (event) {
        $('#txtEditNBIN').removeClass('border-danger');
        $('#lblEditNBINMgs').hide();
    });*/
    

    function EditValidation() {
        debugger;
        var EditValidity = true;
        if ($('#txtEditBankname').val().trim() == "") {
            $('#txtEditBankname').addClass('border-danger');
            $('#lblEditBanknameMgs').show();
            $('#lblEditBanknameMgs').html("Bank name is required");
            //$('#txtBankNameError').css('color', 'red');
            EditValidity = false;
            return EditValidity;
        }
        else if ($('#txtEditBankname').val().trim() != "") {
            

            if ($('#txtEditBankname').val().length <= 100) {
                $('#lblEditBanknameMgs').hide();
                $('#txtEditBankname').removeClass('border-danger');
            } else {
                $('#lblEditBanknameMgs').show();
                $('#lblEditBanknameMgs').html("Bank name must be less than 100 character");
                EditValidity = false;
                return EditValidity;

            }
        }



        if ($('#txtEditIFSC').val().trim() == "") {
            $('#txtEditIFSC').addClass('border-danger');
            $('#lblEditIFSCMgs').show();
            /* $('#txtIFSC').removeClass('form-control');*/
            $('#lblEditIFSCMgs').html("IFSC code is required");
            CheckValidity = false;
        }
        else if ($('#txtEditIFSC').val().trim() != "") {

            if ($('#txtEditIFSC').val().length == 4 || $('#txtEditIFSC').val().length == 11) {
                $('#lblEditIFSCMgs').hide();
                $('#lblEditIFSCMgs').removeClass('border-danger');
            } else {
                $('#lblEditIFSCMgs').show();
                $('#lblEditIFSCMgs').html("IFSCCODE must be 4 or 11 character");

            }


        }  
        /*if ($('#txtEditBankcode').val().trim() == "") {
            $('#txtEditBankcode').addClass('border-danger');
            $('#txtShortCodeError').html("This is field required");
            $('#txtShortCodeError').css('color', 'red');
            CheckValidity = false;
        }
        else if ($('#txtEditBankcode').val().trim() != "") {
            $('#txtEditBankcode').removeClass('border-danger');
        }*/

        /*if ($('#txtEditNBIN').val().trim() == "") {
            $('#txtEditNBIN').addClass('border-danger');
            $('#lblEditNBINMgs').show();
            $('#lblEditNBINMgs').html("NBIN is required");
            //$('#txtNBINError').css('color', 'red');
            EditValidity = false;
            return EditValidity;
        }
        else if ($('#txtEditNBIN').val().trim() != "") {
           

            if ($('#txtEditNBIN').val().length == 4) {
                $('#lblEditNBINMgs').hide();
                $('#txtEditNBIN').removeClass('border-danger');
            } else {
                $('#lblEditNBINMgs').show();
                $('#lblEditNBINMgs').html("NBIN must be 4 digit");
                EditValidity = false;
                return EditValidity;

            }
        }*/
        return EditValidity;
    }

    //Clear textboxes and Hide model
    function clearTextBox() {
        $('#txtEditBankname').val("");
        $('#txtEditBankcode').val("");
        $('#txtEditNBIN').val("");
        $('#txtEditIFSC').val("");

        //closeEdit pop up model
        $('#closeEdit').attr('data-dismiss', 'modal').click();
        $('#closeEdit').addClass('data-dismiss', 'modal').click();
        $('#closeEdit').addClass('class', 'close').click();
        $('#closeEdit').click();
        //HideNBIN();
    }
    /*function HideNBIN() {
        //debugger;
        $('#closeEdit').attr('data-dismiss', 'modal').click();
        $('#closeEdit').addClass('data-dismiss', 'modal').click();
        $('#closeEdit').addClass('class', 'close').click();
        $('#closeEdit').click();
        //fadeMyDiv.call($('#success'));
        //$('#success').show();

    }*/
    //clear edit model and also hide

    function ClearEditNBIN() {


        $('#txtEditBankname').val("");
        $('#lblEditBanknameMgs').hide();


        $('#txtEditBankcode').val("");

        $('#txtEditIFSC').val("");
        $('#lblEditIFSCMgs').hide();

        $('#txtEditNBIN').val("");

        HideEditNBIN();
    }
    //to clear edit Input box from NBIN
    function HideEditNBIN() {
        //debugger;
        $('#closeEditbutton').attr('data-dismiss', 'modal').click();
        $('#closeEditbutton').addClass('data-dismiss', 'modal').click();
        $('#closeEditbutton').addClass('class', 'close').click();
        $('#closeEditbutton').click();
        //fadeMyDiv.call($('#success'));
        $('#success').show();

    }
    //Update NBIN----------------------------------------------------------------------------------------
    $('#btnUpdateNBIN').click(function () {
        var res = EditValidation();
        if (res == false) {
            return false;
        }
        else {
            var Obj = {
                BANKNAME: $("#txtEditBankname").val().trim(),
                SHORTCODE: $("#txtEditBankcode").val().trim(),
                NBIN: $("#txtEditNBIN").val().trim(),
                IFSCCODE: $("#txtEditIFSC").val().trim()

            };

            //debugger;
        /* $.blockUI({ message: <img src="~/assets/LoadingImg/LoadImg.gif" /> });*/
            $.blockUI({
                message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
                css: {
                    backgroundColor: 'transparent',
                    border: '0'
                }
            });
            $.ajax({
                url: "/NBIN/UpdateNBIN",
                data: JSON.stringify(Obj),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {

                    if (result.responseStatus) {
                        alert(result.responseMessage);
                        //Clear text boxes
                        //clearTextBox();
                        //debugger;
                        location.reload(true);
                        ClearEditNBIN();
                        $.unblockUI();
                    }
                    else {
                        alert(result.responseMessage);
                        $.unblockUI();
                    }

                },
                error: function (errormessage) {
                    $.unblockUI();
                    alert("Not Inserted!!!");
                }
            });
            $.unblockUI();
        }
    });
});


//Get editData for NBIN
function CallEdit(idd) {
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    //$.blockUI({ message: null });
    //alert(idd);
    var Obj = {
        BANKID: idd
    };

    $.ajax({
        url: "/NBIN/EditNBIN",
        data: JSON.stringify(Obj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            //debugger;
            $.unblockUI();
            if (result.responseStatus) {
                //alert(result.responseData.bankname);
                //alert(result.responseData.shortcode);
                //alert(result.responseData.nbin);
                $('#txtEditBankname').val(result.responseData.bankname);
                $('#txtEditBankcode').val(result.responseData.shortcode);
                $('#txtEditNBIN').val(result.responseData.nbin);
                $('#txtEditIFSC').val(result.responseData.ifsccode);
            }
            
        },
        error: function (errormessage) {
            $.unblockUI();
            alert("Not Inserted!!!");
        }

    });
    $.unblockUI();
}
function clearNBINbox() {
    $('#txtBankName').val("");
    $('#lblBanknameMgs').hide();

    $('#txtIFSC').val("");
    $('#lblIFSCMgs').hide();

    $('#txtShortCode').val("");
    $('#lblBankCodeMgs').hide();

    $('#txtNBIN').val("");
    $('#lblNBINMgs').hide();

}

