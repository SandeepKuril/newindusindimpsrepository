﻿$(document).ready(function () {
    $("#NBIN_ID").addClass("active");

    //
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        type: "GET",
        url: "/NBIN/getAllNBIN",
        success: function (data) {

            fillNBINTables(data);
            $.unblockUI();
        },
        error: function (e) {

            alert(e.responseText());
            $.unblockUI();
        }
    });



    //

  //  debugger;
    allowAlphabets('txtBankName');
    allowAlphabetsNumber('txtIFSC');
    allowAlphabets('txtShortCode');
    allowNumeric('txtNBIN');
    allowAlphabetsNumber('txtNBIN');

    $('#txtBankName').keypress(function (event) {
        $('#txtBankName').removeClass('border-danger');
        $('#lblBanknameMgs').hide();
    });
    $('#txtIFSC').keypress(function (event) {
        $('#txtIFSC').removeClass('border-danger');
        $('#lblIFSCMgs').hide();
    });
    $('#txtShortCode').keypress(function (event) {
        $('#lblBankCodeMgs').hide();
        $('#txtShortCode').removeClass('border-danger');
    });
    $('#txtNBIN').keypress(function (event) {
        $('#txtNBIN').removeClass('border-danger');
        $('#lblNBINMgs').hide();
    });

function CreateValidation() {
    var CheckValidity = true;
    if ($('#txtBankName').val().trim() == "") {
        $('#txtBankName').addClass('border-danger');
        $('#lblBanknameMgs').show();
        $('#lblBanknameMgs').html("Bank name is required");
        //$('#lblBanknameMgs').css('color', 'red');
        CheckValidity = false;
        
    }
    else if ($('#txtBankName').val().trim() != "") {
        if ($('#txtBankName').val().length <= 100) {
            $('#lblBanknameMgs').hide();
            $('#txtBankName').removeClass('border-danger');
        } else {
            $('#lblBanknameMgs').show();
            $('#lblBanknameMgs').html("Bank name must be less than 100 character");
           
        }
    }
   
    if ($('#txtIFSC').val().trim() == "") {
        $('#txtIFSC').addClass('border-danger');
        $('#lblIFSCMgs').show();
       /* $('#txtIFSC').removeClass('form-control');*/
        $('#lblIFSCMgs').html("IFSC code is required");
        CheckValidity = false;
    }
    else if ($('#txtIFSC').val().trim() != "") {

        if ($('#txtIFSC').val().length == 4 || $('#txtIFSC').val().length == 11) {
            $('#lblIFSCMgs').hide();
            $('#txtIFSC').removeClass('border-danger');
        } else {
            $('#lblIFSCMgs').show();
            $('#lblIFSCMgs').html("IFSCCODE must be 4 or 11 character");
           
        }
       
        
    }   
    

    /*if ($('#txtShortCode').val().trim() == "") {
        $('#txtShortCode').addClass('border-danger');
        $('#lblBankCodeMgs').show();
        $('#lblBankCodeMgs').html("Short code is required");
        //$('#txtShortCodeError').css('color', 'red');
        CheckValidity = false;
    }
    else if ($('#txtShortCode').val().trim() != "") {
        $('#lblBankCodeMgs').hide();
        $('#txtShortCode').removeClass('border-danger');
    }*/

    if ($('#txtNBIN').val().trim() == "") {
        $('#lblNBINMgs').show();
        $('#lblNBINMgs').html("NBIN is required");
        $('#txtNBIN').addClass('border-danger');
       //$('#txtNBINError').html("This is field required");
        //$('#txtNBINError').css('color', 'red');
        CheckValidity = false;
    }
    else if ($('#txtNBIN').val().trim() != "") {

        if ($('#txtNBIN').val().length == 4) {
            $('#lblNBINMgs').hide();
            $('#txtNBIN').removeClass('border-danger');
        } else {
            $('#lblNBINMgs').show();
            $('#lblNBINMgs').html("NBIN must be 4 digit");

        }
        
    }
    return CheckValidity;
    }

//Clear text boxes
    function clearTextBox() {
        $('#txtBankName').val("");
        $('#txtIFSC').val("");
        $('#txtShortCode').val("");
        $('#txtNBIN').val("");
        HideNBIN();
    }
    function HideNBIN() {
        //debugger;
        $('#closee').attr('data-dismiss', 'modal').click();
        $('#closee').addClass('data-dismiss', 'modal').click();
        $('#closee').addClass('class', 'close').click();
        $('#closee').click();
        //fadeMyDiv.call($('#success'));
        $('#success').show();

    }

//InsertNBIN-----------------------------------------------------------------------------------
$("#NBINCreate").click(function () {
   
    var res = CreateValidation();
    if (res == false) {
        return false;
    }
    else {
        
        //$.blockUI({ message: null });
        var Obj = {
            BANKNAME: $("#txtBankName").val(),
            IFSCCODE: $("#txtIFSC").val(),
            SHORTCODE: $("#txtShortCode").val().trim(),
            NBIN: $("#txtNBIN").val().trim()

        };
        
       // debugger;
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
       /* $.blockUI({ message: <img src="~/assets/LoadingImg/LoadImg.gif" /> });*/
        $.ajax({
            url: "/NBIN/CreateNBIN",
            data: JSON.stringify(Obj),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                /*debugger;
                alert(result.responseMessage);*/
               // $.unblockUI();
                 //var errorCode = result.ErrorCode;
               // var Message = result.ResponseMessage;
                if (result.responseStatus) {
                    alert(result.responseMessage);
                    //Clear text boxes
                    clearTextBox();
                    location.reload(true);
                    $.unblockUI();
                }
                else {
                    alert(result.responseMessage);
                    $.unblockUI();
                }
                
            },
            error: function (errormessage) {
               
                alert("Can't reached to the server!!!");
                $.unblockUI();
            }
        });
        $.unblockUI();
    }
});


    
function allowNumeric(control) {
    var valLength = $("#" + control).val();
    $("#" + control).on("keypress", function (evt) {
        var keycode = evt.charCode || evt.keyCode;
        if (keycode == 46) {
            return false;
        }
    });

    $("#" + control).keydown(function (e) {
        //var valLength = $("#" + control).val();
        //if (valLength.length < 1) {
        //    var keycode = e.charCode || e.keyCode;
        //    if (keycode == 48) {
        //        return false;
        //    }
        //}
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}

//Allow Alphabets & space only
function allowAlphabets(control) {
    $("#" + control).keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z \t\b]+$");
        //!e.charCode ? e.which : e.charCode
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode)
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            //alert('Please Enter Alphabate only');
            return false;
        }
    });
}

//Allow Alphabets & space only & Numbers
function allowAlphabetsNumber(control) {
    $("#" + control).keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9 ,\t\b]+$");
        //!e.charCode ? e.which : e.charCode
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode)
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            //alert('Please Enter Alphabate only');
            return false;
        }
    });
}

//Allow Alphabets & space only & Numbers
function allowAlphabetsNumberWithoutSpace(control) {
    $("#" + control).keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9,\t\b]+$");
        //!e.charCode ? e.which : e.charCode
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode)
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            //alert('Please Enter Alphabate only');
            return false;
        }
    });
}

//Allow Alphabets &  Numbers
function allowAlphaNum(control) {
    $("#" + control).keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9,\t\b]+$");
        //!e.charCode ? e.which : e.charCode
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode)
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            //alert('Please Enter Alphabate only');
            return false;
        }
    });
}


});

function fillNBINTables(data) {

    var tab = $('#NBINTablesID').DataTable({
        "lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers"
    });
    tab.clear().draw();
    $.each(data.responseData.listNBIN, function (index, value)
    {
       
            if (value.status == '1') {
                tab.row.add([

                    value.bankname,
                    value.shortcode,
                    value.nbin,
                    value.ifsccode,

                    ' <td><a onclick=" CallEdit(' + value.bankid + ')" id="NBINedit" class="edit" data-toggle="modal" data-target="#editNbin"><i class="fa fa-2 fa-pencil"></i></a></td>' ,
                        '<td><a class="check" style="color:grey"><i class="fa fa-2 fa-check" onclick="return false;"></i></a></td>'

                ]).draw();
            }
            else if (value.status == '0' || value.status == '3') {
                tab.row.add([

                    value.bankname,
                    value.shortcode,
                    value.nbin,
                    value.ifsccode,
                    '<td>'+
                    '<a style = "color:grey" id = "NBINedit" class= "edit" onclick = "return false;" > <i class="fa fa-2 fa-pencil"></i></a ></td > ',
                   
                    '<td>' +
                    ' <a class="check" data-toggle="modal" onclick="acceptNBIN(' + value.nbin + ',' + value.createdby + ')"><i class="fa fa-2 fa-check"></i></a>' +
                    ' <a class="check" data-toggle="modal" onclick="rejectNBIN(' + value.nbin + ',' + value.createdby + ')"><i class="fa fa-2 fa-close"></i></a>' +
                    '</td>'
                ]).draw();
            }
          
    });

}