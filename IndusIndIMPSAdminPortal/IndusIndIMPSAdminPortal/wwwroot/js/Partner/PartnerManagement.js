﻿$(document).ready(function () {
    $("#PM_ID").addClass("active");
    // Insert Partner
    $(document).on("click", "#btnInsertPartner", function () {
        //debugger;
        var validP = validatePartnerInsert();
        if (validP == false) {
            return false;
        }
        var partnerData = {
            //Partner_Id: $('#Partner_Id_E').val(),
            Partner_Name: $('#PartnerName').val().trim(),
            Mobile_Number: $('#MobileNumber').val().trim(),
            Landline: $('#Landline').val().trim(),
            Email: $('#EmailID').val().trim(),
            Office_Address: $('#OfficeAddress').val().trim(),
            Pincode: $('#Pincode').val().trim(),
            State: $('#State').val().trim(),
            City: $('#City').val().trim(),
            Pan_Number: $('#PAN').val().trim(),
            Contact_Person_Name: $('#ContactPersonName').val().trim(),
            Contact_Person_Number: $('#ContactPersonNumber').val().trim(),
            Account_Number: $('#AccountNumber').val().trim(),
            Ifsc_Code: $('#IFSC').val().trim(),
            Centeral_Gst_Number: $('#CentralGSTNumber').val().trim(),
            State_Gst_Number: $('#StateGSTNumber').val().trim(),
            Gst_Address: $('#GSTAddress').val().trim(),
            Website_Url: $('#WebsiteURL').val().trim()
        }
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
        $.ajax({
            type: "POST",
            url: "/Partner/InsertPartner",
            data: partnerData,
            //contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.responseStatus)
                {
                    $('#newPartner').modal('hide');
                    //toastr.success("Insert Success", 'Success Alert', { timeOut: 4000 });
                    alert(response.responseMessage);
                    window.location.href = '/Partner/PartnerManagement';
                    $.unblockUI();
                }
                else
                {
                    $.unblockUI();
                    //toastr.success("Insert Failure", 'Failure Alert', { timeOut: 4000 });
                    alert(response.responseMessage);
                }
            },
            error: function () {
                $.unblockUI();
            }
        });
    });


    // View Partner
    $(document).on("click", "#btnViewPartner", function () {
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
        $.ajax({
            type: "POST",
            url: "/Partner/GetPartnerById",
           // data: partnerData,
            data: { 'PartnerId': $(this).data('id') },
            success: function (response) {
                //debugger;
                if (response != null) {

                    // $("#Partner_Id_V").val(response[0].partner_Id);
                    $('#PartnerName_V').text(response[0].partner_Name);
                    $('#MobileNumber_V').text(response[0].mobile_Number);
                    $('#Landline_V').text(response[0].landline);
                    $('#Email_V').text(response[0].email);
                    $('#OfficeAddress_V').text(response[0].office_Address);
                    $('#Pincode_V').text(response[0].pincode);
                    $('#State_V').text(response[0].state);
                    $('#City_V').text(response[0].city);
                    $('#PAN_V').text(response[0].pan_Number);
                    $('#ContactPersonName_V').text(response[0].contact_Person_Name);
                    $('#ContactPersonNumber_V').text(response[0].contact_Person_Number);
                    $('#AccountNumber_V').text(response[0].account_Number);
                    $('#IFSC_V').text(response[0].ifsc_Code);
                    $('#CentralGSTNumber_V').text(response[0].centeral_Gst_Number);
                    $('#StateGSTNumber_V').text(response[0].state_Gst_Number);
                    $('#GSTAddress_V').text(response[0].gst_Address);
                    $('#WebsiteURL_V').text(response[0].website_Url);

                    $("#viewPartner").modal("show");
                    $.unblockUI();
                }
                else {
                    $.unblockUI();
                    alert("Something went wrong");

                }
            },
            error: function () {
                alert("Error");
            }
        });
    });


    //On Edit button click
    $(document).on("click", "#btnEditPartner", function () {
        //$("#editPartner").modal("show");
        var edit_ID = $(this).data('id');
        editPartnerById(edit_ID);
    });

    // Update Partner
    $(document).on("click", "#btnUpdatePartner", function () {
        //debugger;

        var validP = validatePartnerUpdate();
        if (validP == false) {
            return false;
        }

        var partnerData = {
            Partner_Id: $('#Partner_Id_E').val(),
            Partner_Name : $('#PartnerName_E').val(),
            Mobile_Number: $('#MobileNumber_E').val(),
            Landline: $('#Landline_E').val(),
            Email: $('#Email_E').val(),
            Office_Address: $('#OfficeAddress_E').val(),
            Pincode: $('#Pincode_E').val(),
            State: $('#State_E').val(),
            City: $('#City_E').val(),
            Pan_Number: $('#PAN_E').val(),
            Contact_Person_Name: $('#ContactPersonName_E').val(),
            Contact_Person_Number: $('#ContactPersonNumber_E').val(),
            Account_Number: $('#AccountNumber_E').val(),
            Ifsc_Code: $('#IFSC_E').val(),
            Centeral_Gst_Number: $('#CentralGSTNumber_E').val(),
            State_Gst_Number: $('#StateGSTNumber_E').val(),
            Gst_Address: $('#GSTAddress_E').val(),
            Website_Url: $('#WebsiteURL_E').val()
        }
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
        $.ajax({
            type: "POST",
            url: "/Partner/UpdatePartner",
            data: partnerData,
            //contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                //debugger;
                if (response.responseStatus) {
                    $('#editPartner').modal('hide');
                    //toastr.success("Insert Success", 'Success Alert', { timeOut: 4000 });
                    $.unblockUI();
                    alert(response.responseMessage);

                    window.location.href = '/Partner/PartnerManagement';

                }
                else {
                    //toastr.success("Insert Failure", 'Failure Alert', { timeOut: 4000 });
                    $.unblockUI();
                    alert(response.responseMessage);
                }
            },
            error: function () {
            }
        });
    });


    //On Delete button click in list of Partners
    $(document).on("click", "#btnDelete", function () {
        //$("#editPartner").modal("show");
        var delete_ID = $(this).data('id');
        $("#deletePartner").modal("show");
        $("#Partner_Id_D").val(delete_ID);
    });

    // Delete Partner
    $(document).on("click", "#btnDeletePartner", function () {

        //if (confirm(' Are you sure you want to delete this Partner ?'))
        //{
            //var delete_ID = $(this).data('id');
            var partnerData = {
                Partner_Id: $('#Partner_Id_D').val()
            }
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
            $.ajax({
                type: "POST",
                url: "/Partner/DeletePartner",
                data: partnerData,
                //contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response) {
                        //$('#newPartner').modal('hide');
                        //toastr.success("Insert Success", 'Success Alert', { timeOut: 4000 });
                        $.unblockUI();
                        alert("Delete Success");
                        window.location.href = '/Partner/PartnerManagement';

                    }
                    else {
                        //toastr.success("Insert Failure", 'Failure Alert', { timeOut: 4000 });
                        $.unblockUI();
                        alert("Something went wrong");
                    }
                },
                error: function () {
                    $.unblockUI();
                }
            });
        //}
        //else
        //{
        //    return false;
        //}
    });


    // Check Partner
    $(document).on("click", "#btnCheckPartner", function () {
        //$("#editPartner").modal("show");
        if (confirm(' Are you sure you want to check this Partner ?')) {
            var partnerData = {
                Partner_Id: $(this).data('id')
            }
            $.blockUI({
                message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
                css: {
                    backgroundColor: 'transparent',
                    border: '0'
                }
            });
            $.ajax({
                type: "POST",
                url: "/Partner/CheckPartner",
                data: partnerData,
                //contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response) {
                        //alert("Check Success");
                        $.unblockUI();
                        alert(response);
                        window.location.href = '/Partner/PartnerManagement';
                    }
                    else {
                        $.unblockUI();
                        alert("Something went wrong");
                    }
                },
                error: function () {
                    $.unblockUI();
                }
            });
            $.unblockUI();
        }
        else
        {
            return false;
        }
    });


    //On Reject button click in list of Partners
    $(document).on("click", "#btnReject", function () {
        var reject_ID = $(this).data('id');
        $("#rejectPartner").modal("show");
        $("#Partner_Id_R").val(reject_ID);
    });

    // Reject Partner
    $(document).on("click", "#btnRejectPartner", function () {
        //if (confirm(' Are you sure you want to reject this Partner ?'))
        //{
            //var partnerData = {
            //    Partner_Id: $(this).data('id')
            //}

        if ($('#rejectReason').val().trim() == "") {
            
            $('#rejectReasonMsg').html("Please enter Reason for Rejection.");
            return false;
        }

        var partnerData = {
            Partner_Id: $("#Partner_Id_R").val(),
            //Rejected_By: 10001,
            Rejected_Reason: $("#rejectReason").val()
        }
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
            $.ajax({
                type: "POST",
                url: "/Partner/RejectPartner",
                data: partnerData,
                //contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response) {
                        //alert("Check Success");
                        $.unblockUI();
                        alert(response);
                        window.location.href = '/Partner/PartnerManagement';
                    }
                    else {
                        $.unblockUI();
                        alert("Something went wrong");
                    }
                },
                error: function () {
                    $.unblockUI();
                }
            });
        $.unblockUI();
        //}
        //else
        //{
        //    return false;
        //}
    });


    function editPartnerById(id) {
        //var obj = {
        //    User_Id : id
        //}
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
        $.ajax({
            type: "POST",
            url: "/Partner/GetPartnerById",
            //contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(obj),
            data: { 'PartnerId': id },
            dataType: "json",
            success: function (response) {
                //debugger;
                if (response != null) {

                    $("#Partner_Id_E").val(response[0].partner_Id);
                    $('#PartnerName_E').val(response[0].partner_Name);
                    $('#MobileNumber_E').val(response[0].mobile_Number);
                    $('#Landline_E').val(response[0].landline);
                    $('#Email_E').val(response[0].email);
                    $('#OfficeAddress_E').val(response[0].office_Address);
                    $('#Pincode_E').val(response[0].pincode);
                    $('#State_E').val(response[0].state);
                    $('#City_E').val(response[0].city);
                    $('#PAN_E').val(response[0].pan_Number);
                    $('#ContactPersonName_E').val(response[0].contact_Person_Name);
                    $('#ContactPersonNumber_E').val(response[0].contact_Person_Number);
                    $('#AccountNumber_E').val(response[0].account_Number);
                    $('#IFSC_E').val(response[0].ifsc_Code);
                    $('#CentralGSTNumber_E').val(response[0].centeral_Gst_Number);
                    $('#StateGSTNumber_E').val(response[0].state_Gst_Number);
                    $('#GSTAddress_E').val(response[0].gst_Address);
                    $('#WebsiteURL_E').val(response[0].website_Url);

                    $("#editPartner").modal("show");
                    $.unblockUI();
                }
                else {
                    $.unblockUI();
                    alert("Something went wrong");
                }
            },
            error: function () {
                $.unblockUI();
                alert("Error");
            }
        });
        $.unblockUI();
    }


    $(document).on("click", "#ClosePartner", function () {
        $('#PartnerName').val('');
        $('#MobileNumber').val('');
        $('#Landline').val('');
        $('#EmailId').val('');
        $('#OfficeAddress').val('');
        $('#Pincode').val('');
        $('#State').val('');
        $('#City').val('');
        $('#PAN').val('');
        $('#ContactPersonName').val('');
        $('#ContactPersonNumber').val('');
        $('#AccountNumber').val('');
        $('#IFSC').val('');
        $('#CentralGSTNumber').val('');
        $('#StateGSTNumber').val('');
        $('#GSTAddress').val('');
        $('#WebsiteURL').val('');

        $('#PartnerNameMsg').html("");
        $('#MobileNumberMsg').html("");
        $('#LandlineMsg').html("");
        $('#EmailIDMsg').html("");
        $('#OfficeAddressMsg').html("");
        $('#PincodeMsg').html("");
        $('#StateMsg').html("");
        $('#CityMsg').html("");
        $('#PANMsg').html("");
        $('#ContactPersonNameMsg').html("");
        $('#ContactPersonNumberMsg').html("");
        $('#AccountNumberMsg').html("");
        $('#IFSCMsg').html("");
        $('#CentralGSTNumberMsg').html("");
        $('#StateGSTNumberMsg').html("");
        $('#GSTAddressMsg').html("");
        $('#WebsiteURLMsg').html("");
    });


    $(document).on("click", "#ClosePartnerEdit", function () {
        $('#PartnerNameMsg_E').html("");
        $('#MobileNumberMsg_E').html("");
        $('#LandlineMsg_E').html("");
        $('#EmailIDMsg_E').html("");
        $('#OfficeAddressMsg_E').html("");
        $('#PincodeMsg_E').html("");
        $('#StateMsg_E').html("");
        $('#CityMsg_E').html("");
        $('#PANMsg_E').html("");
        $('#ContactPersonNameMsg_E').html("");
        $('#ContactPersonNumberMsg_E').html("");
        $('#AccountNumberMsg_E').html("");
        $('#IFSCMsg_E').html("");
        $('#CentralGSTNumberMsg_E').html("");
        $('#StateGSTNumberMsg_E').html("");
        $('#GSTAddressMsg_E').html("");
        $('#WebsiteURLMsg_E').html("");
    });

});


function validatePartnerInsert()
{
    var isValid = true;
    if ($('#PartnerName').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#PartnerNameMsg').html("Please enter Partner Name.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#PartnerNameMsg').html("");
    }

    if ($('#MobileNumber').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#MobileNumberMsg').html("Please enter Mobile Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#MobileNumberMsg').html("");
    }

    if ($('#Landline').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#LandlineMsg').html("Please enter Landline Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#LandlineMsg').html("");
    }

    if ($('#EmailID').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#EmailIDMsg').html("Please enter Email ID.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#EmailIDMsg').html("");
    }

    if ($('#OfficeAddress').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#OfficeAddressMsg').html("Please enter Office Address.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#OfficeAddressMsg').html("");
    }

    if ($('#Pincode').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#PincodeMsg').html("Please enter Pincode.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#PincodeMsg').html("");
    }

    if ($('#State').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#StateMsg').html("Please enter State.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#StateMsg').html("");
    }

    if ($('#City').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#CityMsg').html("Please enter City.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#CityMsg').html("");
    }

    if ($('#PAN').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#PANMsg').html("Please enter PAN Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#PANMsg').html("");
    }

    if ($('#ContactPersonName').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#ContactPersonNameMsg').html("Please enter Contact Person Name.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#ContactPersonNameMsg').html("");
    }

    if ($('#ContactPersonNumber').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#ContactPersonNumberMsg').html("Please enter Contact Person Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#ContactPersonNumberMsg').html("");
    }

    if ($('#AccountNumber').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#AccountNumberMsg').html("Please enter Account Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#AccountNumberMsg').html("");
    }

    if ($('#IFSC').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#IFSCMsg').html("Please enter IFSC Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#IFSCMsg').html("");
    }

    if ($('#CentralGSTNumber').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#CentralGSTNumberMsg').html("Please enter Central GST Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#CentralGSTNumberMsg').html("");
    }

    if ($('#StateGSTNumber').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#StateGSTNumberMsg').html("Please enter State GST Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#StateGSTNumberMsg').html("");
    }

    if ($('#GSTAddress').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#GSTAddressMsg').html("Please enter GST Address.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#GSTAddressMsg').html("");
    }

    if ($('#WebsiteURL').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#WebsiteURLMsg').html("Please enter Website URL.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#WebsiteURLMsg').html("");
    }

    return isValid;
}


function validatePartnerUpdate() {
    var isValid = true;
    if ($('#PartnerName_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#PartnerNameMsg_E').html("Please enter Partner Name.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#PartnerNameMsg_E').html("");
    }

    if ($('#MobileNumber_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#MobileNumberMsg_E').html("Please enter Mobile Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#MobileNumberMsg_E').html("");
    }

    if ($('#Landline_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#LandlineMsg_E').html("Please enter Landline Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#LandlineMsg_E').html("");
    }

    if ($('#Email_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#EmailIDMsg_E').html("Please enter Email ID.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#EmailIDMsg_E').html("");
    }

    if ($('#OfficeAddress_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#OfficeAddressMsg_E').html("Please enter Office Address.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#OfficeAddressMsg_E').html("");
    }

    if ($('#Pincode_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#PincodeMsg_E').html("Please enter Pincode.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#PincodeMsg_E').html("");
    }

    if ($('#State_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#StateMsg_E').html("Please enter State.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#StateMsg_E').html("");
    }

    if ($('#City_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#CityMsg_E').html("Please enter City.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#CityMsg_E').html("");
    }

    if ($('#PAN_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#PANMsg_E').html("Please enter PAN Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#PANMsg_E').html("");
    }

    if ($('#ContactPersonName_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#ContactPersonNameMsg_E').html("Please enter Contact Person Name.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#ContactPersonNameMsg_E').html("");
    }

    if ($('#ContactPersonNumber_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#ContactPersonNumberMsg_E').html("Please enter Contact Person Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#ContactPersonNumberMsg_E').html("");
    }

    if ($('#AccountNumber_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#AccountNumberMsg_E').html("Please enter Account Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#AccountNumberMsg_E').html("");
    }

    if ($('#IFSC_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#IFSCMsg_E').html("Please enter IFSC Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#IFSCMsg_E').html("");
    }

    if ($('#CentralGSTNumber_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#CentralGSTNumberMsg_E').html("Please enter Central GST Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#CentralGSTNumberMsg_E').html("");
    }

    if ($('#StateGSTNumber_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#StateGSTNumberMsg_E').html("Please enter State GST Number.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#StateGSTNumberMsg_E').html("");
    }

    if ($('#GSTAddress_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#GSTAddressMsg_E').html("Please enter GST Address.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#GSTAddressMsg_E').html("");
    }

    if ($('#WebsiteURL_E').val().trim() == "") {
        //$('#nameB').css('border-color', 'Red');
        $('#WebsiteURLMsg_E').html("Please enter Website URL.");
        isValid = false;
    }
    else {
        //$('#nameB').css('border-color', 'black');
        $('#WebsiteURLMsg_E').html("");
    }

    return isValid;
}