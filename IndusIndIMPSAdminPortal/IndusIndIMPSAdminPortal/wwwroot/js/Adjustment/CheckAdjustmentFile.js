﻿$(document).ready(function () {
    $("#Adjustment_ID").addClass("active");
    MenuIndicator('AdjustmentFileManagement');

    UncheckReconList();
    //
    $('#nameRejectionComment').keyup(CountCharacter);
    $('#nameRejectionComment').keydown(CountCharacter);
    $("#dismis").click(function () {
        $("#nameRejectionComment").val('');
        $('#nameRejectionComment').css('border-color', 'lightgrey');
        $('#nameRMsg').html("");
        $('#CountChar').text(0);
    })

    $('#btnRefresh').click(function () {
        redirect("/AdjustmentFileManagement/CheckAdjustmentFile");
    });

    $("#saveRejectCommentBtn").click(function () {
        var result = validateReject();
        if (result == false) {
            return false;
        }
        var obj = {
            tccRetId: $("#tccRetIdHidden").val(),
            Rejected_Reason: $("#nameRejectionComment").val()
        }
        //$.blockUI({ message: null });
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
        $.ajax({
            url: "/AdjustmentFileManagement/doRejectTCCRET",
            data: JSON.stringify(obj),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                 $.unblockUI();
                //fetch response and display
                if (response.responseStatus) {
                    $('#commentTccRetRejectModal').modal('hide');
                    alert(response.responseMessage);
                    redirect("/AdjustmentFileManagement/CheckAdjustmentFile");
                } else {
                    $('#commentTccRetRejectModal').modal('hide');
                    $("#nameRejectionComment").val('');
                    alert(response.Message);
                }
            },
            error: function (e) {
                $.unblockUI();
                alert(e.responseText());
            }
        });
    });
});

function UncheckReconList() {
    //$.blockUI({ message: null });
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        url: "/AdjustmentFileManagement/getUncheckedTCCRETList",
        type: "POST",
        contentType: "application/json;charset=utf-8",
        success: function (result) {
            var Message = result.responseMessage;
            showTxnReportTable(result);
            $("#TccRetReportTable").show();
            $.unblockUI();
        },
        error: function (e) {
            $.unblockUI();
            alert(e.responseText());
        }
    });
}

function showTxnReportTable(data) {
    if (data.responseStatus == false) {
        $('#TccRetReportTable').DataTable().destroy();
        alert(data.Message);
    }
    $('#TccRetReportTable').DataTable().destroy();
    var t = $('#TccRetReportTable').DataTable({
        "lengthMenu": [[5, 10, 50, -1], ["All"]],//"lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers"
    });
    var iDisplayIndex = 0;
    t.clear().draw();
    $.each(data.responseData, function (index, value) {

        if (value.Status == "1") {
            t.row.add([
                iDisplayIndex += 1,
                value.fileName,
                value.date,
                value.uploadedBy,
                value.statusRemark,
                '<ul>' +
                '<li style ="list-style : none; display : inline-block; padding-right: 10px; cursor : pointer;">' +
                '<a id="checkbtn" onclick = "checkFn(' + value.tccRetId + ')">' +
                '<input type="button" id="processBtn" class="btn btn-primary btn-xs"  value="CHECK" />' +
                '</a>' +
                '</li>' +
                '</ul>',
                '<ul>' +
                '<li style="list-style : none; display : inline-block;   cursor : pointer;">' +
                '<a id="rejectBtn" onclick= "rejectFn(' + value.tccRetId + ')">' +
                '<input type="button" class="col-sm-12 col-lg-12 col-md-12 btn btn-primary" value="REJECT" />' +
                '</a>' +
                '</li>' +
                '</ul>'
            ]).draw();
        }
        //
        if (value.Status != "1") {
            t.row.add([
                iDisplayIndex += 1,
                value.fileName,
                value.date,
                value.uploadedBy,
                value.statusRemark,
                '<ul>' +
                '<li style ="list-style : none; display : inline-block; padding-right: 10px; cursor : pointer;">' +
                '<a id="checkbtn" onclick = "checkFn(' + value.tccRetId + ')">' +
                '<input type="button" id="processBtn" class="btn btn-primary btn-xs" disabled value="CHECK" />' +
                '</a>' +
                '</li>' +
                '</ul>',
                '<ul>' +
                '<li style="list-style : none; display : inline-block;   cursor : pointer;">' +
                '<a id="rejectBtn" onclick= "rejectFn(' + value.tccRetId + ')">' +
                '<input type="button" class="col-sm-12 col-lg-12 col-md-12 btn btn-primary" disabled value="REJECT" />' +
                '</a>' +
                '</li>' +
                '</ul>'
            ]).draw();
        }
    });
}


//
function destroyReportTable(reportType) {
    $('#' + reportType).DataTable().destroy();
    $("#" + reportType).hide();
    var table = $('#' + reportType).DataTable({
        "lengthMenu": [[5, 10, 50, -1], ["All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers"
    });
    table.clear().draw();
    table.destroy();
}
//
function validateReject() {
    var isValid = true;
    if ($('#nameRejectionComment').val() == "") {
        $('#nameRejectionComment').css('border-color', 'Red');
        $('#nameRMsg').html("Please enter reject reasion.");
        isValid = false;
    }
    else {
        $('#nameRejectionComment').css('border-color', 'lightgrey');
        $('#nameRMsg').html("");
    }
    return isValid;
}


//Check Recon
function checkFn(TccRetFileId) {
    if (confirm(' Are you sure you want to check and authorize this file ?')) {
        var obj = {
            tccRetId: TccRetFileId
        }
       // $.blockUI({ message: null });
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
        $.ajax({
            url: "/AdjustmentFileManagement/doCheckTCCRET",
            data: JSON.stringify(obj),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.responseStatus) {
                    // toastr.success(response.Message, 'Success Alert', { timeOut: 4000 });
                    alert(response.responseMessage);
                    redirect("/AdjustmentFileManagement/CheckAdjustmentFile");
                }
               $.unblockUI();
                alert(response.responseMessage);
            },
            error: function (resp) {
               $.unblockUI();
                alert(resp.responseText());
            }
        });

    }
    else {
        return false;
    }
}
//Reject Recon
function rejectFn(id) {
    if (confirm('Are you sure you want to reject this file ?')) {
        $("#tccRetIdHidden").val(id);
        $("#commentTccRetRejectModal").modal();
    }
    else {
        return false;
    }
}
//
function CountCharacter() {
    var lenth = $(this).val().length;
    $('#CountChar').text(lenth);
}
function MenuIndicator(Controller) {
    $('.expansion').attr("aria-expanded", true);
    $('#' + Controller).addClass('in').attr("aria-expanded", true);
}