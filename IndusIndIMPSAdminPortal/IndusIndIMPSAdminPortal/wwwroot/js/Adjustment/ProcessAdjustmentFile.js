﻿$(document).ready(function () {
    $("#Adjustment_ID").addClass("active");
    MenuIndicator('Recon');

    doReconList();
});

function doReconList() {
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    //$.blockUI({ message: null });
    $.ajax({
        url: "/AdjustmentFileManagement/getTccRetList",
        type: "POST",
        contentType: "application/json;charset=utf-8",
        success: function (result) {
            showTxnReportTable(result);
            $("#TccRetReportTable").show();
            $.unblockUI();
        },
        error: function (e) {
            $.unblockUI();
            alert(e.responseText());
        }
    });
}

function showTxnReportTable(data) {

    if (data.responseStatus == false) {
        $('#TccRetReportTable').DataTable().destroy();
        alert(data.Message);
    }
    $('#TccRetReportTable').DataTable().destroy();
    var t = $('#TccRetReportTable').DataTable({
        "lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers"
    });
    var iDisplayIndex = 0;
    t.clear().draw();
    $.each(data.responseData, function (index, value) {

        if (value.Status == 4) {
            t.row.add([
                iDisplayIndex += 1,
                value.fileName,
                value.date,
                value.uploadedBy,
                value.statusRemark,
                '<ul>' +
                '<li style ="list-style : none; display : inline-block; padding-right: 10px; cursor : pointer;">' +
                '<a id="viewMoreTxnRptBtn" onclick = "ProcessFn(' + value.tccRetId + ')">' +
                '<input type="button" id="processBtn" class="btn btn-primary btn-xs"  value="Do Process" />' +
                '</a>' +
                '</li>' +
                '</ul>'
            ]).draw();
        }
        //
        if (value.Status != 4) {
            t.row.add([
                iDisplayIndex += 1,
                value.fileName,
                value.date,
                value.uploadedBy,
                value.statusRemark,
                '<ul>' +
                '<li style ="list-style : none; display : inline-block; padding-right: 10px; cursor : pointer;">' +
                '<a id="viewMoreTxnRptBtn" onclick = "ProcessFn(' + value.tccRetId + ')">' +
                '<input type="button" id="processBtn" class="btn btn-primary btn-xs" disabled  value="Do Process" />' +
                '</a>' +
                '</li>' +
                '</ul>'
            ]).draw();
        }

    });
}

function ProcessFn(Id) {
    var obj = {
        TccRetId: Id
    }
    //$.blockUI({ message: null });
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        url: urlToController + "/AdjustmentFileManagement/doTccRetTxnProcess",
        data: JSON.stringify(obj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (resp) {
            if (resp.Status) {
                alert(resp.Message);
                redirect(urlToController + "/AdjustmentFileManagement/ProcessAdjustmentFile");
                $.unblockUI();
            } else {
                alert(resp.Message);
                $.unblockUI();
            }
        },
        error: function (resp) {
            $.unblockUI();
            alert(resp.responseText());
            $.unblockUI();
        }
    });
}

function destroyReportTable(reportType) {
    $('#' + reportType).DataTable().destroy();
    $("#" + reportType).hide();
    var table = $('#' + reportType).DataTable({
        "lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers"
    });
    table.clear().draw();
    table.destroy();
}
function MenuIndicator(Controller) {
    $('.expansion').attr("aria-expanded", true);
    $('#' + Controller).addClass('in').attr("aria-expanded", true);
}