﻿$(document).ready(function () {
    $("#Adjustment_ID").addClass("active");
    MenuIndicator('Recon');

    dateFormat('#txtDate');

    //
    $('#btnUploadFile').click(function () {

        var val = validate();
        if (val == false) {
            return false;
        }

        if (($("#TccRetFile").val().toLocaleLowerCase().lastIndexOf(".xlsx") == -1)) {
            alert("Please upload a file with .xlsx extension.");
            return false;
        }
        //debugger;
        //$.blockUI({ message: null });
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
        //if (window.FormData !== undefined) {
            if ($('#TccRetFile').val() == '') {
                alert("Please select file");
                $.unblockUI();
            } else {
                
                var TccRetFile = $('#TccRetFile').get(0);
                var file = TccRetFile.files;
                $("#btnUploadFile").prop("disabled", true);
                // Create FormData object  
                var fileData = new FormData();
                fileData.append(file[0].name, file[0]);
                // Adding one more key to FormData object  
                fileData.append("tccRetDate", $("#txtDate").val());
                //fileData.append('tccRetDate', '13/08/2020');
                //ajax call
               /* var AdjData = {
                    FileName: file[0].name,
                    FilePath: file[0],
                    Tcc_Ret_Date: $("#txtDate").val().trim()
                };*/
                $.blockUI({
                    message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
                    css: {
                        backgroundColor: 'transparent',
                        border: '0'
                    }
                });
                $.ajax({
                    url:'/AdjustmentFileManagement/SaveTccRetFile',
                    type: "POST",
                    contentType: false, // Not to set any content header  
                    processData: false, // Not to process data  
                    data: fileData,
                    success: function (result) {
                        debugger;
                        if (result.responseStatus) {
                            alert(result.responseMessage);
                            clearData();    //clear data here
                            $("#btnUploadFile").prop("disabled", false);
                            //redirect(urlToController + "/Recon/CheckTCCRET");
                            $.unblockUI();
                            //$("#btnUploadFile").prop("disabled", false);
                            //$('#divFileUploadStatus').show();
                            //$('#divFileUpload').hide();

                            $('#divFileUploadStatusTccRet').show();
                            $('#divFileUploadTccRet').hide();
                            getFilesStatus();

                            //getFilesStatus();
                        }
                        if (!result.responseStatus) {
                            alert(result.responseMessage);
                            $.unblockUI();
                            $("#btnUploadFile").prop("disabled", false);
                        }
                    },
                    error: function (err) {
                        //alert('Error Occured');
                        $.unblockUI();
                        $("#btnUploadFile").prop("disabled", false);
                        alert(err.responseMessage);
                    }
                });
            }
        
    })

    $('#divFileUploadStatusTccRet').hide();
    $('#divFileUploadTccRet').show();
    //
    $('a#showFileUploadStatusDivTccRet').click(function () {
        $('#divFileUploadStatusTccRet').show();
        $('#divFileUploadTccRet').hide();
        getFilesStatus();
    });
    //
    $('a#showFileUploadDivTccRet').click(function () {
        $('#divFileUploadStatusTccRet').hide();
        $('#divFileUploadTccRet').show();
    });
    $('#btnRefreshTccRet').click(function () {
        getFilesStatus();
    });

});

function validate() {
    var ret = true;
    if ($("#txtDate").val() == "") {
        $('#txtDate').css('border-color', 'Red');
        $('#MsgtxtDate').html('Please select date');
        ret = false;
    } else {
        $('#txtDate').css('border-color', 'black');
        $('#MsgtxtDate').html('');
    }
    if ($("#TccRetFile").val() == "") {
        $('#TccRetFile').css('border-color', 'Red');
        $('#MsgTccRetFile').html('Please upload Adjustment File');
        ret = false;
    } else {
        $('#TccRetFile').css('border-color', 'black');
        $('#MsgTccRetFile').html('');
    }
    return ret;
}
//
function dateFormat(control) {
    var picker = new Pikaday({
        field: $(control)[0],
        format: 'dd/MM/yyyy',
        toString(date, format) {
            // you should do formatting based on the passed format,
            // but we will just return 'D/M/YYYY' for simplicity
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            return `${day}/${month}/${year}`;
        },
        //parse(dateString, format) {
        //    // dateString is the result of `toString` method
        //    const parts = dateString.split('/');
        //    const day = parseInt(parts[0], 10);
        //    const month = parseInt(parts[1] - 1, 10);
        //    const year = parseInt(parts[1], 10);
        //    return new Date(year, month, day);
        //},
        firstDay: 1,
        minDate: new Date('2000-01-01'),
        //maxDate: new Date('2050-12-31'),
        maxDate: new Date(),
        yearRange: [2000, 2050],
        numberOfMonths: 1
    });
}

function clearData() {
    $('#txtDate').val("");
    $('#TccRetFile').val("");

    $('#MsgtxtDate').html("");
    $('#MsgTccRetFile').html("");

    $('#txtDate').css('border-color', 'black');
    $('#TccRetFile').css('border-color', 'black');
}
//
//
function getFilesStatus() {
    //$.blockUI({ message: null });
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        type: "POST",
        url: "/AdjustmentFileManagement/checkStatusUploadedFilesTccRet",
        success: function (data) {
            $.unblockUI();
            if (data.responseStatus == false) {
                alert(data.responseMessage);
                $('#divFileUploadStatusTccRet').hide();
                $('#divFileUploadTccRet').show();
                return;
            }
            fillDataInTable(data);
            
        },
        error: function (e) {
            $.unblockUI();
            alert(e.responseText());
        }
    });
}
//
function fillDataInTable(data) {
    debugger;
    var t = $('#recoTable').DataTable({
        "lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers",
        retrieve: true
    });
    alert("Not Get Error Yet");
    var iDisplayIndex = 0;
    t.clear().draw();
    $.each(data.responseData, function (index, value) {
        t.row.add([
            iDisplayIndex += 1,
            value.status,
            value.date,
            value.fileName,
            value.statusRemark,
            value.uploadedBy
        ]).draw();
    });
}
function MenuIndicator(Controller) {
    $('.expansion').attr("aria-expanded", true);
    $('#' + Controller).addClass('in').attr("aria-expanded", true);
}