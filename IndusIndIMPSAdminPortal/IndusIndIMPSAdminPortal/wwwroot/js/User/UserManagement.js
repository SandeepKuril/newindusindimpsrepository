﻿$(document).ready(function () {
    $("#UM_ID").addClass("active");
    //Create User
    $("#btnCreateUser").click(function (e) {

        if (CreateValidation() == false) {
            return false;
        }

        var objUser = {
            Login_Username: $("#txtUserName").val(),
            Role_Name: $("#drpRole").val()
        }
        var controllerAction = "/User/CreateUser";
        SaveUpdateDeleteUser(objUser, controllerAction);

    });


    //On Edit button click
    //$(document).on("click", ".edit", function () {
    $(".edit").on('click', function (event) {
        var controllerAction = "/User/GetUSerById";
        GetData($(this).data('id'), controllerAction);
    });


    //Update User
    $("#btnUpdateUser").click(function (e) {

        if (UpdateValidation() == false) {
            return false;
        }

        var objUser = {
            User_Id: $('#hdnUserId').val(),
            Login_Username: $("#txtUserName_E").val(),
            Role_Name: $("#drpRole_E").val()
        }
        var controllerAction = "/User/UpdateUser";

        SaveUpdateDeleteUser(objUser, controllerAction);
    });


    //On Delete button Click
    $(".delete").on('click', function (event) {
        $('#hdnUserId').val($(this).data('id'));
    });

    //Delete User Yes
    $("#btnDeleteUserYes").click(function (e) {
       
        var objUser = {
            User_Id: $('#hdnUserId').val(),
        }
        var controllerAction = "/User/DeleteUser";
        SaveUpdateDeleteUser(objUser, controllerAction);

    });

});

// Create/Update/Delete User
function SaveUpdateDeleteUser(objUser, controllerAction) {

    AjaxCall(controllerAction, objUser, function (data) {
        debugger;
        alert(data.responseMessage)
        if (data.responseStatus) {
            //Reload Page
            //ReloadPage("/User/UserManagement");
            window.location.href = '/User/UserManagement';
        }
    });
}

//Get data on Edit click
function GetData(userId, controllerAction) {
    
    var obj = {
        User_Id: userId,
    }
    AjaxCall(controllerAction, obj, function (data) {
        //debugger;
        if (data.responseStatus) {
            //$('#hdnRoleId').val(roleId);
            //$("#txtERoleName").val(data.responseData.roleName);

            $('#hdnUserId').val(userId);
            $('#txtUserName_E').val(data.responseData.login_Username);
            $('#drpRole_E').val(data.responseData.role_Name);
        }
    });
}

//Validation
function CreateValidation() {
    var isValid = true;
    if ($('#txtUserName').val() == "") {
        $('#spnUserName').html("Please enter Login User Name");
        isValid = false;
    } else {
        $('#spnUserName').html("");
    }

    return isValid;
}

function UpdateValidation() {
    var isValid = true;
    if ($('#txtUserName_E').val() == "") {
        $('#spnUserName_E').html("Please enter Login User Name");
        isValid = false;
    } else {
        $('#spnUserName_E').html("");
    }
    
    return isValid;
}
