﻿$(document).ready(function () {
    $("#RM_ID").addClass("active");

    $("#btnDeleteRoleYes").click(function () {
        var Obj = {
            RoleId: parseInt($("#DeleteRoleId").val())
        };
        
        $.ajax({
            url: "/Role/DeleteRole",
            data: JSON.stringify(Obj),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                
                $.unblockUI();
                if (result.responseStatus) {
                    alert(result.responseMessage);
                    location.reload(true);
                 
                }

            },
            error: function (errormessage) {
                $.unblockUI();
                alert("Not Deleted!!!");
            }

        });
        $.unblockUI();
    });
});

function DeleteRole(id) {
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $('#DeleteRoleId').val(id);
    
}