﻿$(document).ready(function () {

    $("#RM_ID").addClass("active");

    //Get Role List
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    $.ajax({
        type: "GET",
        url: "/Role/getRoleList",
        success: function (data) {
           
            fillTables(data);
            $.unblockUI();
        },
        error: function (e) {
            
            alert(e.responseText());
            $.unblockUI();
        }
    });
    

    //End Role List

    //Create Role
    $("#btnCreateRole").click(function() {

        if (CreateValidation() == false) {
            return false;
        }

        var obj = {
            RoleName: $("#txtCRoleName").val(),
        }
        // debugger;
        $.blockUI({
            message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
            css: {
                backgroundColor: 'transparent',
                border: '0'
            }
        });
        /* $.blockUI({ message: <img src="~/assets/LoadingImg/LoadImg.gif" /> });*/
        $.ajax({
            url: "/Role/SaveRole",
            data: JSON.stringify(obj),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                if (result.responseStatus) {
                    alert(result.responseMessage);
                    //Clear text boxes
                    clearTextBox();
                    location.reload(true);
                    $.unblockUI();
                }
                else {
                    alert(result.responseMessage);
                    $.unblockUI();
                }

            },
            error: function (errormessage) {

                alert("Can't reached to the server!!!");
                $.unblockUI();
            }
        });
        $.unblockUI();
        /*var controllerAction = "/Role/SaveRole";
        SaveUpdateDeleteRole(obj, controllerAction);*/

    });

    //Update Role
    $("#btnUpdateRole").click(function (e) {

        if (UpdateValidation() == false) {
            return false;
        }

        var obj = {
            RoleId : $('#hdnRoleId').val(),
            RoleName: $("#txtERoleName").val()
        }
        var controllerAction = "/Role/SaveRole";

        SaveUpdateDeleteRole(obj, controllerAction);
    });

    //Delete Role No
    $("#btnDeleteRoleNo").click(function (e) {
        $(".close").click(); 
    });

    //Delete Role Yes
    /*$("#btnDeleteRoleYes").click(function (e) {

        var obj = {
            RoleId: $('#hdnRoleId').val(),
        }
        var controllerAction = "/Role/DeleteRole";
        SaveUpdateDeleteRole(obj, controllerAction);

    });*/



    //Edit Click
    $(".edit").on('click', function (event) {

        var controllerAction = "/Role/GetSingleRole";
        GetData($(this).data('id'), controllerAction);

    });

    //Delete Click
    $(".delete").on('click', function (event) {

        $('#hdnRoleId').val($(this).data('id'));

    });
    //Edit
    /*$("#btnUpdateRoleName").click(function () {
        var res = UpdateRoleValidation();
        if (res == false) {
            //alert("False data");
            return false;
        }
        else {

            // alert($("#txtERoleName").val().trim() + " " + $("#txtURoleId").val().trim());
            //debugger;
            *//* $.blockUI({ message: <img src="~/assets/LoadingImg/LoadImg.gif" /> });*//*
            $.blockUI({
                message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
                css: {
                    backgroundColor: 'transparent',
                    border: '0'
                }
            });
            var Object = {
                RoleName: $("#txtERoleName").val().trim(),
                RoleId: $("#txtURoleId").val().trim()
            };
            $.ajax({
                url: "/Role/UpdateRoleName",
                data: JSON.stringify(Object),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {

                    if (result.responseStatus) {
                        alert(result.responseMessage);
                        //Clear text boxes
                        //clearTextBox();
                        //debugger;
                        location.reload(true);
                        ClearEditNBIN();
                        $.unblockUI();
                    }
                    else {
                        alert(result.responseMessage);
                        $.unblockUI();
                    }

                },
                error: function (errormessage) {
                    $.unblockUI();
                    alert("Not Inserted!!!");
                }
            });
            $.unblockUI();
        }
    });*/



    //EditEnd

});
//EditCheck
/*function UpdateRoleValidation() {
    if ($("#txtERoleName").val().trim() == "") {
        $("#txtERoleName").css('border', 'solid 1px red');
        $('#spnERoleName').html("Please Enter RoleName");
        return false;
    }
    else {
        $("#txtERoleName").css('border', 'transparent');
        $('#spnERoleName').html("");
        return true;
    }
}
function CallEdit(idd) {
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    //$.blockUI({ message: null });
    // alert(idd);
    var Obj = {
        RoleId: idd
    };

    $.ajax({
        url: "/Role/GetSingleRole",
        data: JSON.stringify(Obj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            //debugger;
            $.unblockUI();
            if (result.responseStatus) {
                //alert(result.responseData.bankname);
                //alert(result.responseData.shortcode);
                //alert(result.responseData.nbin);
                $('#txtERoleName').val(result.responseData.roleName);
                $('#txtURoleId').val(result.responseData.roleId);

            }

        },
        error: function (errormessage) {
            $.unblockUI();
            alert("Not Inserted!!!");
        }

    });
    $.unblockUI();

}*/

//EndEditcheck


//Save Role
/*function SaveUpdateDeleteRole(obj, controllerAction) {

    AjaxCall(controllerAction, obj, function (data) {

        alert(data.responseMessage);
        if (data.responseStatus) {
            //Reload Page
            ReloadPage("/Role/RoleManagement");
        }
    });
}*/
//Get data for Edit
/*function GetData(roleId, controllerAction) {
    var obj = {
        RoleId: roleId,
    }
    AjaxCall(controllerAction, obj, function (data) {

        if (data.responseStatus) {
            $('#hdnRoleId').val(roleId);
            $("#txtERoleName").val(data.responseData.roleName);
        }
    });
}*/

//Validation
function CreateValidation() {
    var isValid = true;
    if ($('#txtCRoleName').val() == "") {
        $('#spnCRoleName').html("Please enter role name");
        $("#txtCRoleName").css('border', 'solid 1px red');
        isValid = false;
    } else {
        $('#spnCRoleName').html("");
    }
    if ($('#txtCRoleName').val() != "") {
        if ($('#txtCRoleName').val().length < 3 || $('#txtCRoleName').val().length > 20) {
            $('#spnCRoleName').html("Role name should be between 3 and 20 characters");
            $("#txtCRoleName").css('border', 'solid 1px red');
            
            isValid = false;
        } else {
            $('#spnCRoleName').html("");
            $("#txtCRoleName").css('border', 'transparent');
        }
    }


    return isValid;
}
//Cleasr textBox
function clearTextBox() {
    $("#txtCRoleName").val("");
    $('#createRole').hide();
}
function UpdateValidation() {
    var isValid = true;
    if ($('#txtERoleName').val() == "") {
        $('#spnERoleName').html("Please enter role name");
        isValid = false;
    } else {
        $('#spnERoleName').html("");
    }
    if ($('#txtERoleName').val() != "") {
        if ($('#txtERoleName').val().length < 3 || $('#txtERoleName').val().length > 20) {
            $('#spnERoleName').html("Role name should be between 3 and 20 characters");
            isValid = false;
        } else {
            $('#spnERoleName').html("");
        }
    }
    return isValid;
}


function Reset() {
    $("#txtCRoleName").val('');
    $("#txtERoleName").val('');
    $('#spnCRoleName').html("");
    $('#spnERoleName').html("");
}

//
/*function fillTables(data) {
    debugger;
   
    $.each(data.responseData, function (index, value) {
        $('#roleManagementTable').find('tbody')
            .append('<tr>').append('<td>' + value.roleName + '</td>')
            .append('<td>' + value.createdOn + '</td>')
            .append('<td>' + value.userId + '</td>')
            .append('<td>' + '<a href="" data-id="' + value.roleId+'" onclick="CallEdit('+value.roleId+')" class="edit" data-toggle="modal" data-target="#editRole"><i class="fa fa-2 fa-pencil"></i></a>' + '</td>')
            .append('<td>' + '<a href="" data-id="'+value.roleId+'" onclick="DeleteRole('+value.roleId+')" class="delete" data-toggle="modal" data-target="#deleteRole"><i class="fa fa-2 fa-trash"></i></a>' + '</td>')
            .append('</tr>');
    });
       
}*/
function fillTables(data) {
    var tab = $('#roleManagementTables').DataTable({
        "lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        scrollCollapse: true,
        "pagingType": "full_numbers"
    });
    var iDisplayIndex = 0;
    tab.clear().draw();

    $.each(data.responseData, function (index, value) {
        tab.row.add([
            
             value.roleName ,
             value.createdOn,
             value.userId ,
            
            '<td>' + '<a href="" data-id="' + value.roleId + '" onclick="CallEdit(' + value.roleId + ')" class="edit" data-toggle="modal" data-target="#editRole"><i class="fa fa-2 fa-pencil"></i></a>' + '</td>' ,
            '<td>' + '<a href="" data-id="' + value.roleId + '" onclick="DeleteRole(' + value.roleId + ')" class="delete" data-toggle="modal" data-target="#deleteRole"><i class="fa fa-2 fa-trash"></i></a>' + '</td>' 
            
        ]).draw();

    });
       
}