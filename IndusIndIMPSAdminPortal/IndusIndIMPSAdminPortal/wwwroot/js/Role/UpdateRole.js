﻿$(document).ready(function () {
    $("#RM_ID").addClass("active");

    $("#btnUpdateRoleName").click(function () {
        var res = UpdateRoleValidation();
        if (res == false) {
            return false;
        }
        else {

            $.blockUI({
                message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
                css: {
                    backgroundColor: 'transparent',
                    border: '0'
                }
            });
           
            var obj = {
                RoleName: $("#txtERoleName").val(),
                RoleId: parseInt($("#txtURoleId").val())
            };
            
            $.ajax({
                url: "/Role/UpdateRoleName",
                data: JSON.stringify(obj),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {

                    if (result.responseStatus) {
                        alert(result.responseMessage);
                        location.reload(true);
                        $.unblockUI();
                    }
                    else {
                        alert(result.responseMessage);
                        $.unblockUI();
                    }

                },
                error: function (errormessage) {
                    $.unblockUI();
                    alert("Not Inserted!!!");
                }
            });
            $.unblockUI();
        }
    });

});
function UpdateRoleValidation() {
    if ($("#txtERoleName").val().trim() == "") {
        $("#txtERoleName").css('border', 'solid 1px red');
        $('#spnERoleName').html("Please Enter RoleName");
        return false;
    }
    else {
        $("#txtERoleName").css('border', 'transparent');
        $('#spnERoleName').html("");
        return true;
    }
}
function CallEdit(idd) {
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
 
    var Obj = {
        RoleId: idd
    };

    $.ajax({
        url: "/Role/GetSingleRole",
        data: JSON.stringify(Obj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            
            $.unblockUI();
            if (result.responseStatus) {
                $('#txtERoleName').val(result.responseData.roleName);
                $('#txtURoleId').val(result.responseData.roleId);

            }

        },
        error: function (errormessage) {
            $.unblockUI();
            alert("Not Inserted!!!");
        }

    });
    $.unblockUI();

}