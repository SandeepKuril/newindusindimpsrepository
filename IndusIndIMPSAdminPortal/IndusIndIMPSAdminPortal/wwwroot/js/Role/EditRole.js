﻿$(document).ready(function () {
    $("#RM_ID").addClass("active");

    $("#btnUpdateRoleName").click(function () {
        var res = UpdateRoleValidation();
        if (res == false) {
            //alert("False data");
            return false;
        }
        else
        {
            
           // alert($("#txtERoleName").val().trim() + " " + $("#txtURoleId").val().trim());
            //debugger;
            /* $.blockUI({ message: <img src="~/assets/LoadingImg/LoadImg.gif" /> });*/
            $.blockUI({
                message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
                css: {
                    backgroundColor: 'transparent',
                    border: '0'
                }
            });
            var obj = {
                RoleName: $("#txtERoleName").val().trim(),
                RoleId: $("#txtURoleId").val().trim()
            };
            alert($("#txtERoleName").val().trim());

            $.ajax({
                url: "/Role/UpdateRoleName",
                data: JSON.stringify(obj),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {

                    if (result.responseStatus) {
                        alert(result.responseMessage);
                        //Clear text boxes
                        //clearTextBox();
                        //debugger;
                        location.reload(true);
                        ClearEditNBIN();
                        $.unblockUI();
                    }
                    else {
                        alert(result.responseMessage);
                        $.unblockUI();
                    }

                },
                error: function (errormessage) {
                    $.unblockUI();
                    alert("Not Inserted!!!");
                }
            });
            $.unblockUI();
        }
    });
   

});
function UpdateRoleValidation() {
    if ($("#txtERoleName").val().trim() == "") {
        $("#txtERoleName").css('border', 'solid 1px red');
        $('#spnERoleName').html("Please Enter RoleName");
        return false;
    }
    else {
        $("#txtERoleName").css('border', 'transparent');
        $('#spnERoleName').html("");
        return true;
    }
}
function CallEdit(idd) {
    $.blockUI({
        message: '<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" />',
        css: {
            backgroundColor: 'transparent',
            border: '0'
        }
    });
    //$.blockUI({ message: null });
   // alert(idd);
    var Obj = {
        RoleId: idd
    };

    $.ajax({
        url: "/Role/GetSingleRole",
        data: JSON.stringify(Obj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            //debugger;
            $.unblockUI();
            if (result.responseStatus) {
                //alert(result.responseData.bankname);
                //alert(result.responseData.shortcode);
                //alert(result.responseData.nbin);
                $('#txtERoleName').val(result.responseData.roleName);
                $('#txtURoleId').val(result.responseData.roleId);
               
            }

        },
        error: function (errormessage) {
            $.unblockUI();
            alert("Not Inserted!!!");
        }

    });
    $.unblockUI();

}

