function showPassword() {
  var x = document.getElementById("passwordInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function isAlphaNumeric(event){
    var regex = new RegExp("^[a-zA-Z0-9\b]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
}
function alphaOnly(event) {
  //var key = event.keyCode;
  //return ((key >= 65 && key <= 90) || key == 8);
    var regex = new RegExp("^[a-zA-Z ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

};

$(document).ready(function() {
    var table = $('#roleManagementTable').DataTable( {
        lengthChange: true,
        language: {  
        	"search": "_INPUT_",
        	"searchPlaceholder": "Search",
        	"lengthMenu":' <div class="select-box"><select class="form-control">'+
						      '<option value="10">10 per page</option>'+
						      '<option value="20">20 per page</option>'+
						      '<option value="30">30 per page</option>'+
						      '<option value="40">40 per page</option>'+
						      '<option value="50">50 per page</option>'+
						      '<option value="-1">All</option>'+
						      '</select></div> ',
			

        }
    } );
    var table = $('#uploadStatus, #processFile').DataTable( {
        lengthChange: true,
        language: {  
            "search": "_INPUT_",
            "searchPlaceholder": "Search",
            "lengthMenu":' <div class="select-box"><select class="form-control">'+
                              '<option value="10">10 per page</option>'+
                              '<option value="20">20 per page</option>'+
                              '<option value="30">30 per page</option>'+
                              '<option value="40">40 per page</option>'+
                              '<option value="50">50 per page</option>'+
                              '<option value="-1">All</option>'+
                              '</select></div> ',
            

        }
    } );
});

$('.user-login').click(function(){
	$('.user-options').toggle();
})
$(function () {
  $("#datepicker, #datepicker1, #instlDate").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update', new Date());
});
$(document).ready(function() {
   window.setTimeout("fadeMyDiv();", 6000); //call fade in 3 seconds
 }
)

function fadeMyDiv() {
   $(".fade-alert").fadeOut('slow');
}