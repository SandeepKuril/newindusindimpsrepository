using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using System;
using System.IO;

namespace IndusIndIMPSAdminPortal
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
           CreateHostBuilder(args).Build().Run();
           
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
           Host.CreateDefaultBuilder(args).ConfigureLogging((hostingContext, logging) =>
           {
               logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
               logging.AddConsole();
               logging.AddDebug();
               logging.AddEventSourceLogger();
               logging.AddNLog();

           })
               .ConfigureWebHostDefaults(webBuilder =>
               {
                   webBuilder.UseStartup<Startup>();
               });
        
        
        //public static IHostBuilder CreateHostBuilder(string[] args) =>

           /*Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseKestrel(c => c.AddServerHeader = false);
                    webBuilder.UseIIS();
                    webBuilder.ConfigureLogging(logging =>
                    {
                        logging.ClearProviders();
                        logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                    })
                      .UseNLog();
                });*/



       /* public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .UseKestrel(c => c.AddServerHeader = false)
            //.UseIISIntegration()
            .UseStartup<Startup>();*/


    }
}
