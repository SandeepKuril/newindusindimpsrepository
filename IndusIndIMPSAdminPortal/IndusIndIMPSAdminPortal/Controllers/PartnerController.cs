﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IndusIndIMPSAdminPortal.Models;
using EntityLayer;

namespace IndusIndIMPSAdminPortal.Controllers
{
    public class PartnerController : Controller
    {
        public IActionResult PartnerManagement()
        {
            PartnerModel partnerModelCall = new PartnerModel();
            //PartnerViewModel partnerInfo = partnerModelCall.GetPartnerList();
            List<PartnerEL> partnerInfo = partnerModelCall.GetPartnerList();
            return View(partnerInfo);
        }

        [HttpPost]
        public JsonResult GetPartnerById(int PartnerId)
        {
            //PartnerEL partnerInfo = new PartnerEL();
            //partnerInfo.Partner_Id = PartnerId;
            PartnerModel partnerModelCall = new PartnerModel();

            List<PartnerEL> partnerInfo = partnerModelCall.GetPartnerById(PartnerId);

            return Json(partnerInfo);
        }


        [HttpPost]
        public JsonResult InsertPartner(PartnerEL partnerData)
        {
            //PartnerModel objInsertPartner = new PartnerModel();
            //string response = objInsertPartner.insertPartner(partnerData);
            //return Json(response);
            //return RedirectToAction("PartnerManagement");

            PartnerModel objInsertPartner = new PartnerModel();
            Response ObjResponse = new Response();

            try
            {
                var valid = TryValidateModel(partnerData);
                if (valid)
                {
                    
                    ObjResponse = objInsertPartner.insertPartner(partnerData);
                }
                else
                {
                    String messages = String.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors).Take(1)
                                                           .Select(v => v.ErrorMessage + " " + v.Exception));

                    //string ss = String.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                    //                                       .Select(v => v.ErrorMessage + " " + v.Exception));

                    ObjResponse.ResponseMessage = messages;
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseMessage = "Exception in InsertPartner :" + ex;
            }
            return Json(ObjResponse);
        

    }

        [HttpPost]
        public JsonResult UpdatePartner(PartnerEL partnerData)
        {
            //PartnerModel objUpdatePartner = new PartnerModel();
            //string response = objUpdatePartner.updatePartner(partnerData);
            //return Json(response);

            PartnerModel objUpdatePartner = new PartnerModel();
            Response ObjResponse = new Response();

            try
            {
                var valid = TryValidateModel(partnerData);
                if (valid)
                {

                    ObjResponse = objUpdatePartner.updatePartner(partnerData);
                }
                else
                {
                    String messages = String.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors).Take(1)
                                                           .Select(v => v.ErrorMessage + " " + v.Exception));
                    ObjResponse.ResponseMessage = messages;
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseMessage = "Exception in InsertPartner :" + ex;
            }
            return Json(ObjResponse);

        }

        [HttpPost]
        public JsonResult DeletePartner(PartnerEL partnerData)
        {
            PartnerModel objDeletePartner = new PartnerModel();
            string response = objDeletePartner.deletePartner(partnerData);
            return Json(response);
        }

        [HttpPost]
        public JsonResult CheckPartner(PartnerEL partnerData)
        {
            PartnerModel objCheckPartner = new PartnerModel();
            string response = objCheckPartner.checkPartner(partnerData);
            return Json(response);
        }

        [HttpPost]
        public JsonResult RejectPartner(PartnerEL partnerData)
        {
            PartnerModel objRejectPartner = new PartnerModel();
            string response = objRejectPartner.rejectPartner(partnerData);
            return Json(response);
        }


    }
}