﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IndusIndIMPSAdminPortal.Models;
using EntityLayer;

namespace IndusIndIMPSAdminPortal.Controllers
{
    public class UserController : Controller
    {
        public IActionResult UserManagement()
        {
            UserModel objUserList = new UserModel();
            List<UserEL> userInfo = objUserList.GetUserList();
            return View(userInfo);
        }

        [HttpPost]
        public JsonResult GetUserById(UserEL User)
        {
            UserModel objUser = new UserModel();
            Response ObjResponse = new Response();
            ObjResponse = objUser.GetUserById(User);
            return Json(ObjResponse);
        }

        [HttpPost]
        public JsonResult CreateUser(UserEL data)
        {
            UserModel objCreateUser = new UserModel();
            Response ObjResponse = new Response();
            try
            {
                var remoteIpAddress  = HttpContext.Connection.RemoteIpAddress;
                var valid = TryValidateModel(data);
                if (valid)
                {
                    data.Client_IP = remoteIpAddress.ToString();
                    ObjResponse = objCreateUser.createUser(data);
                }
                else
                {
                    String messages = String.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors).Take(1)
                                                           .Select(v => v.ErrorMessage + " " + v.Exception));
                    ObjResponse.ResponseMessage = messages;
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseMessage = "Exception in InsertPartner :" + ex;
            }
            return Json(ObjResponse);
        }

        [HttpPost]
        public JsonResult UpdateUser(UserEL data)
        {
            UserModel objUpdateUser = new UserModel();
            Response ObjResponse = new Response();
            //ObjResponse = objUpdateUser.updateUser(data);
            try
            {
                var valid = TryValidateModel(data);
                if (valid)
                {
                    ObjResponse = objUpdateUser.updateUser(data);
                }
                else
                {
                    String messages = String.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors).Take(1)
                                                           .Select(v => v.ErrorMessage + " " + v.Exception));
                    ObjResponse.ResponseMessage = messages;
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseMessage = "Exception in InsertPartner :" + ex;
            }
            return Json(ObjResponse);
        }

        [HttpPost]
        public JsonResult DeleteUser(UserEL data)
        {
            UserModel objDeleteUser = new UserModel();
            Response ObjResponse = new Response();
            ObjResponse = objDeleteUser.deleteUser(data);
            return Json(ObjResponse);
        }

    }
}