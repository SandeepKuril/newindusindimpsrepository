﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonLayer;
using EntityLayer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IndusIndIMPSAdminPortal
{
    public class RoleController : Controller
    {
        #region RoleManagement
        public ActionResult RoleManagement()
        {
            Response res = new Response();
            List<Role> list = new List<Role>();
            try
            {
                // ViewData["RoleList"] = RoleModel.GetAllRole();
                res.ResponseData = RoleModel.GetAllRole();
            }
            catch (Exception ex)
            {
                //ObjResponse.ResponseMessage = ConstantResponseMsg.ControllerError;
            }
            return View();
        }
        #endregion
        #region RoleList
        [HttpGet]
        public JsonResult getRoleList()
        {
            Response res = new Response();
            res.ResponseStatus = true;
            res.ResponseMessage = "Data can't fetch";
            try
            {
                // ViewData["RoleList"] = RoleModel.GetAllRole();
                res.ResponseData = RoleModel.GetAllRole();
                res.ResponseStatus = true;
                res.ResponseMessage = "Success Role List";
            }
            catch (Exception ex)
            {
                //ObjResponse.ResponseMessage = ConstantResponseMsg.ControllerError;
            }
            return Json(res);
        }
        #endregion

        #region SaveRole
        [HttpPost]
        public JsonResult SaveRole([FromBody] Role ObjRoleModel)
        {
            Response ObjResponse = new Response();
            try
            {
                var valid = TryValidateModel(ObjRoleModel);
                if (valid)
                {
                    ObjRoleModel.UserId = HttpContext.Session.GetString("LoginUser");
                    ObjResponse = RoleModel.SaveRole(ObjRoleModel);
                }
                else
                {
                    ObjResponse.ResponseMessage = String.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors).Take(1)
                                                           .Select(v => v.ErrorMessage + " " + v.Exception));
                }
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseMessage = ConstantResponseMsg.ControllerError;
            }
            return Json(ObjResponse);
        }
        #endregion

        #region GetSingleRole
        [HttpPost]
        public JsonResult GetSingleRole([FromBody] Role RoleData)
        {
            Response ObjResponse = new Response();
            try
            {
                //HttpContext.Session.GetString("LoginUser");
                //int roleId = Convert.ToInt32(RoleId);
                ObjResponse = RoleModel.GetSingleRole(Convert.ToInt32(RoleData.RoleId));
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseMessage = ConstantResponseMsg.ControllerError;
            }
            return Json(ObjResponse);
        }
        #endregion

        #region DeleteRole
        [HttpPost]
        public JsonResult DeleteRole([FromBody] Role ObjRoleModel)
        {
            Response ObjResponse = new Response();
            try
            {
                string userId = HttpContext.Session.GetString("LoginUser");
                // int roleId = Convert.ToInt32(RoleId);
                ObjResponse = RoleModel.DeleteRole(Convert.ToInt32(ObjRoleModel.RoleId), userId);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseMessage = ConstantResponseMsg.ControllerError;
            }
            return Json(ObjResponse);
        }
        #endregion


        #region UpdateRole
        [HttpPost]
        public JsonResult UpdateRoleName([FromBody]Role ObjRoleModel)
        {
            Response ObjResponse = new Response();
            try
            {
                ObjRoleModel.UserId = "Sandeep";// HttpContext.Session.GetString("LoginUser");
                // int roleId = Convert.ToInt32(RoleId);
                ObjResponse = RoleModel.UpdateRole(ObjRoleModel);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseMessage = ConstantResponseMsg.ControllerError;
            }
            return Json(ObjResponse);
        }
        #endregion
    }
}