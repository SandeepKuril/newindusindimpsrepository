﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonLayer;
using EntityLayer;
using IndusIndIMPSAdminPortal.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nancy.Json;
//using System.Web.Script.Serialization;

namespace IndusIndIMPSAdminPortal.Controllers
{
    public class AdjustmentFileManagementController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        #region VIEW
        [HttpGet]
        public ActionResult UploadAdjustmentFile()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CheckAdjustmentFile()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ProcessAdjustmentFile()
        {
            return View();
        }
        #endregion

        #region SaveTccRetFile 
        [HttpPost]
        public JsonResult SaveTccRetFile()
        {
            string requestTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            string responseTime = string.Empty;
           // SessionData sessionData = null;
            string retmsg = string.Empty;
            Response res = new Response();
            try
            { 
                
                //checking count of file injected in Request object
                if (Request.Form.Files.Count > 0)
                {
                    //Request.Form.Files.
                    //sessionData = (SessionData)Session["UserSession"];
                    AdjustmentModel adjustmentModel = new AdjustmentModel();
                    res = adjustmentModel.SaveTccRetData(Request, 1000001300);
                    responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                    adjustmentModel = null;
                }
                else
                {
                    res.ResponseStatus = false;
                    res.ResponseMessage = "No file is selected";
                    res.ResponseData = "";
                }
            }

            catch (Exception ex)
            {
                #region RESPONSE
                //Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + Convert.ToString(sessionData.UserId);
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                //Response
                res.ResponseStatus = false;
                res.ResponseMessage = Messages.InternalServerError;
                responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                #endregion
            }
            finally
            {
                var jss = new JavaScriptSerializer();
                string jssRespose = jss.Serialize(res);
                //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ":- " + "RequestTime: [" + requestTime + "] - ResponseTime: [" + DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff") + "] ,Response : " + jssRespose + " UserId:- " + Convert.ToString(sessionData.UserId) + ", Response:- " + retmsg;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogsTxnReqRes); });
            }
            return Json(res);
        }
        #endregion

        #region checkStatusUploadedFilesTccRet
        [HttpPost]
        public JsonResult checkStatusUploadedFilesTccRet()
        {
            DateTime start = DateTime.Now;
            string UserId = string.Empty;
            Response response = new Response();
            AdjustmentModel adjustmentModel = null;
            try
            {
                UserId = 1000001310.ToString();//Session["UserId"].ToString();
                adjustmentModel = new AdjustmentModel();
                response = adjustmentModel.checkStatusUploadedFilesTccRet(Convert.ToInt64(UserId));
            }
            catch (Exception ex)
            {
                string classMethod = "ReconController - checkStatusUploadedFilesTccRet - UserId : " + UserId;
                /*Task.Factory.StartNew(delegate {
                    LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError);
                });*/
                //Response
                response.ResponseData = string.Empty;
                response.ResponseStatus = false;
                response.ResponseMessage = "Something went wrong,checkStatusUploadedFilesTccRet - ErrorCode - R008";
            }
            finally
            {
                DateTime end = DateTime.Now;
                /*Task.Factory.StartNew(delegate {
                    LogHelper.WriteReconLog(Convert.ToString(UserId), "Status:"
                        + Convert.ToString(response.Status) + ",Message:"
                        + Convert.ToString(response.Message), "ReconController", "checkStatusUploadedFilesTccRet", "NA", start, end);
                });*/
                adjustmentModel = null;
            }
            return Json(response);
        }
        #endregion

        /*#region getTccRetList 
        public JsonResult getTccRetList()
        {
            string requestTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            string responseTime = string.Empty;
            //SessionData sessionData = null;
            Response response = new Response();
            try
            {
                //sessionData = (SessionData)Session["UserSession"];
                string sessionKey = Convert.ToString(Session["SessionEncKey"]);
                AdjustmentModel adjustmentModel = new AdjustmentModel();
                response = adjustmentModel.getTccRetReport(sessionData.UserId, sessionKey);
                responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            }

            catch (Exception ex)
            {
                #region RESPONSE
                //Log Writing 
                string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId;
                Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                //Response  
                responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                response.Status = false;
                response.Message = Messages.InternalServerError;
                #endregion
            }
            finally
            {
                string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId + ", Request Time: " + requestTime + ", Response Time:" + responseTime;
                Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogsTxnReqRes); });
            }
            return Json(response);
        }
        #endregion*/
    }
}