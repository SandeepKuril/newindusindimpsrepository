﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonLayer;
using IndusIndIMPSAdminPortal.HelperClass;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace IndusIndIMPSAdminPortal.Controllers
{
    public class HomeController : Controller
    {

        [AuthenticationFilter]
        public IActionResult Index()
        {
            return View();
        }


        [AuthenticationFilter]
        public IActionResult DashBoard()
        {
            NLogHelper logs = new NLogHelper(LogManager.GetCurrentClassLogger());
            logs.Write_Nlog("IActionResult : DashBoard Executed | UserName : "+ Session("LoginUser"),LogType.Information);
            return View();
        }

        private string Session(string v)
        {
            throw new NotImplementedException();
        }
    }
}