﻿using EntityLayer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using CommonLayer;
using Microsoft.Extensions.Logging;
using IndusIndIMPSAdminPortal.HelperClass;
using NLog;
using System.Threading.Tasks;
using System.Reflection;

namespace IndusIndIMPSAdminPortal
{
    //[ValidateAntiForgeryToken]

    public class LoginController : Controller
    {
        static NLogHelper logs = new NLogHelper(LogManager.GetCurrentClassLogger());
        DateTime startTime, endTime;
        string UserName = null;
        /*private readonly ILogger<LoginController> _logger;
        //= new LogHelper();
        DateTime startTime, endTime;
        string UserName = null;
        public LoginController(ILogger<LoginController> logger)
        {
            _logger = logger;
        }*/
        //private readonly SignInManager<IdentityUser> signInManager;

        //public LoginController(SignInManager<IdentityUser> signInManager)
        //{
        //    this.signInManager = signInManager;
        //}


        [HttpGet]
        public IActionResult PortalLogin()
        {
            LoginEL loginModel = new LoginEL();
            loginModel.EncValue = Guid.NewGuid().ToString().Replace("-", "");
            if (loginModel.EncValue.Length > 16)
                loginModel.EncValue = loginModel.EncValue.Substring(0, 16);
            return View(loginModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PortalLogin(LoginEL model)
        {
           
            string Message = string.Empty;
            Response response;
            try
            {
                startTime = DateTime.Now;
                model.Password = Helper.DecryptJS_AES(model.Password, model.EncValue);
                if (ModelState.IsValid)
                {
                    model.SessionId = "";
                    response = LoginModel.Login(model);
                    if (response.ResponseStatus)
                    {
                        // Session
                        // HttpContext.Session.SetInt32("Active",1);
                        HttpContext.Session.SetString("LoginUser", model.UserName);
                        TempData["LoginUser"] = model.UserName;
                        UserName = model.UserName;
                        endTime  = DateTime.Now;
                        Task.Factory.StartNew(delegate{
                            logs.Write_Nlog(" UserName: "+ UserName + "| Loging successfully ", LogType.Information);
                        });
                        // NLogHelper nLog = new NLogHelper();
                        //NLogHelper.Write_Nlog("Nlog Logging Successful by :" + model.UserName.ToString(),LogType.Information);
                       /* _logger.LogInformation("Logging Successful by :" + model.UserName.ToString());
                        _logger.LogError("From Error log by :" + model.UserName.ToString());
                        _logger.LogDebug("From DB Error log by :" + model.UserName.ToString());*/
                        return RedirectToAction("DashBoard", "Home");
                    }
                    ViewData["ErrorMessage"] = response.ResponseMessage;
                    
                    return View();
                }
                else
                {
                    return View(model);
                }
            }
            catch(Exception ex)
            {
                ViewData["ErrorMessage"] = ConstantResponseMsg.ControllerError;
                //Log
                string errormesg = ex.ToString() + " - " + " By User: " + UserName;
                Task.Factory.StartNew(delegate {
                    logs.Write_Nlog(MethodBase.GetCurrentMethod().ToString(), ex, LogType.Error, LogFiles.INDIMPS_LogsError);
                });
                return View();
            }

            
        }
        public ActionResult Logout()
        {
            try
            {
                HttpContext.Session.Remove("LoginUser");
                HttpContext.Session.Clear();
                return RedirectToAction("PortalLogin", "Login");
            }
            catch (Exception ex)
            {

            }
            return View();
        }
    }
}