﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityLayer;
using IndusIndIMPSAdminPortal.Models;
using Microsoft.AspNetCore.Mvc;

namespace IndusIndIMPSAdminPortal.Controllers
{
    public class ReconController : Controller
    {
        #region View
        public IActionResult NtslRawFile()
        {
            return View();
        }
        #endregion

        #region UploadNTSLRawFile
        [HttpPost]
        public IActionResult UploadNtslRowFile(NTSLEL fileData)
        {
            Response ObjResponse = new Response();

            if (fileData.NtslFile != null || fileData.RawFile != null)
            {
                ReconModel objUploadFile = new ReconModel();
                ObjResponse = objUploadFile.uploadNtslRowFile(fileData);
            }
            else
            {
                ObjResponse.ResponseStatus = false;
                ObjResponse.ResponseMessage = "No files selected.";
                //return Json(res);
            }

            TempData["response"] = ObjResponse.ResponseMessage;
            return RedirectToAction("NtslRawFile");
        }
        #endregion

        #region checkStatusUploadedFiles
        public JsonResult checkStatusUploadedFiles()
        {
            DateTime start = DateTime.Now;
            //string UserId = string.Empty;
            Response response = new Response();
            ReconModel objCheckFile = null;
            try
            {
                //UserId = Session["UserId"].ToString();
                objCheckFile = new ReconModel();
                response = objCheckFile.checkStatusUploadedFiles();
            }
            catch (Exception ex)
            {
                string classMethod = "ReconController - checkStatusUploadedFiles";
                //Task.Factory.StartNew(delegate {
                //    LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError);
                //});
                //Response
                response.ResponseData = string.Empty;
                response.ResponseStatus = false;
                response.ResponseMessage = "Something went wrong,checkStatusUploadedFiles - ErrorCode - R008";
            }
            finally
            {
                DateTime end = DateTime.Now;
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteReconLog(Convert.ToString(UserId), "Status:"
                //        + Convert.ToString(response.Status) + ",Message:"
                //        + Convert.ToString(response.Message), "ReconController", "checkStatusUploadedFiles", "NA", start, end);
                //});
                objCheckFile = null;
            }
            return Json(response);
        }
        #endregion

        #region getUncheckedReconList
        public JsonResult getUncheckedReconList()
        {
            Response response = new Response();
            try
            {
                ReconModel reconModel = new ReconModel();
                response = reconModel.getUncheckedRecon();
            }

            catch (Exception ex)
            {
                #region RESPONSE
                //Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response  
                //responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                //response.Status = false;
                //response.Message = Messages.InternalServerError;
                #endregion
            }
            finally
            {
                //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId + ", Request Time: " + requestTime + ", Response Time:" + responseTime;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogsTxnReqRes); });
            }
            return Json(response);
        }
        #endregion


        #region doCheckRecon
        [HttpPost]
        public JsonResult doCheckRecon(CheckRecon checkRecon)
        {
            string requestTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            string responseTime = string.Empty;
            //SessionData sessionData = null;
            Response response = new Response();
            try
            {
                //sessionData = (SessionData)Session["UserSession"];
                //string sessionKey = Convert.ToString(Session["SessionEncKey"]);
                //checkRecon.reconId = AEPSUtil.AESCrypto.Decrypt(checkRecon.reconId, sessionKey);
                ReconModel reconModel = new ReconModel();
                //checkRecon.CheckerId = sessionData.UserId;
                response = reconModel.checkRecon(checkRecon);
                //responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            }

            catch (Exception ex)
            {
                //#region RESPONSE
                ////Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response  
                //responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                //response.Status = false;
                //response.Message = Messages.InternalServerError;
                //#endregion
            }
            finally
            {
                //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId + ", Request Time: " + requestTime + ", Response Time:" + responseTime;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogsTxnReqRes); });
            }
            return Json(response);
        }
        #endregion

        #region doRejectRecon
        [HttpPost]
        public JsonResult doRejectRecon(RejectRecon rejectRecon)
        {
            //string requestTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            //string responseTime = string.Empty;
            //SessionData sessionData = null;
            Response response = new Response();
            try
            {
                //sessionData = (SessionData)Session["UserSession"];
                ReconModel reconModel = new ReconModel();
                //rejectRecon.Rejected_By = sessionData.UserId;

                //string sessionKey = Convert.ToString(Session["SessionEncKey"]);
                //rejectRecon.reconId = AEPSUtil.AESCrypto.Decrypt(rejectRecon.reconId, sessionKey);

                response = reconModel.rejectRecon(rejectRecon);
                //responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            }

            catch (Exception ex)
            {
                //#region RESPONSE
                ////Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response  
                //responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                //response.Status = false;
                //response.Message = Messages.InternalServerError;
                //#endregion
            }
            finally
            {
                //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId + ", Request Time: " + requestTime + ", Response Time:" + responseTime;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogsTxnReqRes); });
            }
            return Json(response);
        }
        #endregion

        #region getDoReconList
        [HttpPost]
        public JsonResult getDoReconList()
        {
            string requestTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            string responseTime = string.Empty;
            //SessionData sessionData = null;
            Response response = new Response();
            try
            {
                //sessionData = (SessionData)Session["UserSession"];
                //string sessionKey = Convert.ToString(Session["SessionEncKey"]);
                ReconModel reconModel = new ReconModel();
                //response = reconModel.getDoReconList(sessionKey, sessionData.UserId);
                response = reconModel.getDoReconList();
                responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            }

            catch (Exception ex)
            {
                #region RESPONSE
                ////Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response  
                //responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                //response.Status = false;
                //response.Message = Messages.InternalServerError;
                #endregion
            }
            finally
            {
                //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId + ", Request Time: " + requestTime + ", Response Time:" + responseTime;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogsTxnReqRes); });
            }
            return Json(response);
        }
        #endregion


        #region doReconProcess
        [HttpPost]
        public JsonResult doReconProcess(CheckRecon doRecon)
        {
            string requestTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            string responseTime = string.Empty;
            //SessionData sessionData = null;
            Response response = new Response();
            try
            {

                //sessionData = (SessionData)Session["UserSession"];
                //string sessionKey = Convert.ToString(Session["SessionEncKey"]);
                ReconModel reconModel = new ReconModel();
                //reconId = AEPSUtil.AESCrypto.Decrypt(reconId, sessionKey);
                //response = reconModel.doReconProcess(Convert.ToInt64(reconId), sessionData.UserId);
                response = reconModel.doReconProcess(Convert.ToInt64(doRecon.ReconFileId), 1003);
                responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            }
            catch (Exception ex)
            {
                #region RESPONSE
                ////Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response  
                //responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                //response.Status = false;
                //response.Message = Messages.InternalServerError;
                #endregion
            }
            finally
            {
                //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId + ", Request Time: " + requestTime + ", Response Time:" + responseTime;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogsTxnReqRes); });
            }
            return Json(response);
        }
        #endregion

    }
}