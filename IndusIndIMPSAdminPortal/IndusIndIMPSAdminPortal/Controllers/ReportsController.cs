﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EntityLayer;
using IndusIndIMPSAdminPortal.Models;

namespace IndusIndIMPSAdminPortal.Controllers
{
    public class ReportsController : Controller
    {
        public IActionResult ReconReport()
        {
                return View();
        }


        //set Recon Report Properties in Temp Data
        [HttpPost]
        public JsonResult setReport([FromBody]ReportsRequest reportRequest)
        {
            Response response = new Response();
            ReportsModel reportModel = new ReportsModel();
            string outmsg = string.Empty;
            //SessionData sessionData = null;
            try
            {
                //sessionData = (SessionData)Session["UserSession"];
                //if (!reportModel.ValidateReport(reportRequest, out outmsg))
                //{
                //    response.Message = outmsg;
                //}
                //else
                //{
                    TempData["FromDate"] = reportRequest.fromDate;
                    TempData["ToDate"] = reportRequest.toDate;
                    TempData.Peek("FromDate");
                    TempData.Peek("ToDate");
                    if (!string.IsNullOrEmpty(reportRequest.rrn))
                    {
                        TempData["RRN"] = reportRequest.rrn;
                        TempData.Peek("RRN");
                    }
                    if (!string.IsNullOrEmpty(reportRequest.status))
                    {
                        TempData["Status"] = reportRequest.status;
                        TempData.Peek("Status");
                    }
                    response.ResponseStatus = true;
                    response.ResponseMessage = "Please wait while downloading file.Click OK to proceed.";
                //}
            }
            catch (Exception ex)
            {
                //#region RESPONSE
                ////Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response
                //response.Message = Messages.InternalServerError;
                //#endregion
            }
            finally
            {
                //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogsTxnReqRes); });
                //reportModel = null;
            }
            return Json(response);
        }
        //

        

        //public ActionResult getReconReportList()
        public ActionResult getReconReportList([FromBody]ReportsRequest reportRequest)
        {
            string requestTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
            string responseTime = string.Empty;
            //SessionData sessionData = null;
            Response response = new Response();
            string fileName = string.Empty;
            //ViewBag.Msg = "Could not process your request.Pease try again";
            try
            {
                //Thread.Sleep(5000);
                //sessionData = (SessionData)Session["UserSession"];
                ReportsModel reportsModel = new ReportsModel();
                //ReportsRequest reportRequest = new ReportsRequest();

                //reportRequest.fromDate = TempData["FromDate"].ToString();
                //reportRequest.toDate = TempData["ToDate"].ToString();
                //reportRequest.status = TempData["Status"].ToString();
                //if (TempData["RRN"] != null)
                //{
                //    reportRequest.rrn = TempData["RRN"].ToString();
                //}



                // = reportsModel.getReconReport(reportRequest, sessionData.UserId);
                response = reportsModel.getReconReport(reportRequest, 1001);

                responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                if (response.ResponseStatus)
                {
                    //fileName = "ReconReport_" + sessionData.UserId.ToString() + "_" + responseTime + ".csv";
                    fileName = "ReconReport_" + "1001" + "_" + responseTime + ".csv";
                    return File(new System.Text.UTF8Encoding().GetBytes(response.ResponseData.ToString()), "text/csv", fileName);
                }

                //ViewBag.Msg = response.Message;
            }
            catch (Exception ex)
            {
                #region RESPONSE
                //Log Writing 
                //string classMethod = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(classMethod, ex, LogType.Error, LogFiles.LogsError); });
                ////Response  
                //responseTime = DateTime.Now.ToString("dd/MM/yyyy:HH-mm-ss-fff");
                //response.Status = false;
                //response.Message = Messages.InternalServerError;
                //return View("ResponseMessage");
                #endregion
            }
            finally
            {
                //string logMessage = this.GetType().Name + " - " + MethodBase.GetCurrentMethod().Name + ", UserId:" + sessionData.UserId + ", Request Time: " + requestTime + ", Response Time:" + responseTime;
                //Task.Factory.StartNew(delegate { LogHelper.Writelog(logMessage, LogType.Information, LogFiles.LogsTxnReqRes); });
            }

             //return Json(response);
            return View("ReconReport");
        }


        [HttpPost]
        public JsonResult getTestList(ReportsRequest reportRequest)
        {
            Response response = new Response();
            try
            {
                response.ResponseMessage = "Success";
                response.ResponseStatus = true;
            }
            catch (Exception ex)
            {
            }
            return Json(response);
        }


    }
}