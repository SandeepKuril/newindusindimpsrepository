﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityLayer;
using IndusIndIMPSAdminPortal.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IndusIndIMPSAdminPortal.Controllers
{
    public class NBINController : Controller
    {
       NBINModel model = new NBINModel();
        private readonly ILogger<Program> logger;
        public ActionResult Index()
        {
          
            NBINModel EntityModel = new NBINModel();
            
            return View();
           
        }

        [HttpGet]
        public JsonResult getAllNBIN()
        {
            Response res = new Response();
            res.ResponseStatus = true;
            res.ResponseMessage = "NBIN data not found";
            try
            {
                NBINModel EntityModel = new NBINModel();
                res.ResponseData = EntityModel.GetNBINList();
                res.ResponseMessage = "Success";
            }
            catch (Exception ex)
            {
                res.ResponseMessage = "Something went wrong";
            }
            return Json(res);
        }

        [HttpPost]
        public JsonResult CreateNBIN([FromBody] NBINEL data)
        {
            Response response = new Response();
            try
            {
                response = model.Create(data);
                return Json(response);

                /*if (model.Create(data).ResponseStatus)
                {
                    response.ResponseStatus = true;

                    //response.ResponseMessage = "NBIN created successfully";
                    //ViewBag.Message = HttpContext.Session.ToString("NBIN created successfully"); 
                    return Json(response);
                }
                else
                {
                    response.ResponseStatus = false;
                    response.ResponseMessage = "NBIN Creation Failed";
                }*/
            }
            catch (Exception ex)
            {
                response.ResponseStatus = false;
                response.ResponseMessage = "Task can't be performed due to some error";
                
            }
            //response = model.Create(data);
            
            return Json(response);
        }

        
        [HttpPost]
        public JsonResult EditNBIN([FromBody] NBINEL Bank_ID)
        {
            Response res = new Response();
            try
            {
                NBINViewModel editlist = new NBINViewModel();
                res = model.GetNBINbyId(Convert.ToInt32(Bank_ID.BANKID));

            }
            catch (Exception ex)
            {
                res.ResponseMessage = "Task can't be performed due to some error";
                res.ResponseStatus = false;
            }
            return Json(res);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public Response UpdateNBIN([FromBody] NBINEL NBINdata)
        {
            Response res = new Response();
            try
            {
                res= model.UpdateNBIN(NBINdata);
            }
            catch (Exception ex)
            {
                res.ResponseMessage = "Task can't be performed due to some error";
                res.ResponseStatus = false;
            }
            return res;
        }

        [HttpPost]
        public JsonResult CheckNBIN(NBINEL data)
        {
            Response res = new Response();
            try
            {
                string strnbin = data.NBIN;
                int IntChecker = 7676;// data.CREATEDBY;
                string feedback = data.REJECTION_DESP;
                res = model.CheckNBIN(strnbin, IntChecker, feedback);
            }
            catch (Exception ex)
            {
                res.ResponseMessage = "Task can't be performed due to some error";
                res.ResponseStatus = false;
            }
            return Json(res);
        }

    }
}
