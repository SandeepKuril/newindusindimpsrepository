﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CommonLayer
{
    public class Helper
    {
        #region JAVASCRIPT-ENCRYPTED-DECRIPTION
        public static string DecryptJS_AES(string cipherText, string DecriptionString)
        {
            try
            {
                var keybytes = Encoding.UTF8.GetBytes(DecriptionString);
                var iv = Encoding.UTF8.GetBytes(DecriptionString);
                var encrypted = Convert.FromBase64String(cipherText.Replace(" ", "+"));
                var decriptedFromJavascript = DecryptFromBytes(encrypted, keybytes, iv);
                return string.Format(decriptedFromJavascript);
            }
            catch (Exception ex)
            {
            }
            return "";
        }

        private static string DecryptFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }

            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            string plaintext = null;
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;
                rijAlg.Key = key;
                rijAlg.IV = iv;
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                try
                {
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                plaintext = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }
        #endregion
    }
}
