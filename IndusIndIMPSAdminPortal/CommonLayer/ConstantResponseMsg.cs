﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLayer
{
    public class ConstantResponseMsg
    {
        #region Common
        public static string DBError = "Something went wrong.Please try again";
        public static string ModelError = "Internal server error.Please try again";
        public static string ControllerError = "Unable to process your request.Please try again.";
        public const string notFound = "Data not found";
        #endregion

        #region Login
        public static string LoginUserNotExist = "User not exist";
        public static string LoginUserBlocked = "User is blocked.Please reset";
        #endregion

        #region Role
        public static string SaveRole = "Role saved successfully";
        public static string AlreadyExist = "This rolename already exist";
        public static string AlreadyExistDeactive = "Same rolename already exist but in deactive state";
        public static string DeleteRole = "Role deleted successfully";
        public static string UpdateRole = "Role updated successfully";
        
        #endregion

        #region Partner
        public const string InsertPartner = "Partner created successfully";
        public const string UpdatedPartner = "Partner updated successfully";
        public const string DeletedPartner = "Partner deleted successfully";
        public const string CheckedPartner = "Partner Checked Successfully";
        public const string RejectedPartner = "Partner rejected Successfully";
        public const string PartnerExist = "Partner already Exist";
        public const string PartnerNotExist = "Partner not Exist";
        public const string CheckNotAllowed = "Maker can not Check";
        public const string RejectNotAllowed = "Maker can not Reject";
        #endregion

        #region User
        public const string InsertUser = "User created successfully";
        public const string UpdatedUser = "User updated successfully";
        public const string DeletedUser = "User deleted successfully";
        public const string UserExist = "User already Exist";
        public const string UserNotExist = "User not Exist";
        #endregion

        #region File Upload
        public const string Fail = "FAILED";
        public const string Success = "SUCCESS";
        public const string FailureDuringDBOperation = "Fail, Error occured during database operation";
        public const string InternalServerError = "Could not process your request.Please try again";
        #endregion

        #region Cycle Validation from DB
        public const string FailedWhileInserting = "Failed while inserting data.Please try again";
        public const string FileNameAlreadyExistsInDB = "File name already exists in database";
        public const string AlreadyUploaded2c = "2C has already uploaded.";
        public const string Select2CFirst = "Please select 2C cycle. ";
        public const string Select3CFirst = "Please select 3C cycle. ";
        public const string Select4CFirst = "Please select 4C cycle. ";
        public const string Select1CFirst = "Please select 1C cycle. ";
        public const string SelectPrevDate3C = "Previous date of 3C not Uploaded.Please Upload";
        public const string SelectPrevDate4C = "Previous date of 4C not Uploaded.Please Upload";
        public const string SelectPrevDate1C = "Previous date of 1C not Uploaded.Please Upload";
        public const string SelectPrevDateFile = "Previous date file not Uploaded.Please Upload";
        public const string ReconNotDonePrevFile = "Please do Recon of previous uploaded file.";
        #endregion

        #region Recon
        public const string FileHasBeenChecked = "File has been checked successfully.";
        public const string FileHasBeenRejected = "File has been rejected successfully.";
        public const string FileHasBeenProcessed = "File has been processed successfully.";
        public const string RecordNotFound = "Record not found";
        public const string MakerCantCheck = "Maker can not check.";
        public const string MakerCantReject = "Maker can not reject.";
        public const string FileAlreadyRejectd = "File already rejectd.";

        public const string ReconIdNotFoundInNTSlMaster = "Recon id not found. ";
        public const string InsertedDeletedNotMatched = "Total inserted and deleted rows not matched in case of data archival.";
        public const string NoDataFoundInRange = "No data found within range to archive.";
        public const string PrevReconChecking = "Previous file recon is pending, Please process previous one first. Order will be 2C-3C-4C-1C or ALL(Single Day)";

        #endregion

    }
}
