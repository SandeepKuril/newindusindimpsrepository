﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndusIndIMPSAdminPortal.HelperClass
{
    public  class NLogHelper
    {
        private readonly ILogger _logger;
        
        public NLogHelper(ILogger logger)
        {
            _logger = logger;
        }
        
        public void Write_Nlog(string message, LogType logType = LogType.Debug)
        {
            //string msg = message;
            try
            {
                switch (logType)
                {
                    case LogType.Debug:
                        _logger.Debug(message, logType);
                        break;
                    case LogType.Information:
                        _logger.Info(message, logType);
                        break;
                    case LogType.Warnings:
                        _logger.Warn(message, logType);
                        break;
                    case LogType.Error:
                        _logger.Error(message, logType);
                        break;
                    case LogType.Logtrace:
                        _logger.Trace(message, logType);
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void Write_Nlog(string ClassMethod, Exception Ex, LogType logtype = LogType.Error, LogFiles logFiles = LogFiles.INDIMPS_LogsError) //Default Value Error
        {
            try
            {
                string message = ClassMethod + " - " + Ex.Message + " - " + Ex.ToString() + " - " + Ex.StackTrace;
              
                switch (logtype)
                {
                    case LogType.Debug:
                        _logger.Debug(message, logtype);
                        break;
                    case LogType.Information:
                        _logger.Info(message, logtype);
                        break;
                    case LogType.Warnings:
                        _logger.Warn(message, logtype);
                        break;
                    case LogType.Error:
                        _logger.Error(message, logtype);
                        break;
                    case LogType.Fatal:
                        _logger.Fatal(message, logtype);
                        break;
                }
            }
            catch (Exception)
            {
            }
        }


        public void WriteDBError(string LoginUserId, Exception errorMessage, string SPName, string ClassMethod, LogType logtype = LogType.Error, LogFiles logFiles = LogFiles.INDIMPS_DB_Error)
        {
            try
            {
                string Message = " User_ID : " + LoginUserId + ", Method Name: " + ClassMethod.ToString() + ", SPName : " + SPName
                         + ", ErrorType =" + LogFiles.INDIMPS_DB_Error + ", Exception =" + errorMessage.Message;
                //DebugLogger = GetLogger("logger_DbError");
                _logger.Error(Message, logtype);
            }
            catch (Exception)
            {
            }
        }

        public void WriteReconLog(string LoginUserId, string LogMessage, string Class, string Method, string reconFileID, DateTime startTime, DateTime endTime)
        {
            try
            {
                LogMessage = "Class: " + Class + ", Method: "
                    + Method + ", LoginUserId: " + LoginUserId
                    + ", Message: " + LogMessage + ", StartTimeStamp:"
                    + startTime.ToString("dd/MM/yyyy:HH-mm-ss-fff")
                    + ", EndTimeStamp:" + endTime.ToString("dd/MM/yyyy:HH-mm-ss-fff")
                    + ", TimeSpan: " + endTime.Subtract(startTime).ToString();

                _logger.Info(LogMessage);
            }
            catch (Exception)
            {
            }
        }

    }

    public enum LogType
    {
        Debug = 1,
        Information = 2,
        Warnings = 3,
        Error = 4,
        Logtrace = 5,
        Fatal = 6

    }

    public enum LogFiles
    {
        INDIMPS_LogsError = 1,
        INDIMPS_LogsTxnReqRes = 2,
        INDIMPS_LogApplication = 3,
        INDIMPS_DB_Error = 4
    }
}