﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using EntityLayer;
using System.Linq;
using Microsoft.Security.Application;

namespace CommonLayer
{
    public class FileReading
    {
        //Created On 29112018, Purpose - Read NTSL file
        #region NTSLxlsFileToDataTable
        public static bool NTSLxlsFileToDataTable(string filePath, out string Message, out DataTable dataTable)
        {
            DateTime start = DateTime.Now;
            Message = string.Empty;
            dataTable = new DataTable();
            bool retVal = false;
            try
            {
                if (string.IsNullOrEmpty(filePath))
                {
                    Message = "Please provide file path";
                    return false;
                }
                else if (Path.GetExtension(filePath) != ".xlsx")
                {
                    Message = "Please select valid file, file extention should be .xls or .xlsx";
                    return false;
                }
                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(filePath, false))
                {
                    dataTable.Columns.Add("Description");
                    dataTable.Columns.Add("noOfTxn");
                    dataTable.Columns.Add("Debit");
                    dataTable.Columns.Add("Credit");
                    //
                    //Read the first sheet from excel file
                    Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();

                    //Get the workSheet Instance
                    Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;

                    //Fetch all the rows present int worksheet
                    IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();

                    #region get worksheet rows
                    foreach (var row in rows)
                    {
                        //Add row's to dataTable
                        dataTable.Rows.Add();
                        int i = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            //Get values
                            if (cell.CellValue != null)
                            {
                                string colName = cell.CellValue.InnerText;
                                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                                {
                                    colName = doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(colName)).InnerText;
                                }
                                //
                                if (!string.IsNullOrEmpty(colName))
                                {
                                    dataTable.Rows[dataTable.Rows.Count - 1][i] = colName;
                                }
                            }
                            else
                            {
                                dataTable.Rows[dataTable.Rows.Count - 1][i] = "";
                            }
                            i++;
                        }
                    }
                    #endregion
                }
                retVal = true;
            }
            catch (Exception ex)
            {
                //get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                //get the top stack frame
                var frame = st.GetFrame(0);
                //Get the file name from the stack frame
                string fileName = frame.GetFileName();
                //get the line number from the stack frame
                int lineNumber = frame.GetFileLineNumber();

                Message = "FileName : " + fileName +
                          " Line Number :" + lineNumber.ToString() +
                          " Error Message : NTSL File Reading Issue ( " + ex.Message + " )";
                //
                //string logMessage = "FileReading - NTSLxlsFileToDataTable -  Error occured during \""
                //    + filePath + "\" file reading - error message - " + ex.Message;
                //Task.Factory.StartNew(delegate {
                //    LogHelper.Writelog(logMessage, ex, LogType.Error, LogFiles.LogsError);
                //});
                retVal = false;
            }
            finally
            {
                //Task.Factory.StartNew(delegate {
                //    LogHelper.WriteReconLog("NA"
                //            , "NTSL file to DataTable reading has been done -  status:"
                //            + retVal.ToString() + " - file path  - [ " + filePath + " ]"
                //            , "FileReading", "NTSLxlsFileToDataTable", "NA", start, DateTime.Now);
                //});
            }
            return retVal;
        }
        #endregion


        #region NTSLDataTableToDBObject
        public static bool NTSLDataTableToDBObject(DataTable NTSLDt, Int64 Recon_Id, out List<NTSLData> NTSLBulkData, out string Message)
        {
            DateTime start = DateTime.Now;
            Message = string.Empty;
            NTSLBulkData = new List<NTSLData>();
            bool retVal = true;
            //
            try
            {
                if (NTSLDt.Rows.Count <= 0)
                {
                    Message = "No data found in NTSL DataTable";
                    return false;
                }
                //
                foreach (DataRow row in NTSLDt.Rows)
                {
                    #region DataTable To BulkList
                    NTSLData obj = new NTSLData();
                    obj.ReconMstrId = Recon_Id;
                    obj.Description = Convert.ToString(row[0]).Trim();
                    obj.NoOfTransactions = Convert.ToString(row[1]).Trim();
                    obj.Debit = Convert.ToString(row[2]).Trim();
                    obj.Credit = Convert.ToString(row[3]).Trim();

                    //Remove XSS malicius file 
                    // obj = (NTSLData)AntiXSS.cleanXSSUsingHtmlEncode(obj);

                    NTSLBulkData.Add(obj);
                    #endregion
                }
                retVal = true;
            }
            catch (Exception ex)
            {
                //get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                //get the top stack frame
                var frame = st.GetFrame(0);
                //Get the file name from the stack frame
                string fileName = frame.GetFileName();
                //get the line number from the stack frame
                int lineNumber = frame.GetFileLineNumber();

                Message = "FileName : " + fileName +
                          " Line Number :" + lineNumber.ToString() +
                          " Error Message : NTSL File Reading Issue while converting to object";
                //  
                //string logMessage = "FileReading - NTSLDataTableToDBObject "
                //    + "- Error occured during converting NTSLDataTable  object .";
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.Writelog(logMessage, ex, LogType.Error, LogFiles.LogsError);
                //});
                retVal = false;
            }
            finally
            {
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog("NA", "NTSLDataTable to obj -  retVal:" + retVal.ToString()
                //        , "FileReading", "NTSLDataTableToDBObject", "NA", start, DateTime.Now);
                //});
            }
            return retVal;
        }
        #endregion

        #region RAWFileToObject
        public bool RAWFileToObject(string RawFilePath, out List<RawDataFile> result, out string msg)
        {
            #region Code
            DateTime startTime = DateTime.Now;
            bool retVal = false; int rowCount = 0;
            result = new List<RawDataFile>();
            msg = string.Empty;
            try
            {
                StreamReader s = new StreamReader(RawFilePath);
                string AllData = s.ReadToEnd();

                AllData = AllData.Replace("\"", "");
                string[] rows = AllData.Split("\r\n".ToCharArray());
                //string[] rows = AllData.Split("\n".ToCharArray());
                foreach (string r in rows)
                {
                    if (!string.IsNullOrEmpty(r))
                    {
                        string TxnType = string.Empty;
                        #region Message Parsing

                        RawDataFile rdf = new RawDataFile();
                        rdf.ParticipantID = r.Substring(0, 3);
                        rdf.TransactionType = r.Substring(3, 2);
                        rdf.FromAccountType = r.Substring(5, 2);
                        rdf.ToAccountType = r.Substring(7, 2);
                        rdf.TransactionSerialNumber = r.Substring(9, 12);
                        rdf.ResponseCode = r.Substring(21, 2);
                        rdf.PANNumber = r.Substring(23, 19);
                        rdf.MemberNumber = r.Substring(42, 1);
                        rdf.ApprovalNumber = r.Substring(43, 6);
                        rdf.SystemTraceAuditNumber = r.Substring(49, 12);
                        rdf.TransactionDate = r.Substring(61, 6);
                        rdf.TransactionTime = r.Substring(67, 6);
                        rdf.MerchantCategoryCode = r.Substring(73, 4);
                        rdf.CardAcceptorSettlDate = r.Substring(77, 6);
                        rdf.CardAcceptorID = r.Substring(83, 15);
                        rdf.CardAcceptorTerminalID = r.Substring(98, 8);
                        rdf.CardAcceptorTermLocation = r.Substring(106, 40);
                        rdf.AquirerID = r.Substring(146, 11);
                        rdf.AcquirerSettlementDate = r.Substring(157, 6);
                        rdf.TransactionCurrencyCode = r.Substring(163, 3);
                        rdf.TransactionAmount = Convert.ToDouble(r.Substring(166, 15)) / 100;
                        rdf.ActualTransactionAmount = Convert.ToDouble(r.Substring(181, 15)) / 100;
                        rdf.TransActivityFee = r.Substring(196, 15);
                        rdf.AcquirerStlCurrCode = r.Substring(211, 3);
                        rdf.AcquirerSettlementAmount = r.Substring(214, 15);
                        rdf.AcquirerStlFee = r.Substring(229, 15);
                        rdf.AcquirerStlProcFee = r.Substring(244, 15);
                        rdf.AcquirerStlConvRate = r.Substring(259, 15);
                        rdf.PaymentReference = r.Substring(274, 50);
                        rdf.Unknown = r.Substring(324, 29);
                        #endregion
                        //Remove XSS malicius file 
                        //rdf = (RawDataFile)AntiXSS.cleanXSSUsingHtmlEncode(rdf);
                        result.Add(rdf);
                    }
                }
                retVal = true;
                rowCount = result.Count;
                s.Dispose();
            }
            catch (Exception ex)
            {
                //get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                //get the top stack frame
                var frame = st.GetFrame(0);
                //Get the file name from the stack frame
                string fileName = frame.GetFileName();
                //get the line number from the stack frame
                int lineNumber = frame.GetFileLineNumber();

                msg = "FileName : " + fileName +
                      " Line Number :" + lineNumber.ToString() +
                      " Error Message : RAW File Reading Issue ( " + ex.Message + " )";
                //
                //string logMessage = "FileReading - RAWFileToObject -  Error occured during \""
                //    + RawFilePath + "\" file reading - error message - " + ex.Message;
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.Writelog(logMessage, ex, LogType.Error, LogFiles.LogsError);
                //});
                retVal = false;
            }
            finally
            {
                //Task.Factory.StartNew(delegate
                //{
                //    LogHelper.WriteReconLog("NA"
                //            , "NTSL file to DataTable reading has been done -  status:"
                //            + retVal.ToString() + " - file path  - [ " + RawFilePath + " ]"
                //            , "FileReading", "NTSLxlsFileToDataTable", "NA", startTime, DateTime.Now);
                //});
            }
            return retVal;
            #endregion
        }
        #endregion

        //#region TCCRETxlsxFileToDataTable
        //public static bool TCCRETxlsxFileToDataTable(string filePath, out string Message, out DataTable dataTable)
        //{
        //    DateTime start = DateTime.Now;
        //    Message = string.Empty;
        //    dataTable = new DataTable();
        //    bool retVal = false;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(filePath))
        //        {
        //            Message = "Please provide file path";
        //            return false;
        //        }
        //        else if (Path.GetExtension(filePath) != ".xls" && Path.GetExtension(filePath) != ".xlsx")
        //        {
        //            Message = "Please select valid file, file extention should be .xls or .xlsx";
        //            return false;
        //        }
        //        using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filePath, false))
        //        {
        //            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        //            IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        //            string relationshipId = sheets.First().Id.Value;
        //            WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        //            Worksheet workSheet = worksheetPart.Worksheet;
        //            SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        //            IEnumerable<Row> rows = sheetData.Descendants<Row>();

        //            foreach (Cell cell in rows.ElementAt(0))
        //            {
        //                //if (dataTable.Columns.Contains("RRN") && GetCellValue(spreadSheetDocument, cell) == "RRN")
        //                //    dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell) + "_No");
        //                //else
        //                dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
        //            }
        //            foreach (Row row in rows)
        //            {
        //                DataRow dataRow = dataTable.NewRow();
        //                for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
        //                {
        //                    if (string.IsNullOrEmpty(GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i))))
        //                    {
        //                        dataRow[i] = "";
        //                    }
        //                    else
        //                    {
        //                        dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
        //                    }
        //                }

        //                dataTable.Rows.Add(dataRow);
        //            }

        //        }
        //        if (dataTable.Rows[0][0].ToString().ToLower() == "txnuid")
        //        {
        //            dataTable.Rows.RemoveAt(0);
        //            retVal = true;
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        //get stack trace for the exception with source file information
        //        var st = new StackTrace(ex, true);
        //        //get the top stack frame
        //        var frame = st.GetFrame(0);
        //        //Get the file name from the stack frame
        //        string fileName = frame.GetFileName();
        //        //get the line number from the stack frame
        //        int lineNumber = frame.GetFileLineNumber();

        //        Message = "FileName : " + fileName +
        //              " Line Number :" + lineNumber.ToString() +
        //              " Error Message : Adjustment File Reading Issue ( " + ex.Message + " )";
        //        //
        //        string logMessage = "FileReading - TCCRETxlsxFileToDataTable -  Error occured during \""
        //            + filePath + "\" file reading - error message - " + ex.Message;
        //        Task.Factory.StartNew(delegate {
        //            LogHelper.Writelog(logMessage, ex, LogType.Error, LogFiles.LogsError);
        //        });
        //        retVal = false;
        //    }
        //    finally
        //    {
        //        Task.Factory.StartNew(delegate {
        //            LogHelper.WriteReconLog("NA"
        //                    , "TCC RET file to DataTable reading has been done -  status:"
        //                    + retVal.ToString() + " - file path  - [ " + filePath + " ]"
        //                    , "FileReading", "TCCRETxlsxFileToDataTable", "NA", start, DateTime.Now);
        //        });
        //    }
        //    return retVal;
        //}
        ////
        //private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        //{
        //    string value = string.Empty;
        //    try
        //    {
        //        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        //        if (cell.CellValue != null)
        //            value = cell.CellValue.InnerXml;

        //        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        //        {
        //            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        //        }
        //        else
        //        {
        //            return value;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        value = "";
        //    }
        //    return value;


        //}
        //#endregion

        //#region NTSLDataTableToDBObject
        //public static bool TCCRETDataTableToDBObject(DataTable NTSLDt, Int64 tccRetId, out List<TCCRET> TCCRETBulkData, out string Message)
        //{
        //    DateTime start = DateTime.Now;
        //    Message = string.Empty;
        //    TCCRETBulkData = new List<TCCRET>();
        //    bool retVal = true;
        //    //
        //    try
        //    {
        //        if (NTSLDt.Rows.Count <= 0)
        //        {
        //            Message = "No data found in TCC RET DataTable";
        //            return false;
        //        }
        //        //
        //        foreach (DataRow row in NTSLDt.Rows)
        //        {
        //            #region DataTable To BulkList
        //            //NTSLData obj = new NTSLData();
        //            //obj.ReconMstrId = Recon_Id;
        //            //obj.Description = Convert.ToString(row[0]).Trim();
        //            //obj.NoOfTransactions = Convert.ToString(row[1]).Trim();
        //            //obj.Debit = Convert.ToString(row[2]).Trim();
        //            //obj.Credit = Convert.ToString(row[3]).Trim();

        //            TCCRET objTccRet = new TCCRET();

        //            objTccRet.TCC_RET_MSTR_ID = tccRetId;
        //            //objTccRet.RRN = Convert.ToString(row[0]).Trim();
        //            objTccRet.TXN_UID = Convert.ToString(row[0]).Trim();
        //            objTccRet.U_ID = Convert.ToString(row[1]).Trim();
        //            objTccRet.ADJDATE = Convert.ToString(row[2]).Trim();
        //            objTccRet.ADJTYPE = Convert.ToString(row[3]).Trim();
        //            objTccRet.REMITTER = Convert.ToString(row[4]).Trim();
        //            objTccRet.BENEFICIERY = Convert.ToString(row[5]).Trim();
        //            objTccRet.RESPONSE = Convert.ToString(row[6]).Trim();
        //            objTccRet.TXNDATE = Convert.ToString(row[7]).Trim();
        //            objTccRet.TXNTIME = Convert.ToString(row[8]).Trim();
        //            if (Convert.ToString(row[9]).Trim().Contains('\''))
        //            {
        //                objTccRet.RRN = Convert.ToString(row[9]).Trim().Trim('\'');
        //            }
        //            else
        //            {
        //                objTccRet.RRN = Convert.ToString(row[9]).Trim();
        //            }

        //            objTccRet.TERMINALID = Convert.ToString(row[10]).Trim();
        //            objTccRet.BEN_MOBILE_NO = Convert.ToString(row[11]).Trim();
        //            objTccRet.REM_MOBILE_N0 = Convert.ToString(row[12]).Trim();
        //            objTccRet.CHBDATE = Convert.ToString(row[13]).Trim();
        //            objTccRet.CHBREF = Convert.ToString(row[14]).Trim();
        //            objTccRet.TXNAMOUNT = Convert.ToString(row[15]).Trim();
        //            objTccRet.ADJAMOUNT = Convert.ToString(row[16]).Trim();
        //            objTccRet.REM_FEE = Convert.ToString(row[17]).Trim();
        //            objTccRet.BEN_FEE = Convert.ToString(row[18]).Trim();
        //            objTccRet.BEN_FEESW = Convert.ToString(row[19]).Trim();
        //            objTccRet.ADJ_FEE = Convert.ToString(row[20]).Trim();
        //            objTccRet.NPCIFEE = Convert.ToString(row[21]).Trim();
        //            objTccRet.REMFEETAX = Convert.ToString(row[22]).Trim();
        //            objTccRet.BEN_FEE_TAX = Convert.ToString(row[23]).Trim();
        //            objTccRet.NPCITAX = Convert.ToString(row[24]).Trim();
        //            objTccRet.ADJREF = Convert.ToString(row[25]).Trim();
        //            objTccRet.BANKADJREF = Convert.ToString(row[26]).Trim();
        //            objTccRet.ADJPROOF = Convert.ToString(row[27]).Trim();
        //            objTccRet.SHDT70 = Convert.ToString(row[28]).Trim();
        //            objTccRet.SHDT71 = Convert.ToString(row[29]).Trim();
        //            objTccRet.SHDT72 = Convert.ToString(row[30]).Trim();
        //            objTccRet.SHDT73 = Convert.ToString(row[31]).Trim();
        //            objTccRet.SHDT74 = Convert.ToString(row[32]).Trim();
        //            objTccRet.SHDT75 = Convert.ToString(row[33]).Trim();
        //            objTccRet.SHDT76 = Convert.ToString(row[34]).Trim();
        //            objTccRet.SHDT77 = Convert.ToString(row[35]).Trim();

        //            //Remove XSS malicius file 
        //            objTccRet = (TCCRET)AntiXSS.cleanXSSUsingHtmlEncode(objTccRet);

        //            TCCRETBulkData.Add(objTccRet);
        //            #endregion
        //        }

        //        #region 

        //        #endregion
        //        retVal = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Message = "Error occured during reading Adjustment File ";
        //        //  
        //        string logMessage = "FileReading - TCCRETDataTableToDBObject "
        //            + "- Error occured during converting NTSLDataTable  object .";
        //        Task.Factory.StartNew(delegate
        //        {
        //            LogHelper.Writelog(logMessage, ex, LogType.Error, LogFiles.LogsError);
        //        });
        //        retVal = false;
        //    }
        //    finally
        //    {
        //        Task.Factory.StartNew(delegate
        //        {
        //            LogHelper.WriteReconLog("NA", "TCCRETDataTableToDBObject-  retVal:" + retVal.ToString()
        //                , "FileReading", "TCCRETDataTableToDBObject", "NA", start, DateTime.Now);
        //        });
        //    }
        //    return retVal;
        //}
        //#endregion

        #region TCCRETxlsxFileToDataTable
        public static bool TCCRETxlsxFileToDataTable(string filePath, out string Message, out DataTable dataTable)
        {
            DateTime start = DateTime.Now;
            Message = string.Empty;
            dataTable = new DataTable();
            bool retVal = false;
            try
            {
                if (string.IsNullOrEmpty(filePath))
                {
                    Message = "Please provide file path";
                    return false;
                }
                else if (Path.GetExtension(filePath) != ".xls" && Path.GetExtension(filePath) != ".xlsx")
                {
                    Message = "Please select valid file, file extention should be .xls or .xlsx";
                    return false;
                }
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filePath, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        //if (dataTable.Columns.Contains("RRN") && GetCellValue(spreadSheetDocument, cell) == "RRN")
                        //    dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell) + "_No");
                        //else
                        dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                    }
                    foreach (Row row in rows)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {
                            if (string.IsNullOrEmpty(GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i))))
                            {
                                dataRow[i] = "";
                            }
                            else
                            {
                                dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                            }
                        }

                        dataTable.Rows.Add(dataRow);
                    }

                }
                if (dataTable.Rows[0][0].ToString().ToLower() == "txnuid")
                {
                    dataTable.Rows.RemoveAt(0);
                    retVal = true;
                }
            }

            catch (Exception ex)
            {
                //get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                //get the top stack frame
                var frame = st.GetFrame(0);
                //Get the file name from the stack frame
                string fileName = frame.GetFileName();
                //get the line number from the stack frame
                int lineNumber = frame.GetFileLineNumber();

                Message = "FileName : " + fileName +
                      " Line Number :" + lineNumber.ToString() +
                      " Error Message : Adjustment File Reading Issue ( " + ex.Message + " )";
                //
                string logMessage = "FileReading - TCCRETxlsxFileToDataTable -  Error occured during \""
                    + filePath + "\" file reading - error message - " + ex.Message;
                /*Task.Factory.StartNew(delegate {
                    LogHelper.WriteLog(logMessage, ex, LogType.Error, LogFiles.LogsError);
                });*/
                retVal = false;
            }
            finally
            {
                /*Task.Factory.StartNew(delegate {
                    LogHelper.WriteReconLog("NA"
                            , "TCC RET file to DataTable reading has been done -  status:"
                            + retVal.ToString() + " - file path  - [ " + filePath + " ]"
                            , "FileReading", "TCCRETxlsxFileToDataTable", "NA", start, DateTime.Now);
                });*/
            }
            return retVal;
        }
        //
        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            string value = string.Empty;
            try
            {
                SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
                if (cell.CellValue != null)
                    value = cell.CellValue.InnerXml;

                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                }
                else
                {
                    return value;
                }
            }
            catch (Exception)
            {
                value = "";
            }
            return value;


        }
        #endregion

        #region NTSLDataTableToDBObject
        public static bool TCCRETDataTableToDBObject(DataTable NTSLDt, Int64 tccRetId, out List<TCCRET> TCCRETBulkData, out string Message)
        {
            DateTime start = DateTime.Now;
            Message = string.Empty;
            TCCRETBulkData = new List<TCCRET>();
            bool retVal = true;
            //
            try
            {
                if (NTSLDt.Rows.Count <= 0)
                {
                    Message = "No data found in TCC RET DataTable";
                    return false;
                }
                //
                foreach (DataRow row in NTSLDt.Rows)
                {
                    #region DataTable To BulkList
                    //NTSLData obj = new NTSLData();
                    //obj.ReconMstrId = Recon_Id;
                    //obj.Description = Convert.ToString(row[0]).Trim();
                    //obj.NoOfTransactions = Convert.ToString(row[1]).Trim();
                    //obj.Debit = Convert.ToString(row[2]).Trim();
                    //obj.Credit = Convert.ToString(row[3]).Trim();

                    TCCRET objTccRet = new TCCRET();

                    objTccRet.TCC_RET_MSTR_ID = tccRetId;
                    //objTccRet.RRN = Convert.ToString(row[0]).Trim();
                    objTccRet.TXN_UID = Convert.ToString(row[0]).Trim();
                    objTccRet.U_ID = Convert.ToString(row[1]).Trim();
                    objTccRet.ADJDATE = Convert.ToString(row[2]).Trim();
                    objTccRet.ADJTYPE = Convert.ToString(row[3]).Trim();
                    objTccRet.REMITTER = Convert.ToString(row[4]).Trim();
                    objTccRet.BENEFICIERY = Convert.ToString(row[5]).Trim();
                    objTccRet.RESPONSE = Convert.ToString(row[6]).Trim();
                    objTccRet.TXNDATE = Convert.ToString(row[7]).Trim();
                    objTccRet.TXNTIME = Convert.ToString(row[8]).Trim();
                    if (Convert.ToString(row[9]).Trim().Contains('\''))
                    {
                        objTccRet.RRN = Convert.ToString(row[9]).Trim().Trim('\'');
                    }
                    else
                    {
                        objTccRet.RRN = Convert.ToString(row[9]).Trim();
                    }

                    objTccRet.TERMINALID = Convert.ToString(row[10]).Trim();
                    objTccRet.BEN_MOBILE_NO = Convert.ToString(row[11]).Trim();
                    objTccRet.REM_MOBILE_N0 = Convert.ToString(row[12]).Trim();
                    objTccRet.CHBDATE = Convert.ToString(row[13]).Trim();
                    objTccRet.CHBREF = Convert.ToString(row[14]).Trim();
                    objTccRet.TXNAMOUNT = Convert.ToString(row[15]).Trim();
                    objTccRet.ADJAMOUNT = Convert.ToString(row[16]).Trim();
                    objTccRet.REM_FEE = Convert.ToString(row[17]).Trim();
                    objTccRet.BEN_FEE = Convert.ToString(row[18]).Trim();
                    objTccRet.BEN_FEESW = Convert.ToString(row[19]).Trim();
                    objTccRet.ADJ_FEE = Convert.ToString(row[20]).Trim();
                    objTccRet.NPCIFEE = Convert.ToString(row[21]).Trim();
                    objTccRet.REMFEETAX = Convert.ToString(row[22]).Trim();
                    objTccRet.BEN_FEE_TAX = Convert.ToString(row[23]).Trim();
                    objTccRet.NPCITAX = Convert.ToString(row[24]).Trim();
                    objTccRet.ADJREF = Convert.ToString(row[25]).Trim();
                    objTccRet.BANKADJREF = Convert.ToString(row[26]).Trim();
                    objTccRet.ADJPROOF = Convert.ToString(row[27]).Trim();
                    objTccRet.SHDT70 = Convert.ToString(row[28]).Trim();
                    objTccRet.SHDT71 = Convert.ToString(row[29]).Trim();
                    objTccRet.SHDT72 = Convert.ToString(row[30]).Trim();
                    objTccRet.SHDT73 = Convert.ToString(row[31]).Trim();
                    objTccRet.SHDT74 = Convert.ToString(row[32]).Trim();
                    objTccRet.SHDT75 = Convert.ToString(row[33]).Trim();
                    objTccRet.SHDT76 = Convert.ToString(row[34]).Trim();
                    objTccRet.SHDT77 = Convert.ToString(row[35]).Trim();

                    //Remove XSS malicius file 
                    //objTccRet = (TCCRET)AntiXss.cleanXSSUsingHtmlEncode(objTccRet);

                    TCCRETBulkData.Add(objTccRet);
                    #endregion
                }

                #region 

                #endregion
                retVal = true;
            }
            catch (Exception ex)
            {
                Message = "Error occured during reading Adjustment File ";
                //  
                string logMessage = "FileReading - TCCRETDataTableToDBObject "
                    + "- Error occured during converting NTSLDataTable  object .";
                /*Task.Factory.StartNew(delegate
                {
                    LogHelper.Writelog(logMessage, ex, LogType.Error, LogFiles.LogsError);
                });*/
                retVal = false;
            }
            finally
            {
                /*Task.Factory.StartNew(delegate
                {
                    LogHelper.WriteReconLog("NA", "TCCRETDataTableToDBObject-  retVal:" + retVal.ToString()
                        , "FileReading", "TCCRETDataTableToDBObject", "NA", start, DateTime.Now);
                });*/
            }
            return retVal;
        }
        #endregion

    }
}
