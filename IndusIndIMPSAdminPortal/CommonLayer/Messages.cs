﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLayer
{
     public enum ErrorType
        {
            VALIDATION = 1,
            EXCEPTION = 2
        }
     public class Messages
        {
            public const int EC_Sucess = 0;
            public const int EC_DatabaseError_1 = -1;
            public const string Faile = "FAILED";
            public const string Success = "SUCCESS";
            public const string DeactivatedUser = "Deactivated User";
            public const int EC_InternalServerError_2 = -2;
            public const string InternalServerError = "Could not process your request.Please try again";
            public const string InvalidRequest = "Invalid request";
            public const string FailureDuringDBOperation = "Fail, Error occured during database operation";
            public const string UserNotAllowedToLogin = "User not allowed to login. Contact to admin";
            public const string TooManyRows = "More than one record of same login user.";

            //Exception error
            public const string ExceptionError = "Something went wrong.Please try again";

            public const string alreadyExist = "Already exist";

            //
            public const string InvalidUserId = "Invalid UserId";
            public const string UnableToLogin = "Unable to login";
            public const string InvalidUserName = "Invalid UserName";
            public const string UserNotActive = "User Not Active";
            public const string ExceededMaximumFailureAttempt = "Exceeded maximum failure attempt";
            public const string userNotFound = "User not found";
            public const string EnterValidUserName = "Enter valid UserName";

            // RECON
            public const string FileNameAlreadyExistsInDB = "File name already exists in database";
            public const string PendingFileNotFound = "Pending file not found";
            public const string RecordNotFound = "Records not found";
            public const string TccRetUpdated = "No data matched and hence could not be updated";
            public const string ReconIdNotFoundInNTSlMaster = "Recon id not found. ";
            public const string InsertedDeletedNotMatched = "Total inserted and deleted rows not matched in case of data archival.";
            public const string NoDataFoundInRange = "No data found within range to archive.";
            public const string PrevReconChecking = "Previous file recon is pending, Please process previous one first. Order will be 2C-3C-4C-1C or ALL(Single Day)";

            public const string FileHasBeenProcessed = "File has been processed successfully.";
            public const string FileHasBeenChecked = "File has been checked successfully.";
            public const string FileHasBeenRejected = "File has been rejected successfully.";
            public const string MakerCantCheck = "Maker can not check.";
            public const string MakerCantReject = "Maker can not reject.";
            public const string FileAlreadyRejectd = "File already rejectd.";


            //CYCLE VALIDATION FROM DB

            public const string FailedWhileInserting = "Failed while inserting data.Please try again";
            public const string AlreadyUploaded2c = "2C has already uploaded.";
            public const string AlreadyUploadedAll = "All data of this date is already uploaded.";
            public const string CantUploadCycle = "All data of this date is already uploaded.Can't upload cycle-2C/3C/4C/1C";
            public const string Select2CAllFirst = "Please select 2C cycle or All. ";
            public const string Select3CFirst = "Please select 3C cycle. ";
            public const string Select4CFirst = "Please select 4C cycle. ";
            public const string Select1CFirst = "Please select 1C cycle. ";
            public const string SelectAllFirst = "Same cycle already exist, Can't upload 2C,3C,4C & 1C. ";
            public const string SelectPrevDate3C = "Previous date of 3C not Uploaded.Please Upload";
            public const string SelectPrevDate4C = "Previous date of 4C not Uploaded.Please Upload";
            public const string SelectPrevDate1C = "Previous date of 1C not Uploaded.Please Upload";

            //Role Management
            public const string insertRole = "Role successfully inserted";
            public const string provideRole = "Please provide role name";
            public const string updateRole = "Role successfully updated";
            public const string deleteRole = "Role successfully deleted";
            public const string Invalid = "is invalid";

            //Role Management
            public const string createUser = "User successfully created";
            public const string updateUser = "User successfully updated";
            public const string deleteUser = "User successfully deleted";
            public const string notFound = "Data not found";
            public const string userAlreadyExist = "Same login username is already exist";

            //Role menu mapping
            public const string roleMenuMappingSuccess = "Selected menu successfully mapped with role";
            public const string roleMenuMappingFail = "Something wrong while mapping role with menu";

        }
    }
