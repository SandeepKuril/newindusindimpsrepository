﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EntityLayer
{
    
    public class Role
    {
       public Nullable<Int32> RoleId { get; set; } = 0;
       // public Int32 RoleId { get; set; }
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Use letters only please")]

        [Required(ErrorMessage = "Please enter role name")]
        [StringLength(20,ErrorMessage ="Role name can not be longer than 20 characters")]
        public string RoleName { get; set; }

        public string CreatedOn { get; set; }
        public string UserId { get; set; }
    }
}
