﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;


namespace EntityLayer
{
    public class NTSLEL
    {
        public long NTSL_ID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Description { get; set; }
        public long CreatedBy { get; set; }
        public string ReconDate { get; set; }
        public string ReconCycle { get; set; }
        public IFormFile NtslFile { get; set; }
        public IFormFile RawFile { get; set; }
    }

    public class ReconFilemaster
    {
        public long Recon_ID { get; set; }
        public string NTSLFileName { get; set; }
        public string NTSLFilePath { get; set; }
        public string RAWFileName { get; set; }
        public string RAWFilePath { get; set; }
        public string Description { get; set; }
        public long UploadedBy { get; set; }
        public string ReconDate { get; set; }
        public string ReconCycle { get; set; }
        //
        public override string ToString()
        {
            return " Recon_ID: " + Convert.ToString(Recon_ID) + ", NTSLFileName:" + NTSLFileName + ", NTSLFilePath: " + NTSLFilePath + ", RawFileName: " + RAWFileName + ",RawFilePath: " + RAWFilePath + ", UploadedBy:" + Convert.ToString(UploadedBy);
        }
    }

    public class NTSLData
    {
        public long ReconMstrId { get; set; }
        public string Description { get; set; }
        public string NoOfTransactions { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public long CreatedBy { get; set; }
    }

    public class RawDataFile
    {
        public long RawId { get; set; }
        public string ParticipantID { get; set; }
        public string TransactionType { get; set; }
        public string FromAccountType { get; set; }
        public string ToAccountType { get; set; }
        public string TransactionSerialNumber { get; set; }
        public string ResponseCode { get; set; }
        public string PANNumber { get; set; }
        public string MemberNumber { get; set; }
        public string ApprovalNumber { get; set; }
        public string SystemTraceAuditNumber { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionTime { get; set; }
        public string MerchantCategoryCode { get; set; }
        public string CardAcceptorSettlDate { get; set; }
        public string CardAcceptorID { get; set; }
        public string CardAcceptorTerminalID { get; set; }
        public string CardAcceptorTermLocation { get; set; }
        public string AquirerID { get; set; }
        public string AcquirerSettlementDate { get; set; }
        public string TransactionCurrencyCode { get; set; }
        public double TransactionAmount { get; set; }
        public double ActualTransactionAmount { get; set; }
        public string TransActivityFee { get; set; }
        public string AcquirerStlCurrCode { get; set; }
        public string AcquirerSettlementAmount { get; set; }
        public string AcquirerStlFee { get; set; }
        public string AcquirerStlProcFee { get; set; }
        public string AcquirerStlConvRate { get; set; }
        public string PaymentReference { get; set; }
        public string Unknown { get; set; }
    }


    public class ReconFileCheker
    {
        public string ReconFileId { get; set; }
        public long MakerId { get; set; }
        public string ReconDate { get; set; }
        public string NTSLFileName { get; set; }
        public string RawFileName { get; set; }
        public string NTSLTotalAmount { get; set; }
        public string NTSLTotalSuccesCountTxn { get; set; }
        public string RawTotalAmount { get; set; }
        public string RawTotalSuccesCountTxn { get; set; }
        public string NTSLTotalAmount08 { get; set; }
        public string NTSLTotalSuccesCountTxn08 { get; set; }
        public string RawTotalAmount08 { get; set; }
        public string RawTotalSuccesCountTxn08 { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public string NTSLSettlment { get; set; }
    }


}
