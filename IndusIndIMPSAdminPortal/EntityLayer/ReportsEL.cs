﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityLayer
{
    public class ReportsEL
    {
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string status { get; set; }
        public string rrn { get; set; }
    }

    public class ReportsRequest : ReportsEL
    {
    }

    public class ReconReportsResponse
    {
        public string TransactionDateTime { get; set; }
        public string Amount { get; set; }
        public string Aadhar { get; set; }
        public string RRN { get; set; }
        public string STAN { get; set; }
        public string IIN { get; set; }
        public string ResponseCode { get; set; }
        public string Status { get; set; }
        public string TransactionTypeCode { get; set; }
        public string Retry { get; set; }
        public string IsVR { get; set; }
        public string RetailerTxnId { get; set; }
        public string TerminalId { get; set; }
        public string UserId { get; set; }
        public string RetailerId { get; set; }
        public string ReconStatus { get; set; }
        public string ReconDate { get; set; }
        public string NpciResponseCode { get; set; }
    }
}
