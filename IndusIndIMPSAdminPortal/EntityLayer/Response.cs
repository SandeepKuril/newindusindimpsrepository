﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityLayer
{
    public class Response
    {
        public bool ResponseStatus { get; set; } = false;
        public string ResponseMessage { get; set; }
        public object ResponseData { get; set; }
    }
}
