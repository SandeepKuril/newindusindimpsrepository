﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace EntityLayer
{
    public class LoginEL
    {
        [Required(ErrorMessage = "Username is required")]
        [StringLength(30, ErrorMessage = "Username can not be longer than 30 characters.")]
        [DisplayName("User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        //[StringLength(15, ErrorMessage = "Password can not be longer than 15 characters.")]
        [DisplayName("User Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string EncValue { get; set; }
        public string SessionId { get; set; }
    }
}
