﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace EntityLayer
{
    public class PartnerEL
    {
        public int Partner_Id { get; set; }

        [Required(ErrorMessage = "Partner Name is required")]
        [MaxLength(100, ErrorMessage = "Partner Name can not exceed 100 characters")]
        public string Partner_Name { get; set; }

        [Required(ErrorMessage = "Mobile Number is required")]
        [RegularExpression("([1-9][0-9]{9})", ErrorMessage = "Mobile Number is invalid.")]
        [MaxLength(10, ErrorMessage = "Mobile Number can not exceed 10 characters")]
        public string Mobile_Number { get; set; }


        [Required(ErrorMessage = "Landline Number is required")]
        [RegularExpression("([1-9][0-9]{11})", ErrorMessage = "Landline Number is invalid.")]
        [MaxLength(12, ErrorMessage = "Landline Number can not exceed 12 characters")]
        public string Landline { get; set; }

        [Required(ErrorMessage = "Email is required")]
        //[DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        [MaxLength(50, ErrorMessage = "Email can not exceed 100 characters")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Office Address is required")]
        [MaxLength(300, ErrorMessage = "Office Address can not exceed 300 characters")]
        public string Office_Address { get; set; }

        [Required(ErrorMessage = "Pincode is required")]
        [RegularExpression("([0-9]{6})", ErrorMessage = "Pincode can not exceed 6 characters")]
        //[MaxLength(6, ErrorMessage = "Pincode can not exceed 6 characters")]
        public Nullable<int> Pincode { get; set; }

        [Required(ErrorMessage = "State is required")]
        [MaxLength(20, ErrorMessage = "State can not exceed 20 characters")]
        public string State { get; set; }

        [Required(ErrorMessage = "City is required")]
        [MaxLength(20, ErrorMessage = "City can not exceed 20 characters")]
        public string City { get; set; }

        [Required(ErrorMessage = "Pan Number is required")]
        [MaxLength(10, ErrorMessage = "Pan Number can not exceed 10 characters")]
        public string Pan_Number { get; set; }

        [Required(ErrorMessage = "Contact Person Name is required")]
        [MaxLength(100, ErrorMessage = "Contact Person Name can not exceed 100 characters")]
        public string Contact_Person_Name { get; set; }

        [Required(ErrorMessage = "Contact Person Number is required")]
        [MaxLength(10, ErrorMessage = "Contact Person Number can not exceed 10 characters")]
        [RegularExpression("([1-9][0-9]{9})", ErrorMessage = "Contact Person Number is invalid.")]
        public string Contact_Person_Number { get; set; }

        [Required(ErrorMessage = "Account Number is required")]
        [MaxLength(50, ErrorMessage = "Account Number can not exceed 50 characters")]
        public string Account_Number { get; set; }

        [Required(ErrorMessage = "Ifsc Code is required")]
        [MaxLength(11, ErrorMessage = "Ifsc Code can not exceed 11 characters")]
        public string Ifsc_Code { get; set; }

        [Required(ErrorMessage = "Centeral Gst Number is required")]
        [MaxLength(15, ErrorMessage = "Centeral Gst Number can not exceed 15 characters")]
        public string Centeral_Gst_Number { get; set; }

        [Required(ErrorMessage = "State Gst Number is required")]
        [MaxLength(15, ErrorMessage = "State Gst Number can not exceed 15 characters")]
        public string State_Gst_Number { get; set; }

        [Required(ErrorMessage = "Gst Address is required")]
        [MaxLength(300, ErrorMessage = "Gst Address can not exceed 300 characters")]
        public string Gst_Address { get; set; }

        [Required(ErrorMessage = "Website Url is required")]
        [MaxLength(100, ErrorMessage = "Website Url can not exceed 100 characters")]
        public string Website_Url { get; set; }
        public Nullable<int> Status { get; set; }
        public string Status_Msg { get; set; }
        public DateTime Created_On { get; set; }
        public int Created_By { get; set; }
        public DateTime Modified_On { get; set; }
        public int Modified_By { get; set; }
        public DateTime Rejected_On { get; set; }
        public int Rejected_By { get; set; }

        //[Required(ErrorMessage = "Reason for rejection is required")]
        //[MaxLength(100, ErrorMessage = "Reason for rejection can not exceed 100 characters")]
        public string Rejected_Reason { get; set; }
        public DateTime Deleted_On { get; set; }
        public int Deleted_By { get; set; }
    }

    //public class PartnerViewModel
    //{
    //    public PartnerEL PartnerData { get; set; }
    //    public List<PartnerEL> PartnerList { get; set; }
    //}
}
