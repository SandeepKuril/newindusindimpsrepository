﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EntityLayer
{
    public class NBINEL
    {
        
        public int BANKID { get; set; }
        [Required]
        public string BANKNAME { get; set; }
        public string IFSCCODE { get; set; }
        public string NBIN { get; set; }
        public int ENABLESTATUS { get; set; }
        public string SHORTCODE { get; set; }
        public int MODIFIEDBY { get; set; }
        public DateTime MODIFIEDON { get; set; }
        public int CREATEDBY { get; set; }
        public DateTime CREATEDON { get; set; }
        public char IFSCSTATUS { get; set; }
        public char STATUS { get; set; }
        public int CHECKEDBY { get; set; }
        public DateTime CHECKEDON { get; set; }
        public string REJECTION_DESP { get; set; }

    }

    public class NBINViewModel
    {
        public NBINEL NBIN { get; set; }
        public List<NBINEL> ListNBIN { get; set; }

    }
}
