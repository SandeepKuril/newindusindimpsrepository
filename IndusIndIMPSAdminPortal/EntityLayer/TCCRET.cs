﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityLayer
{
   public class TCCRET_File
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Tcc_Ret_Date { get; set; }
    }
    //
    public class TCCRET
    {
        public Int64 TCC_RET_MSTR_ID { get; set; }
        public string RRN { get; set; }
        public string TXN_UID { get; set; }
        public string U_ID { get; set; }
        public string ADJDATE { get; set; }
        public string ADJTYPE { get; set; }
        public string REMITTER { get; set; }
        public string BENEFICIERY { get; set; }
        public string RESPONSE { get; set; }
        public string TXNDATE { get; set; }
        public string TXNTIME { get; set; }
        public string TERMINALID { get; set; }
        public string BEN_MOBILE_NO { get; set; }
        public string REM_MOBILE_N0 { get; set; }
        public string CHBDATE { get; set; }
        public string CHBREF { get; set; }
        public string TXNAMOUNT { get; set; }
        public string ADJAMOUNT { get; set; }
        public string REM_FEE { get; set; }
        public string BEN_FEE { get; set; }
        public string BEN_FEESW { get; set; }
        public string ADJ_FEE { get; set; }
        public string NPCIFEE { get; set; }
        public string REMFEETAX { get; set; }
        public string BEN_FEE_TAX { get; set; }
        public string NPCITAX { get; set; }
        public string ADJREF { get; set; }
        public string BANKADJREF { get; set; }
        public string ADJPROOF { get; set; }
        public string SHDT70 { get; set; }
        public string SHDT71 { get; set; }
        public string SHDT72 { get; set; }
        public string SHDT73 { get; set; }
        public string SHDT74 { get; set; }
        public string SHDT75 { get; set; }
        public string SHDT76 { get; set; }
        public string SHDT77 { get; set; }
    }

    public class CheckTCCRET
    {
        public string tccRetId { get; set; }
        public long CheckerId { get; set; }
    }
    public class RejectTCCRET
    {
        public string tccRetId { get; set; }
        public long Rejected_By { get; set; }
        public string Rejected_Reason { get; set; }
    }
    public class TccRetREPORT
    {
        public string TccRetId { get; set; }
        public string FileName { get; set; }
        public string Status { get; set; }
        public string StatusRemark { get; set; }
        public string Date { get; set; }
        public string UploadedBy { get; set; }
    }
}
