﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EntityLayer
{
    public class UserEL
    {
        public int User_Id { get; set; }

        [Required(ErrorMessage = "Login Username is required")]
        [MaxLength(40, ErrorMessage = "Login Username can not exceed 40 characters")]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Use letters only please")]
        public string Login_Username { get; set; }
        public DateTime Last_Login_Time { get; set; }
        public string Client_IP { get; set; }
        public char Status { get; set; }
        public DateTime Created_On { get; set; }
        public int Created_By { get; set; }
        public DateTime Modified_On { get; set; }
        public int Modified_By { get; set; }
        public DateTime Deleted_On { get; set; }
        public int Deleted_By { get; set; }
        public int Role_Id { get; set; }
        public string Role_Name { get; set; }
    }

    //public class UserViewModel 
    //{
    //    public UserEL UserData { get; set; }
    //    public List<UserEL> UserList { get; set; }
    //}

}
