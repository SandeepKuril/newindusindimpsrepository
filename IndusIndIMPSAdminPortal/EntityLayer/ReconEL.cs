﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityLayer
{
    class ReconEL
    {
    }

    public class CheckRecon
    {
        public string ReconFileId { get; set; }
        public long CheckerId { get; set; }
    }
    public class RejectRecon
    {
        public string ReconFileId { get; set; }
        public long Rejected_By { get; set; }
        public string RejectedReason { get; set; }
    }
}
